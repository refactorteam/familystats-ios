//
//  PrivacyViewController.swift
// FamilyStats
//
//  Created by Can Koç on 22.05.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import CORELIB
import AppTrackingTransparency
import AdSupport

class PrivacyViewController: BaseViewController<BaseViewModel> {
    
    var delegate: PrivacyPageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.privacy.rawValue
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        // action
        actionButtonPrivacy()
        actionButtonTerms()
        actionButtonContinue()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.Privacy.title.UILocalize(lblTitle)
        LocKey.View.Privacy.settingsDescription.UILocalize(lblSettingsDecription)
        txtDescription.text = LocKey.View.Privacy.description.localize
        LocKey.View.Onboarding.buttonTerms.UILocalize(btnTerms)
        LocKey.View.Onboarding.buttonPrivacy.UILocalize(btnPrivacy)
        LocKey.View.Privacy.buttonContinue.UILocalize(btnContinue)
    }
    
    override func setUI() {
        super.setUI()
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var txtDescription: UITextView!
    @IBOutlet weak fileprivate var lblSettingsDecription: UILabel!
    @IBOutlet weak fileprivate var btnContinue: BorderedCornerButton!
    @IBOutlet weak fileprivate var btnPrivacy: UIButton!
    @IBOutlet weak fileprivate var btnTerms: UIButton!
}

// MARK: - Action

extension PrivacyViewController {
    
    private func actionButtonPrivacy() {
        btnPrivacy.rx.tap.bind(onNext: { [weak self] _ in
            guard let privacyURL = CORELIB.shared.getPrivacyURL(),
                let webView = CORELIB.shared.getWebView(with: privacyURL) else { return }
            self?.present(webView, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonTerms() {
        btnTerms.rx.tap.bind(onNext: { [weak self] _ in
            guard let termsURL = CORELIB.shared.getTermsURL(),
                let webView = CORELIB.shared.getWebView(with: termsURL) else { return }
            self?.present(webView, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonContinue() {
        btnContinue.rx.tap.bind(onNext: { [weak self] _ in
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                    DispatchQueue.main.async {
                        self?.requestNotificationPermission()
                    }
                })
            } else {
                self?.requestNotificationPermission()
            }
        }).disposed(by: disposeBag)
    }
    
    private func requestNotificationPermission() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (_, _) in
            DispatchQueue.main.async {
                self.delegate?.privacyPageClosed()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
