//
//  CodeVerificationViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB
import RxSwift
import IQKeyboardManagerSwift
import RxCocoa
import RxSwift

class CodeVerificationViewController: BaseViewController<CodeVerificationViewModel> {
    
    var delegate: NumberVerificationDelegate?
    var phoneNumber = BehaviorRelay(value: String())
    var verificationId = BehaviorRelay(value: String())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.codeVerification.rawValue
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        // bind
        bindInputToViewModel()
        
        // action
        actionButtonNext()
        actionButtonBack()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.CodeVerification.title.UILocalize(lblTitle)
        LocKey.View.CodeVerification.buttonNext.UILocalize(btnNext)
    }
    
    override func setUI() {
        super.setUI()
        
        txtNumbers.first?.becomeFirstResponder()
        txtNumbers.forEach({ txtInput in
            setBorder(to: txtInput)
            addDelegate(to: txtInput)
        })
        
        btnBack.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 15) {
            self.btnBack.isHidden = false
        }
    }
    
    // MARK: - Outlet
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate var txtNumbers: [TextFieldWBackSpace]!
    @IBOutlet fileprivate weak var btnNext: UIButton!
    @IBOutlet fileprivate weak var btnBack: HigligtedButton!
}

// MARK: - Bind

extension CodeVerificationViewController {
    
    private func bindInputToViewModel() {
        phoneNumber.bind(to: viewModel.phoneNumber).disposed(by: disposeBag)
        verificationId.bind(to: viewModel.verificationId).disposed(by: disposeBag)
    }
}

// MARK: - Action

extension CodeVerificationViewController: LoaderShowable, Validatable {
    
    private func actionButtonNext() {
        btnNext.rx.tap.filter({ [weak self] _ in
            let smsCode = self?.getSmsCodeFromInputs() ?? String()
            if self?.isValid(text: smsCode) == false {
                AlertManager.shared.showNoAction(title: LocKey.View.CodeVerification.alertNumberNull.localize, button: nil)
                return false
            } else {
                self?.viewModel.smsCode.accept(smsCode)
                self?.showLoader()
                return true
            }
        }).bind(onNext: { [weak self] _ in
            self?.viewModel.actionVerify().subscribe(onNext: { result in
                self?.hideLoader()
                if result {
                    IQKeyboardManager.shared.enableAutoToolbar = true
                    self?.navigationController?.dismiss(animated: true, completion: {
                        self?.delegate?.numberVerificationCompleted()
                    })
                } else {
                    AlertManager.shared.showNoAction(title: LocKey.Alert.General.warning.localize, button: nil)
                }
            }, onError: { error in
                self?.hideLoader()
                guard let error = error as? NumberVerifyError else { return }
                switch error {
                case .firebaseError(let errorMessage):
                    AlertManager.shared.showNoAction(title: errorMessage, button: nil)
                }
            }).disposed(by: (self?.disposeBag)!)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonBack() {
        btnBack.rx.tap.bind(onNext: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    private func getSmsCodeFromInputs() -> String {
        var smsCode = String()
        txtNumbers.forEach({ txtInput in
            smsCode += (txtInput.text ?? String())
        })
        return smsCode
    }
    
    private func addDelegate(to textField: TextFieldWBackSpace) {
        textField.delegate = self
        textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        textField.backSpaceDelegate = self
    }
}

// MARK: - UITextFieldDelegate

extension CodeVerificationViewController: UITextFieldDelegate, BackSpaceDelegate {
    
    // UITextFieldDelegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text != nil && textField.text?.isEmpty == false) {
            if textField.tag != (self.txtNumbers.count - 1) {
                self.txtNumbers[textField.tag + 1].becomeFirstResponder()
            }
        }
    }
    
    // BackSpaceDelegate
    func didBackSpaceTapped(on textField: UITextField) {
        if textField.tag > 0 {
            self.txtNumbers[textField.tag - 1].becomeFirstResponder()
            self.txtNumbers[textField.tag - 1].text = String()
        }
    }
    
    // UITextFieldDelegate: Maxlength
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if Int(string) != nil && string.count > 1 && string.count <= 6 {
            string.enumerated().forEach({ (item) in
                self.txtNumbers[item.offset].text = String(item.element)
            })
        }
        
        let maxLength = 1
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

// MARK: - UI

extension CodeVerificationViewController {
    
    private func setBorder(to textField: UITextField) {
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 2
        textField.layer.borderColor = UIColor(red: 17/255, green: 32/255, blue: 77/255, alpha: 255/255).cgColor
        textField.tintColor = .clear
    }
}
