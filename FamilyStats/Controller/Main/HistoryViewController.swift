//
//  HistoryViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import CORELIB

class HistoryViewController: BaseViewController<HistoryViewModel> {
    
    var name = BehaviorRelay<String>(value: String())
    var phoneNumber = BehaviorRelay<String>(value: String())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.numberHistory.rawValue
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        lblPhoneNumber.text = phoneNumber.value
        lblName.text = name.value
        
        // bind
        bindInputToViewModel()
        bindTableView()
        bindTotalDuration()
        
        // action
        actionButtonBack()
        actionButtonReport()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.Main.userOnline.UILocalize(lblOnline)
        LocKey.View.Main.userOffline.UILocalize(lblOffline)
        LocKey.View.History.duration.UILocalize(lblDuration)
        LocKey.View.History.report.localize.UILocalize(btnReport)
        
        LocKey.View.History.onlineTime.UILocalize(lblOnlineTimeTitle)
        LocKey.View.History.total.UILocalize(lblOnlineTimeDescription)
    }
    
    override func setUI() {
        super.setUI()
        
        tblList.tableFooterView = UIView()
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblName: UILabel!
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var tblList: UITableView!
    @IBOutlet weak fileprivate var lblPhoneNumber: UILabel!
    @IBOutlet weak fileprivate var btnReport: UIButton!
    @IBOutlet weak fileprivate var lblOnlineTime: UILabel!
    
    @IBOutlet weak fileprivate var lblOnlineTimeTitle: UILabel!
    @IBOutlet weak fileprivate var lblOnlineTimeDescription: UILabel!
    @IBOutlet weak fileprivate var lblOnline: UILabel!
    @IBOutlet weak fileprivate var lblOffline: UILabel!
    @IBOutlet weak fileprivate var lblDuration: UILabel!
    @IBOutlet weak fileprivate var btnBack: UIButton!
}

// MARK: - Bind

extension HistoryViewController {
    
    private func bindInputToViewModel() {
        phoneNumber.bind(to: viewModel.phoneNumber).disposed(by: disposeBag)
        viewModel.getAllHistory()
    }
    
    private func bindTableView() {
        let cellIdentifier = HistoryTableViewCell.nameOfClass
        tblList.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        viewModel.historyList.bind(to: tblList.rx.items(cellIdentifier: cellIdentifier, cellType: HistoryTableViewCell.self)) { (index, model, cell) in
            cell.cellItem = model
        }.disposed(by: disposeBag)
    }
    
    private func bindTotalDuration() {
        viewModel.totalDuration.bind(to: lblOnlineTime.rx.text).disposed(by: disposeBag)
    }
}

// MARK: - Action

extension HistoryViewController {
    
    private func actionButtonReport() {
        btnReport.rx.tap.bind(onNext: { [weak self] _ in
            if CORELIB.shared.isLandingPageActive(for: .appSpecific(ShowPackagePageEnum.report.rawValue)) {
                CORELIB.shared.showLandingPage(for: .appSpecific(ShowPackagePageEnum.report.rawValue), on: self!)
            } else {
                CORELIB.shared.updateUsageCountForLandingPage(for: .appSpecific(ShowPackagePageEnum.report.rawValue))
                self?.performSegue(withIdentifier: SegueConstant.report, sender: nil)
            }
        }).disposed(by: self.disposeBag)

    }
    
    private func actionButtonBack() {
        btnBack.rx.tap.bind(onNext: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
}

// MARK: - Segue

extension HistoryViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueConstant.report,
            let viewController = segue.destination as? StatisticsViewController {
            viewController.name.accept(self.name.value)
            viewController.phoneNumber.accept(self.phoneNumber.value)
            viewController.userHistory.accept(self.viewModel.historyList.value)
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
        }
    }
}
