//
//  StatisticsViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 23.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Charts
import CORELIB

class StatisticsViewController: BaseViewController<StatisticsViewModel> {
    
    var name = BehaviorRelay<String>(value: String())
    var phoneNumber = BehaviorRelay<String>(value: String())
    var userHistory = BehaviorRelay<[UserHistoryModel]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.numberStatistics.rawValue
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        // bind
        bindInputToViewModel()
        
        // action
        actionButtonBack()
        actionButtonDaily()
        actionButtonWeekly()
        actionButtonMonthly()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.Report.title.localize.UILocalize(lblTitle)
        LocKey.View.Report.segmentToday.UILocalize(btnDaily)
        LocKey.View.Report.segmentWeekly.UILocalize(btnWeekly)
        LocKey.View.Report.segmentMonthly.UILocalize(btnMonthly)
        LocKey.View.Report.sessionCountTitle.UILocalize(lblSessionCountTitle)
        LocKey.View.Report.totalUsageTitle.UILocalize(lblTotalUsageTitle)
    }
    
    override func setUI() {
        super.setUI()
        
        setUIChartView()
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var lblName: UILabel!
    @IBOutlet weak fileprivate var lblPhoneNumber: UILabel!
    @IBOutlet weak fileprivate var viewChart: BarChartView!
    @IBOutlet weak fileprivate var btnBack: UIButton!
    
    @IBOutlet weak fileprivate var lblSessionCountTitle: UILabel!
    @IBOutlet weak fileprivate var lblSessionCount: UILabel!
    @IBOutlet weak fileprivate var lblTotalUsageTitle: UILabel!
    @IBOutlet weak fileprivate var lblTotalUsage: UILabel!
    
    @IBOutlet weak fileprivate var btnDaily: UIButton!
    @IBOutlet weak fileprivate var viewDaily: BorderedCornerView!
    @IBOutlet weak fileprivate var btnWeekly: UIButton!
    @IBOutlet weak fileprivate var viewWeekly: BorderedCornerView!
    @IBOutlet weak fileprivate var btnMonthly: UIButton!
    @IBOutlet weak fileprivate var viewMonthly: BorderedCornerView!
}

// MARK: - Action

extension StatisticsViewController {
    
    private func actionButtonDaily() {
        btnDaily.rx.tap.bind(onNext: { [weak self] _ in
            self?.viewModel.calculateData(for: .today)
            self?.bindData()
            self?.hideAllView()
            self?.viewDaily.isHidden = false
            self?.btnDaily.setTitleColor(.black, for: .normal)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonWeekly() {
        btnWeekly.rx.tap.bind(onNext: { [weak self] _ in
            self?.viewModel.calculateData(for: .week)
            self?.bindData()
            self?.hideAllView()
            self?.viewWeekly.isHidden = false
            self?.btnWeekly.setTitleColor(.black, for: .normal)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonMonthly() {
        btnMonthly.rx.tap.bind(onNext: { [weak self] _ in
            self?.viewModel.calculateData(for: .month)
            self?.bindData()
            self?.hideAllView()
            self?.viewMonthly.isHidden = false
            self?.btnMonthly.setTitleColor(.black, for: .normal)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonBack() {
        btnBack.rx.tap.bind(onNext: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    private func hideAllView() {
        viewDaily.isHidden = true
        viewWeekly.isHidden = true
        viewMonthly.isHidden = true
        
        btnDaily.setTitleColor(.lightGray, for: .normal)
        btnWeekly.setTitleColor(.lightGray, for: .normal)
        btnMonthly.setTitleColor(.lightGray, for: .normal)
    }
}

// MARK: - Bind

extension StatisticsViewController {
    
    private func bindInputToViewModel() {
        userHistory.bind(to: viewModel.userHistory).disposed(by: disposeBag)
        viewModel.calculateData(for: .today)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            self.bindData()
        }
    }
    
    private func bindData() {
        self.phoneNumber.bind(to: lblPhoneNumber.rx.text).disposed(by: disposeBag)
        self.name.bind(to: lblName.rx.text).disposed(by: disposeBag)
        
        viewChart.data = viewModel.getBindableData()
        lblSessionCount.text = String(viewModel.sessionCount)
        
        var totalUsageText = String(viewModel.usageSeconds / 3600) + " " + LocKey.View.Report.hour.localize
        let minutes = viewModel.usageSeconds / 60
        if minutes > 0 {
            totalUsageText += " " + String(minutes) + " " + LocKey.View.Report.minutes.localize
        }
        lblTotalUsage.text = totalUsageText
    }
}

// MARK: - UI

extension StatisticsViewController {
    
    private func setUIChartView() {
        viewChart.dragEnabled = true
        viewChart.setScaleEnabled(true)
        viewChart.pinchZoomEnabled = true
        viewChart.noDataFont = Theme.Font.poppinsMedium.of(size: 13)
        viewChart.noDataTextColor = .black
        viewChart.noDataText = LocKey.View.Report.noData.localize
        
        let yAxis = viewChart.leftAxis
        yAxis.labelFont = Theme.Font.poppinsMedium.of(size: 13)
        yAxis.labelTextColor = .black
        yAxis.labelPosition = .outsideChart
        yAxis.drawAxisLineEnabled = false
        yAxis.gridColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        yAxis.drawZeroLineEnabled = true
        yAxis.drawBottomYLabelEntryEnabled = false
        yAxis.drawTopYLabelEntryEnabled = true
        yAxis.labelTextColor = .black
        yAxis.valueFormatter = self
        yAxis.granularity = 1
        yAxis.axisMaxLabels = 5
        yAxis.axisMinLabels = 1
        yAxis.labelXOffset = -10
        yAxis.xOffset = 15
        
        let yAxisRight = viewChart.rightAxis
        yAxisRight.xOffset = 10
        yAxisRight.drawLabelsEnabled = true
        yAxisRight.labelTextColor = .clear
        yAxisRight.drawAxisLineEnabled = false
        yAxisRight.drawGridLinesEnabled = false
        
        let xAxis = viewChart.xAxis
        xAxis.granularity = 1
        xAxis.drawGridLinesEnabled = false
        xAxis.valueFormatter = self
        xAxis.labelFont = Theme.Font.poppinsRegular.of(size: 13)
        xAxis.labelTextColor = .black
        xAxis.axisMinLabels = 1
        xAxis.axisMaxLabels = 5
        xAxis.labelPosition = .bottom
        
        viewChart.rightAxis.enabled = true
        viewChart.legend.enabled = false
        viewChart.animate(xAxisDuration: 0.3, yAxisDuration: 0.5)
    }
}

// MARK: - Bottom Label Delegate

extension StatisticsViewController: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if axis == viewChart.xAxis {
            return viewModel.dateList[Int(value) % viewModel.dateList.count]
        } else {
            return String(Int(value))
        }
    }
}

