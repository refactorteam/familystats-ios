//
//  GroupHistoryViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import CORELIB

class GroupHistoryViewController: BaseViewController<GroupHistoryViewModel> {
    
    let trackingList = BehaviorRelay<[UserTrackingModel]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.groupHistory.rawValue
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        self.screenName = EventScreenName.groupHistory.rawValue
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        guard let group = AppSingleton.shared.userTrackingList.value?.groups.first,
              let firstNumber = group.members.first?.phoneNumber,
              let secondNumber = group.members.last?.phoneNumber else { return }
        
        lblTitle.text = group.name
        lblPhoneNumber.text = firstNumber + " - " + secondNumber
        
        // bind
        bindTableView()
        
        // action
        actionButtonBack()
        actionButtonReport()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.GroupHistory.title.UILocalize(lblPageTitle)
        LocKey.View.Main.userOnline.UILocalize(lblOnline)
        LocKey.View.Main.userOffline.UILocalize(lblOffline)
        LocKey.View.History.duration.UILocalize(lblDuration)
        
        LocKey.View.History.report.localize.UILocalize(btnReport)
    }
    
    override func setUI() {
        super.setUI()
        
        tblList.tableFooterView = UIView()
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var navigationItemHistory: UINavigationItem!
    @IBOutlet weak fileprivate var tblList: UITableView!
    @IBOutlet weak fileprivate var lblPhoneNumber: UILabel!
    @IBOutlet weak fileprivate var btnReport: UIButton!
    
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var lblOnline: UILabel!
    @IBOutlet weak fileprivate var lblOffline: UILabel!
    @IBOutlet weak fileprivate var lblDuration: UILabel!
    
    @IBOutlet weak fileprivate var btnBack: UIButton!
    @IBOutlet weak fileprivate var lblPageTitle: UILabel!
}

// MARK: - Bind

extension GroupHistoryViewController {
    
    private func bindTableView() {
        let cellIdentifier = HistoryTableViewCell.nameOfClass
        tblList.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        viewModel.historyList.bind(to: tblList.rx.items(cellIdentifier: cellIdentifier, cellType: HistoryTableViewCell.self)) { (index, model, cell) in
            cell.cellItem = model
        }.disposed(by: disposeBag)
    }
}

// MARK: - Action

extension GroupHistoryViewController {
    
    private func actionButtonReport() {
        btnReport.rx.tap.bind(onNext: { [weak self] _ in
            if CORELIB.shared.isLandingPageActive(for: .appSpecific(ShowPackagePageEnum.groupReport.rawValue)) {
                CORELIB.shared.showLandingPage(for: .appSpecific(ShowPackagePageEnum.groupReport.rawValue), on: self!)
            } else {
                CORELIB.shared.updateUsageCountForLandingPage(for: .appSpecific(ShowPackagePageEnum.groupReport.rawValue))
                self?.performSegue(withIdentifier: SegueConstant.groupReport, sender: nil)
            }
        }).disposed(by: self.disposeBag)
    }
    
    private func actionButtonBack() {
        btnBack.rx.tap.bind(onNext: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
}

// MARK: - Segue

extension GroupHistoryViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueConstant.groupReport,
            let viewController = segue.destination as? StatisticsViewController {
            
            guard let group = AppSingleton.shared.userTrackingList.value?.groups.first,
                let firstNumber = group.members.first?.phoneNumber,
                let secondNumber = group.members.last?.phoneNumber else {
                return
            }
            viewController.name.accept(group.name)
            viewController.phoneNumber.accept(firstNumber + " - " + secondNumber)
            viewController.userHistory.accept(self.viewModel.historyList.value)
        }
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
}
