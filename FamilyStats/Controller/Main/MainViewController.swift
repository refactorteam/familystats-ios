//
//  MainViewController.swift
// FamilyStats
//
//  Created by Can Koç on 10.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import PhoneNumberKit
import RxSwift
import ContactsUI
import CORELIB

class MainViewController: BaseViewController<MainViewModel> {
    
    private var groupObserverDisposer: Disposable?
    private var imageDisposer: [Disposable?] = []
    private var createGroupViewModel: CreateGroupViewModel!
    private var bannerAdManager: BannerAdInterface?
    
    private let storageManager = FirebaseStorageManager()
    private let phoneNumberKit = PhoneNumberKit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.main.rawValue
        CORELIB.shared.startMainViewController(on: self, disposeBag)
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        createGroupViewModel = CreateGroupViewModel(presentedViewController: self)
        
        // observe
        observePhoneNumberTextField()
        observeGroupInfo()
        observePremiumStatus()
        
        // bind
        bindBannerAd()
        bindInputToViewModel()
        bindListFollows()
        
        // action
        actionButtonAddNumber()
        actionButtonCreateGroup()
        actionButtonActiveGroup()
        actionButtonContactPicker()
        actionButtonActiveGroupMore()
        actionButtonRemoveBanner()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        txtPhoneNumber.toolbarPlaceholder = LocKey.View.Main.enterPhoneNumber.localize
        txtName.placeholder = LocKey.View.Main.enterName.localize
        LocKey.View.Main.buttonStartTracking.UILocalize(btnTrack)
        LocKey.View.Main.noDataDescription.UILocalize(lblNoData)
        LocKey.View.Main.createGroupTitle.UILocalize(btnCreateGroup)
        
        LocKey.TabBar.dashboard.UILocalize(btnTabMain)
        LocKey.TabBar.contactManager.UILocalize(lblContactManager)
        LocKey.TabBar.other.UILocalize(btnTabOther)
    }
    
    override func setUI() {
        super.setUI()
        
        if #available(iOS 11.0, *) {
            txtPhoneNumber.withDefaultPickerUI = true
        }
        txtPhoneNumber.withFlag = true
        txtPhoneNumber.withExamplePlaceholder = true
        txtPhoneNumber.withPrefix = true
        
        if CORELIB.shared.readAppInfoValue(for: "group_status") == "false" {
            self.viewGroup.isHidden = true
        }
        
        setMainUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: - Outlet
    
    @IBOutlet weak var txtPhoneNumber: PhoneNumberTextField!
    @IBOutlet weak fileprivate var txtName: UITextField!
    @IBOutlet weak fileprivate var btnTrack: BorderedCornerButton!
    @IBOutlet weak fileprivate var btnContactPicker: UIButton!
    @IBOutlet weak fileprivate var imgUser: UIImageView!
    
    @IBOutlet weak fileprivate var viewNoData: UIView!
    @IBOutlet weak fileprivate var lblNoData: UILabel!
    @IBOutlet weak fileprivate var viewList: UIView!
    @IBOutlet weak fileprivate var tblList: UITableView!
    @IBOutlet weak fileprivate var viewSubscriberArea: UIView!
    
    // TAB
    
    @IBOutlet weak fileprivate var btnTabMain: UIButton!
    @IBOutlet weak fileprivate var btnTabOther: UIButton!
    @IBOutlet weak fileprivate var lblContactManager: UILabel!
    
    // GROUP
    
    @IBOutlet weak fileprivate var viewGroup: UIView!
    
    @IBOutlet weak fileprivate var viewCreateGroup: UIView!
    @IBOutlet weak fileprivate var btnCreateGroup: UIButton!
    
    @IBOutlet weak fileprivate var viewActiveGroup: BorderedCornerView!
    @IBOutlet weak fileprivate var btnActiveGroupMore: UIButton!
    @IBOutlet weak fileprivate var lblActiveGroupName: UILabel!
    @IBOutlet weak fileprivate var viewActiveGroupStatusView: BorderedCornerView!
    @IBOutlet weak fileprivate var lblActiveGroupStatusName: UILabel!
    @IBOutlet weak fileprivate var lblActiveGroupLastSeen: UILabel!
    @IBOutlet weak fileprivate var btnActiveGroup: UIButton!
    @IBOutlet weak fileprivate var constTableHeight: NSLayoutConstraint!
    
    // Ad
    
    @IBOutlet weak fileprivate var viewBannerAd: UIView!
    @IBOutlet weak fileprivate var btnRemoveBanner: UIButton!
    @IBOutlet weak fileprivate var constViewAdHeight: NSLayoutConstraint!
    
    // Const
    
    @IBOutlet weak fileprivate var constViewTopHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var constImageHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var constStackHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var constButtonHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var constStackBottom: NSLayoutConstraint!
    @IBOutlet weak fileprivate var constImageTop: NSLayoutConstraint!
    
}

// MARK: - Bind

extension MainViewController {
    
    private func bindInputToViewModel() {
        txtPhoneNumber.rx.text.orEmpty.bind(to: viewModel.phoneNumber).disposed(by: disposeBag)
        txtName.rx.text.orEmpty.bind(to: viewModel.name).disposed(by: disposeBag)
    }
    
    private func bindListFollows() {
        let cellIdentifier = TrackTableViewCell.nameOfClass
        tblList.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        viewModel.trackingList.filter({ [weak self] list in
            let listIsEmpty = list.isEmpty
            self?.viewNoData.isHidden = !listIsEmpty
            self?.viewList.isHidden = listIsEmpty
            if !listIsEmpty && list.count >= (Int(CORELIB.shared.readAppInfoValue(for: "max_number_track_count") ?? "2") ?? 2) {
                self?.btnTrack.alpha = 0.5
                self?.btnTrack.isUserInteractionEnabled = false
                self?.constTableHeight.constant = 152
            } else {
                self?.btnTrack.alpha = 1
                self?.btnTrack.isUserInteractionEnabled = true
                self?.constTableHeight.constant = CGFloat((80 * list.count))
            }
            
            if CORELIB.shared.readAppInfoValue(for: "group_status") == "true" {
                self?.viewGroup.isHidden = listIsEmpty
            }
            
            return !listIsEmpty
        }).bind(to: tblList.rx.items(cellIdentifier: cellIdentifier, cellType: TrackTableViewCell.self)) { [weak self] (index, model, cell) in
            cell.cellItem = model

            cell.btnMore.rx.tap.bind(onNext: { _ in
                
                let alert = UIAlertController(title: model.name, message: model.phoneNumber, preferredStyle: .actionSheet)
                var muteLocalization = LocKey.View.Main.unmuteNumber.localize
                if model.notf == true {
                    muteLocalization = LocKey.View.Main.muteNumber.localize
                }
                alert.addAction(UIAlertAction(title: muteLocalization, style: .default, handler: { _ in
                    let notf = model.notf
                    self?.viewModel.actionUpdateStatusNotification(phoneNumber: model.phoneNumber, status: !notf)
                    self?.viewModel.actionUpdateNotificationStatusToList(with: model.phoneNumber)
                }))
                alert.addAction(UIAlertAction(title: LocKey.View.Main.deleteNumber.localize, style: .destructive, handler: { _ in
                    DispatchQueue.main.async {
                        let deleteAction = UIAlertAction(title: LocKey.View.Main.deleteNumber.localize, style: .destructive) { _ in
                            self?.viewModel.requestDeleteNumber(with: model.phoneNumber)
                        }
                        let cancel = UIAlertAction(title: LocKey.Dialog.General.buttonCancel.localize, style: .cancel, handler: nil)
                        AlertManager.shared.show(title: LocKey.View.GroupHistory.removeDescription.localize, message: nil, action: [deleteAction, cancel])
                    }
                }))
                alert.addAction(UIAlertAction(title: LocKey.Dialog.General.buttonCancel.localize, style: .cancel, handler: nil))
                self?.present(alert, animated: true, completion: nil)
                
            }).disposed(by: cell.disposeBag)
            
        }.disposed(by: disposeBag)
        
        tblList.rx.itemSelected.bind(onNext: { [weak self] indexPath in
            if CORELIB.shared.isLandingPageActive(for: .appSpecific(ShowPackagePageEnum.numberDetail.rawValue)) {
                CORELIB.shared.showLandingPage(for: .appSpecific(ShowPackagePageEnum.numberDetail.rawValue), on: self!)
            } else {
                guard let cell = self?.tblList.cellForRow(at: indexPath) as? TrackTableViewCell,
                    let phoneNumber = cell.cellItem?.phoneNumber,
                    let name = cell.cellItem?.name else {
                    return
                }
                self?.performSegue(withIdentifier: SegueConstant.history, sender: (name: name, number: phoneNumber))
                CORELIB.shared.updateUsageCountForLandingPage(for: .appSpecific(ShowPackagePageEnum.numberDetail.rawValue))
            }
        }).disposed(by: disposeBag)
    }
}

// MARK: - Observe

extension MainViewController {
    
    private func observePhoneNumberTextField() {
        txtPhoneNumber.rx.controlEvent(.editingDidBegin).bind(onNext: { _ in
            if CORELIB.shared.readAppInfoValue(for: "show_number_alert") == "true" {
                DispatchQueue.main.async {
                    AlertManager.shared.showNoAction(title: LocKey.View.Main.ownNumberCautionTitle.localize,
                                                     message: LocKey.View.Main.ownNumberCaution.localize, button: nil)
                }
            }
        }).disposed(by: disposeBag)
        
        if CORELIB.shared.readAppInfoValue(for: "image_checker") == "true" {
            txtPhoneNumber.rx.text.subscribe(onNext: { [weak self] _ in
                do {
                    guard let parsedNumber = try self?.phoneNumberKit.parse((self?.txtPhoneNumber.text)!) else { return }
                    let number = parsedNumber.numberString.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+", with: "")
                    
                    DispatchQueue.main.async {
                        self?.imageDisposer.append(self?.storageManager.getImage(for: number, on: (self?.imgUser)!).subscribe(onNext: { result in
                            if !result {
                                ApiRequest.image(with: ImageRequestModel(countryCode: DeviceHelper.getDeviceCountry(), number: number)).subscribe().disposed(by: (self?.disposeBag)!)
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                                    self?.imageDisposer.append(self?.storageManager.getImage(for: number, on: (self?.imgUser)!).subscribe())
                                }
                            }
                        }))
                    }
                } catch {
                    self?.imgUser.image = UIImage(named: "imgProfile")
                    self?.imageDisposer.removeAll()
                }
            }).disposed(by: disposeBag)
        }
    }
    
    private func observeGroupInfo() {
        CORELIB.shared.observeAppInfoValue(for: "group_status").subscribe(onNext: { [weak self] settings in
            if settings == "true" {
                self?.observeGroup()
            } else {
                self?.viewGroup.isHidden = true
            }
        }).disposed(by: disposeBag)
    }
    
    private func observeGroup() {
        AppSingleton.shared.userTrackingList.subscribe(onNext: { [weak self] trackingInfo in
            if let group = trackingInfo?.groups.first {
                self?.observeGroupStatus()
                self?.lblActiveGroupName.text = group.name
                self?.viewActiveGroup.isHidden = false
                self?.viewCreateGroup.isHidden = true
            } else {
                self?.viewActiveGroup.isHidden = true
                self?.viewCreateGroup.isHidden = false
                self?.groupObserverDisposer?.dispose()
            }
        }).disposed(by: disposeBag)
    }
    
    private func observeGroupStatus() {
        if groupObserverDisposer != nil {
            groupObserverDisposer?.dispose()
        }
        groupObserverDisposer = viewModel.observeGroupStatus().subscribe(onNext: { [weak self] groupStatus in
            guard let groupStatus = groupStatus else {
                return
            }
            
            if groupStatus.isOnline {
                self?.viewActiveGroup.borderColor = UIColor(red: 0/255, green: 191/255, blue: 118/255, alpha: 255)
                self?.viewActiveGroupStatusView.backgroundColor = UIColor(red: 0/255, green: 191/255, blue: 118/255, alpha: 255)
                self?.lblActiveGroupStatusName.text = LocKey.View.Main.userOnline.localize
            } else {
                self?.viewActiveGroup.borderColor = UIColor(red: 239/255, green: 68/255, blue: 69/255, alpha: 255)
                self?.viewActiveGroupStatusView.backgroundColor = UIColor(red: 239/255, green: 68/255, blue: 69/255, alpha: 255)
                self?.lblActiveGroupStatusName.text = LocKey.View.Main.userOffline.localize
            }
            
            if groupStatus.lastOnline > 0 {
                self?.lblActiveGroupLastSeen.text = DateHelper().getReadableFormat(timeStamp: TimeInterval(groupStatus.lastOnline))
            }
        })
    }
    
    private func observePremiumStatus() {
        CORELIB.shared.observeSubscriptionStatus().subscribe(onNext: { [weak self] isPro in
            if isPro {
                self?.viewBannerAd.subviews.forEach({ $0.removeFromSuperview() })
                self?.btnRemoveBanner.isHidden = true
                self?.constViewAdHeight.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.view.layoutIfNeeded()
                }
                self?.viewModel.addUserToTrackingList()
            } else {
                if CORELIB.shared.readAppInfoValue(for: "free_trial") == "true" {
                    self?.viewModel.addUserToTrackingList()
                } else {
                    self?.viewModel.trackingList.accept([])
                }
            }
        }).disposed(by: disposeBag)
    }
}

// MARK: - Action

extension MainViewController: Hapticable, LoaderShowable {
    
    func actionButtonAddNumber() {
        btnTrack.rx.tap.bind(onNext: { [weak self] _ in
            do {
                _ = try self?.phoneNumberKit.parse((self?.txtPhoneNumber.text)!)
                if self?.txtName.text?.isEmpty == true {
                   AlertManager.shared.showNoAction(title: LocKey.View.Main.nameNullValidation.localize, button: nil)
                } else {
                    if CORELIB.shared.isLandingPageActive(for: .appSpecific(ShowPackagePageEnum.trackerAdd.rawValue)) {
                        CORELIB.shared.showLandingPage(for: .appSpecific(ShowPackagePageEnum.trackerAdd.rawValue), on: self!)
                    } else {
                        if CORELIB.shared.readAppInfoValue(for: "number_verification") == "true" {
                            guard let vc = UIStoryboard(name: StoryboardConstant.verification, bundle: nil).instantiateViewController(identifier: VerificationNavigationViewController.nameOfClass) as? VerificationNavigationViewController else { return }
                            vc.verificationDelegate = self
                            vc.phoneNumber = self?.txtPhoneNumber.phoneNumber
                            self?.present(vc, animated: true, completion: nil)
                        } else {
                            self?.viewModel.actionPhoneNumberAdd()
                            self?.imageDisposer.removeAll()
                        }
                    }
                }
            } catch {
                AlertManager.shared.showNoAction(title: LocKey.View.Main.phoneNumberNotValidValidation.localize, button: nil)
            }
        }).disposed(by: self.disposeBag)
    }
    
    private func actionButtonContactPicker() {
        btnContactPicker.rx.tap.bind(onNext: { [weak self] _ in
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self!
            contactPicker.displayedPropertyKeys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
            self?.present(contactPicker, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    func actionClearField() {
        self.view.endEditing(true)
        txtPhoneNumber.text = ""
        txtName.text = ""
        viewModel.phoneNumber.accept("")
        viewModel.name.accept("")
        imgUser.image = UIImage(named: "imgProfile")
    }
    
    private func actionButtonCreateGroup() {
        btnCreateGroup.rx.tap.bind(onNext: { [weak self] _ in
            if self?.viewModel.trackingList.value.count == 2 {
                self?.performSegue(withIdentifier: SegueConstant.createGroup, sender: nil)
            } else {
                AlertManager.shared.showNoAction(title: LocKey.View.Main.alertGroupLimit.localize, button: nil)
            }
        }).disposed(by: self.disposeBag)
    }
        
    private func actionButtonActiveGroup() {
        btnActiveGroup.rx.tap.bind(onNext: { [weak self] _ in
            self?.performSegue(withIdentifier: SegueConstant.groupHistory, sender: nil)
        }).disposed(by: self.disposeBag)
    }
    
    private func actionButtonActiveGroupMore() {
        btnActiveGroupMore.rx.tap.bind(onNext: { [weak self] _ in

            guard let group = AppSingleton.shared.userTrackingList.value?.groups.first else { return }
            let alert = UIAlertController(title: group.name, message: nil, preferredStyle: .actionSheet)
            var muteLocalization = LocKey.View.Main.unmuteNumber.localize
            if group.onAlert == true {
                muteLocalization = LocKey.View.Main.muteNumber.localize
            }
            alert.addAction(UIAlertAction(title: muteLocalization, style: .default, handler: { _ in
                self?.createGroupViewModel.actionUpdateOnlineStatusNotification(status: !group.onAlert)
            }))
            alert.addAction(UIAlertAction(title: LocKey.View.Main.groupName.localize, style: .default, handler: { _ in
                self?.performSegue(withIdentifier: SegueConstant.editGroup, sender: nil)
            }))
            alert.addAction(UIAlertAction(title: LocKey.View.Main.deleteNumber.localize, style: .destructive, handler: { _ in
                DispatchQueue.main.async {
                    let deleteAction = UIAlertAction(title: LocKey.View.Main.deleteNumber.localize, style: .destructive) { _ in
                        self?.createGroupViewModel.actionRemoveGroup()
                    }
                    let cancel = UIAlertAction(title: LocKey.Dialog.General.buttonCancel.localize, style: .cancel, handler: nil)
                    AlertManager.shared.show(title: LocKey.View.GroupHistory.removeDescription.localize, message: nil, action: [deleteAction, cancel])
                }
            }))
            alert.addAction(UIAlertAction(title: LocKey.Dialog.General.buttonCancel.localize, style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonRemoveBanner() {
        btnRemoveBanner.rx.tap.bind(onNext: { [weak self] _ in
            CORELIB.shared.showLandingPage(for: .appSpecific(ShowPackagePageEnum.bannerRemove.rawValue), on: self!)
        }).disposed(by: disposeBag)
    }
}

// MARK: - Contact Picker

extension MainViewController: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        let userName: String = contact.givenName
        let userPhoneNumbers: [CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber: CNPhoneNumber = userPhoneNumbers[0].value
        let primaryPhoneNumberStr: String = firstPhoneNumber.stringValue
        
        txtPhoneNumber.text = primaryPhoneNumberStr
        txtName.text = userName
        
        viewModel.phoneNumber.accept(primaryPhoneNumberStr)
        viewModel.name.accept(userName)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {}
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) { }
}

// MARK: - Segue

extension MainViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueConstant.history,
            let viewController = segue.destination as? HistoryViewController,
            let senderInfo = sender as? (name: String, number: String) {
            viewController.name.accept(senderInfo.name)
            viewController.phoneNumber.accept(senderInfo.number)
        } else if segue.identifier == SegueConstant.createGroup,
            let viewController = segue.destination as? CreateGroupViewController {
            viewController.trackingList.accept(self.viewModel.trackingList.value)
            viewController.delegate = self
            viewController.modalPresentationStyle = .overCurrentContext
        } else if segue.identifier == SegueConstant.groupHistory,
            let viewController = segue.destination as? GroupHistoryViewController {
            viewController.trackingList.accept(self.viewModel.trackingList.value)
        } else if segue.identifier == SegueConstant.editGroup,
            let viewController = segue.destination as? CreateGroupViewController {
            
            guard let group = AppSingleton.shared.userTrackingList.value?.groups.first else {
                return
            }
            viewController.trackingList.accept(self.viewModel.trackingList.value)
            viewController.groupName = group.name
            viewController.modalPresentationStyle = .overCurrentContext
        }
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
}

// MARK: - GroupCreateProtocol

extension MainViewController: GroupCreateProtocol {
    
    func groupCreated() {
        // self.viewModel.getGroupStatus()
    }
}

// MARK: - NumberVerificationDelegate

extension MainViewController: NumberVerificationDelegate {
    
    func numberVerificationCompleted() {
        self.viewModel.actionPhoneNumberAdd()
    }
    
}

// MARK: - UI

extension MainViewController {
    
    func setMainUI() {
        if UIScreen.main.bounds.size.height > 800 {
            constViewTopHeight.constant = 285
            constImageHeight.constant = 70
            constStackHeight.constant = 115
            constButtonHeight.constant = 60
            btnTrack.cornerRadius = 30
            constImageTop.constant = 5
            self.view.layoutIfNeeded()
            
            imgUser.layer.cornerRadius = 35
            imgUser.layer.masksToBounds = true
            imgUser.clipsToBounds = true
        } else {
            constViewTopHeight.constant = 235
            constImageHeight.constant = 50
            constStackHeight.constant = 105
            constButtonHeight.constant = 50
            btnTrack.cornerRadius = 25
            constImageTop.constant = 0
            self.view.layoutIfNeeded()
            
            imgUser.layer.cornerRadius = 25
            imgUser.layer.masksToBounds = true
            imgUser.clipsToBounds = true
        }
    }
}

// MARK: - BannerAdShowable, BannerAdLoadingProtocol

extension MainViewController: BannerAdShowable, BannerAdLoadingProtocol {
    
    fileprivate func bindBannerAd() {
        bannerAdManager = createBannerAdManager(with: BannerAdOptions(viewController: self, adAreaView: viewBannerAd,
                                                                      adSource: .main), delegate: self)
        DispatchQueue.main.async {
            self.bannerAdManager?.showBannerAd()
        }
    }
    
    func bannerAdDidShow(with state: Bool) {
        constViewAdHeight.constant = state ? 70 : 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.btnRemoveBanner.isHidden = !state
        })
    }
}


// MARK: - PremiumProtocol

extension MainViewController: PremiumProtocol {
    
    func paymentSuccess() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            if self.viewModel.trackingList.value.count < (Int(CORELIB.shared.readAppInfoValue(for: "max_number_track_count") ?? "2") ?? 2)
                && !self.viewModel.phoneNumber.value.isEmpty
                && !self.viewModel.name.value.isEmpty {
                
                    do {
                        _ = try self.phoneNumberKit.parse(self.txtPhoneNumber.text!)
                        
                        if CORELIB.shared.readAppInfoValue(for: "number_verification") == "true" {
                            guard let vc = UIStoryboard(name: StoryboardConstant.verification, bundle: nil).instantiateViewController(identifier: VerificationNavigationViewController.nameOfClass) as? VerificationNavigationViewController else { return }
                            vc.verificationDelegate = self
                            vc.phoneNumber = self.txtPhoneNumber.phoneNumber
                            self.present(vc, animated: true, completion: nil)
                        } else {
                            self.viewModel.actionPhoneNumberAdd()
                        }
                    } catch {
                        AlertManager.shared.showNoAction(title: LocKey.View.Main.phoneNumberNotValidValidation.localize, button: nil)
                    }
            }
        }
    }
    
    func paymentFail() {
    }
    
    func paymentCancel() {
    }
}
