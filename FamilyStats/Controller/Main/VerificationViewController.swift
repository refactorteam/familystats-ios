//
//  VerificationViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import PhoneNumberKit
import CORELIB
import IQKeyboardManagerSwift

class VerificationViewController: BaseViewController<VerificationViewModel> {
    
    var phoneNumber: PhoneNumber?
    var delegate: NumberVerificationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.numberRegistration.rawValue
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        txtPhoneNumber.text = phoneNumber?.numberString
        
        // bind
        bindInputToViewModel()
        
        // action
        actionButtonNext()
        actionButtonClose()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.NumberVerification.title.UILocalize(lblTitle)
        LocKey.View.NumberVerification.buttonNext.UILocalize(btnNext)
        LocKey.View.NumberVerification.buttonClose.UILocalize(btnClose)
    }
    
    override func setUI() {
        super.setUI()
        
        txtPhoneNumber.becomeFirstResponder()
        setUIPhoneNumberTextField()
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var txtPhoneNumber: PhoneNumberTextField!
    @IBOutlet weak fileprivate var btnNext: UIButton!
    @IBOutlet weak fileprivate var btnClose: HigligtedButton!
}

// MARK: - Bind

extension VerificationViewController {
    
    private func bindInputToViewModel() {
        txtPhoneNumber.rx.text.orEmpty.bind(to: viewModel.phoneNumber).disposed(by: disposeBag)
    }
}

// MARK: - Action

extension VerificationViewController {
    
    private func actionButtonNext() {
        btnNext.rx.tap.bind(onNext: { [weak self] _ in
            self?.viewModel.actionSendVerifyCode().subscribe(onNext: { result in
                self?.performSegue(withIdentifier: SegueConstant.codeVerification, sender: result)
            }, onError: { error in
            guard let error = error as? NumberVerificationError else { return }
            switch error {
            case .phoneNumberNull:
                AlertManager.shared.showNoAction(title: LocKey.View.NumberVerification.alertNumberNull.localize, button: nil)
            case .firebaseError(let errorMessage):
                AlertManager.shared.showNoAction(title: errorMessage, button: nil)
            case .parseError(let errorMessage):
                AlertManager.shared.showNoAction(title: errorMessage, button: nil)
            case .unknown:
                AlertManager.shared.showNoAction(title: LocKey.View.NumberVerification.alertGeneralError.localize, button: nil)
            }
            }).disposed(by: (self?.disposeBag)!)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonClose() {
        btnClose.rx.tap.bind(onNext: { [weak self] _ in
            self?.dismiss(animated: true, completion: {
                IQKeyboardManager.shared.enable = true
                IQKeyboardManager.shared.enableAutoToolbar = true
            })
        }).disposed(by: disposeBag)
    }
}

// MARK: - UI

extension VerificationViewController {
    
    private func setUIPhoneNumberTextField() {
        if #available(iOS 11.0, *) {
            txtPhoneNumber.withDefaultPickerUI = true
        }
        txtPhoneNumber.withFlag = true
        txtPhoneNumber.withExamplePlaceholder = true
        txtPhoneNumber.withPrefix = true
        txtPhoneNumber.countryCodePlaceholderColor = UIColor(red: 17/255, green: 32/255, blue: 77/255, alpha: 255/255)
        txtPhoneNumber.numberPlaceholderColor = UIColor(red: 17/255, green: 32/255, blue: 77/255, alpha: 127/255)
        txtPhoneNumber.flagButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 5)
        txtPhoneNumber.layer.cornerRadius = 14
        txtPhoneNumber.layer.borderColor = UIColor(red: 17/255, green: 32/255, blue: 77/255, alpha: 255/255).cgColor
        txtPhoneNumber.layer.borderWidth = 2
    }
}

// MARK: - Segue

extension VerificationViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let viewController = segue.destination as? CodeVerificationViewController,
              let numberRegistrationResult = sender as? (phoneNumber: String, verificationId: String) else { return }
        viewController.phoneNumber.accept(numberRegistrationResult.phoneNumber)
        viewController.verificationId.accept(numberRegistrationResult.verificationId)
        viewController.delegate = delegate
    }
}
