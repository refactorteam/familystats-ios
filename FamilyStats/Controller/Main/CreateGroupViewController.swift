//
//  CreateGroupViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxCocoa
import CORELIB

class CreateGroupViewController: BaseViewController<CreateGroupViewModel> {
    
    var groupName: String?
    let trackingList = BehaviorRelay<[UserTrackingModel]>(value: [])
    var delegate: GroupCreateProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.createGroup.rawValue
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.CreateGroup.title.localize.UILocalize(lblTitle)
        txtGroupName.placeholder = LocKey.View.CreateGroup.txtGroupName.localize
        
        if groupName != nil {
            LocKey.View.CreateGroup.buttonRename.UILocalize(btnStartTrack)
        } else {
            LocKey.View.CreateGroup.buttonStart.UILocalize(btnStartTrack)
        }
    }
    
    override func setBinding() {
        super.setBinding()
        
        if groupName != nil {
            txtGroupName.text = groupName
            viewModel.isEdit = true
        }
        
        self.txtGroupName.becomeFirstResponder()
        
        // bind
        bindInputToViewModel()
        
        // action
        actionButtonCreateGroup()
        actionButtonClose()
        
        // observe
        observeSwitchStatus()
    }
    
    override func setUI() {
        super.setUI()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.set(view: self.viewAlert, hidden: false)
            self.set(view: self.viewBackground, hidden: false)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var txtGroupName: UITextField!
    @IBOutlet weak fileprivate var btnStartTrack: BorderedCornerButton!
    @IBOutlet weak fileprivate var createGroupNavigationItem: UINavigationItem!
    @IBOutlet weak fileprivate var btnClose: UIButton!
    @IBOutlet weak fileprivate var viewBackground: UIVisualEffectView!
    @IBOutlet weak fileprivate var viewAlert: UIView!
    
}

// MARK: - Action

extension CreateGroupViewController: LoaderShowable {
    
    private func actionButtonCreateGroup() {
        btnStartTrack.rx.tap.bind(onNext: { [weak self] _ in
            self?.showLoader()
            self?.viewModel.actionCreateGroup().subscribe(onNext: { status in
                self?.txtGroupName.resignFirstResponder()
                self?.hideLoader()
                if status != nil {
                    self?.delegate?.groupCreated()
                    self?.actionDismiss()
                }
            }).disposed(by: (self?.disposeBag)!)
        }).disposed(by: self.disposeBag)
    }
    
    private func actionButtonClose() {
        btnClose.rx.tap.bind(onNext: { [weak self] _ in
            self?.actionDismiss()
        }).disposed(by: disposeBag)
    }
    
    private func actionDismiss() {
        self.set(view: self.viewAlert, hidden: true)
        self.set(view: self.viewBackground, hidden: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Observe

extension CreateGroupViewController {
    
    private func observeSwitchStatus() {
        // self.viewModel.switchStatus.accept(AppSingleton.shared.userTrackingList.value.group?.onAlert == true)
    }
}

// MARK: - Bind

extension CreateGroupViewController {
    
    private func bindInputToViewModel() {
        
        trackingList.bind(to: viewModel.trackingList).disposed(by: disposeBag)
        txtGroupName.rx.text.orEmpty.bind(to: viewModel.groupName).disposed(by: disposeBag)
    }
}

// MARK: - View

extension CreateGroupViewController {

    func set(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
}
