//
//  VerificationNavigationViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import PhoneNumberKit

class VerificationNavigationViewController: UINavigationController {

    var phoneNumber: PhoneNumber?
    var verificationDelegate: NumberVerificationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let verificationViewController = UIStoryboard(name: StoryboardConstant.verification, bundle: nil).instantiateViewController(withIdentifier: VerificationViewController.nameOfClass) as? VerificationViewController else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        verificationViewController.delegate = verificationDelegate
        verificationViewController.phoneNumber = phoneNumber
        self.setViewControllers([verificationViewController], animated: false)
    }
}
