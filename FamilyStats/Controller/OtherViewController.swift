//
//  OtherViewController.swift
// FamilyStats
//
//  Created by Can Koç on 21.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import CORELIB
import RxSwift

class OtherViewController: BaseViewController<OtherViewModel>, InterstitialAdShowable, UITableViewDelegate {
    
    internal var nativeAdManager: NativeAdInterface?
    private var isAnimationCalled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.other.rawValue
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tblMenuList.deselectRows()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Required
    
    override func setBinding() {
        super.setBinding()
        
        // observe
        observeNativeAd()
        observePremiumStatus()
        
        // bind
        bindTableView()
        
        // action
        // actionButtonClose()
        actionButtonSubscribe()
        actionButtonBack()
    }
    
    override func setLocalization() {
        super.setLocalization()
        
        LocKey.View.Other.title.UILocalize(lblTitle)
        
        LocKey.View.Other.premiumTitle.UILocalize(lblPremiumTitle)
        LocKey.View.Other.premiumDescription.UILocalize(lblPremiumDescription)
        
        LocKey.TabBar.dashboard.UILocalize(btnTabMain)
        LocKey.TabBar.contactManager.UILocalize(lblContactManager)
        LocKey.TabBar.other.UILocalize(btnTabOther)
    }
    
    override func setUI() {
        super.setUI()
        
        tblMenuList.tableFooterView = UIView()
        
        tblMenuList.backgroundColor = .clear
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var tblMenuList: UITableView!
    
    @IBOutlet weak fileprivate var btnStats: UIButton!
    @IBOutlet weak fileprivate var lblPremiumTitle: UILabel!
    @IBOutlet weak fileprivate var lblPremiumDescription: UILabel!
    @IBOutlet weak fileprivate var btnSubscribe: BorderedCornerButton!
    @IBOutlet weak fileprivate var viewPremium: UIView!
    @IBOutlet weak fileprivate var constViewPremiumHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var viewAdContainer: UIView!
    
    @IBOutlet weak fileprivate var btnTabOther: UIButton!
    @IBOutlet weak fileprivate var btnTabMain: UIButton!
    @IBOutlet weak fileprivate var lblContactManager: UILabel!
}

// MARK: - Binding

extension OtherViewController {
    
    private func bindTableView() {
        let cellIdentifier = DefaultTableViewCell.nameOfClass
        tblMenuList.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        viewModel.otherMenuList.bind(to: tblMenuList.rx.items(cellIdentifier: cellIdentifier, cellType: DefaultTableViewCell.self)) { (_, model, cell) in
            cell.menuItem = model
        }.disposed(by: disposeBag)
        
        tblMenuList.rx.itemSelected.bind(onNext: { [weak self] indexPath in
            guard let cell = self?.tblMenuList.cellForRow(at: indexPath) as? DefaultTableViewCell,
                let menuItem = cell.menuItem else {
                return
            }
            
            switch menuItem.getType() {
            case .contact:
                self?.actionOpenContactPage()
            case .web:
                guard let urlString = menuItem.getURL(), let url = URL(string: urlString) else { return }
                let urlItem = StaticURLModel(title: menuItem.getTitle(), url: url)
                guard let webView = CORELIB.shared.getWebView(with: urlItem) else { return }
                self?.present(webView, animated: true, completion: nil)
            default: break
            }
        }).disposed(by: disposeBag)
    }
}

// MARK: - Action

extension OtherViewController {
    
    private func actionButtonSubscribe() {
        btnSubscribe.rx.tap.bind(onNext: { _ in
            CORELIB.shared.showLandingPage(for: .other, on: self)
        }).disposed(by: disposeBag)
    }
    
    private func actionOpenContactPage() {
        let contactViewController = CORELIB.shared.getContactViewController(with: ContactOptions(sendButtonBackgroundColor: .black, sendButtonTextColor: .white))
        self.present(contactViewController, animated: true, completion: nil)
    }
    
    private func actionButtonBack() {
        btnStats.rx.tap.bind(onNext: { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        }).disposed(by: disposeBag)
    }
}

// MARK: - Observe

extension OtherViewController {
    
    private func observePremiumStatus() {
        CORELIB.shared.observeSubscriptionStatus().subscribe(onNext: { [weak self] isPro in
            self?.viewPremium.isHidden = isPro
            self?.viewAdContainer.isHidden = isPro
            self?.constViewPremiumHeight.constant = isPro ? 0 : 100
        }).disposed(by: disposeBag)
    }
}

// MARK: - Segue

extension OtherViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        backItem.tintColor = .white
    }
}

// MARK: - View

extension OtherViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.tblMenuList.deselectRows()
    }
}

// MARK: - Native Ad

extension OtherViewController: NativeAdShowable {
    
    fileprivate func observeNativeAd() {
        nativeAdManager = nil
        nativeAdManager = createNativeAdManager(with: getNativeAdOptions(), delegate: nil)
        viewAdContainer.subviews.forEach({ $0.removeFromSuperview() })
        
        isNativeAdPossible(for: getNativeAdOptions()).subscribe(onNext: { [weak self] result in
            if result {
                self?.actionShowNativeAd()
            }
        }).disposed(by: disposeBag)
    }
    
    private func getNativeAdOptions() -> AdOptions {
        return AdOptions(viewController: self,
                                    adContainerView: viewAdContainer, adNibNameForFacebook: "FacebookNativeSmallView",
                                    adNibNameForGoogle: "AdMobNativeCellView", adSource: .main)
    }
    
    @objc private func actionShowNativeAd() {
        guard let nativeAdManager = self.nativeAdManager else { return }
        self.bindNativeAd(with: nativeAdManager, for: self.getNativeAdOptions()).disposed(by: disposeBag)
    }
}
