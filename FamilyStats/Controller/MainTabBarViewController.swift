//
//  MainTabBarViewController.swift
//  FamilyStats
//
//  Created by Can Koç on 5.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
            setUI()
        }
        
    private func setUI() {
        self.tabBar.tintColor = UIColor.black
    }
}
