import UIKit
import VisualEffectView

protocol CustomAlertProtocol {
	func okButtonTapped(type: Bool)
	func cancelButtonTapped()
}
class CustomAlertViewController: UIViewController {
	
	// MARK: - Outlet
	
	@IBOutlet weak var blurView: VisualEffectView!
	@IBOutlet weak var popView: UIView!
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var lblDescription: UILabel!
	@IBOutlet weak var lblOkTitle: UILabel!
	@IBOutlet weak var lblCancelTitle: UILabel!
	@IBOutlet weak var imgIcon: UIImageView!
	
	var titleText: String?
	var descriptionText: String?
	var okBtnTitle: String?
	var cancelBtnTitle: String?
	var imgIconName: String?
	var tapGesture = UITapGestureRecognizer()
	var type = false
	var delegate: CustomAlertProtocol?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setUI()
		setLocalization()
	}
	
	// MARK: - View
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.popView.alpha = 0
	}
	
	func setUI() {
		self.blurView.colorTint = .white
		self.blurView.colorTintAlpha = 0.2
		self.blurView.blurRadius = 5
		self.blurView.scale = 1
		
		tapGesture = UITapGestureRecognizer(target: self, action: #selector(CustomAlertViewController.viewTapped(_:)))
		tapGesture.numberOfTapsRequired = 1
		tapGesture.numberOfTouchesRequired = 1
		self.blurView.addGestureRecognizer(tapGesture)
		self.blurView.isUserInteractionEnabled = true
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
			self.popView.alpha = 1
			self.popView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
			UIView.animate(withDuration: 0.5,
						   delay: 0,
						   usingSpringWithDamping: 0.2,
						   initialSpringVelocity: 5.0,
						   options: .allowUserInteraction,
						   animations: {
							self.popView.transform = .identity
			},
						   completion: nil)
		}
	}
	
	@objc func viewTapped(_ sender: UITapGestureRecognizer) {
		self.dismiss(animated: true, completion: nil)
	}
	
	func setLocalization() {
		self.lblTitle.text = titleText
		self.lblTitle.font = Theme.Font.poppinsBold.of(size: 24)
		self.lblTitle.textColor = Theme.Color.navyBlue
		self.lblTitle.numberOfLines = 0
		
		self.lblDescription.text = descriptionText
		self.lblDescription.font = Theme.Font.poppinsRegular.of(size: 13)
		self.lblDescription.textColor = Theme.Color.navyBlue
		
		self.lblOkTitle.text = okBtnTitle
		self.lblOkTitle.font = Theme.Font.poppinsRegular.of(size: 16)
		self.lblOkTitle.textColor = Theme.Color.navyBlue
		
		self.lblCancelTitle.text = cancelBtnTitle
		self.lblCancelTitle.font = Theme.Font.poppinsRegular.of(size: 16)
		self.lblCancelTitle.textColor = Theme.Color.navyBlue
		
		self.imgIcon?.image = UIImage(named: imgIconName ?? "")
	}
	
	// MARK: - Action
	
	@IBAction func okButton(_ sender: Any) {
		self.delegate?.okButtonTapped(type: type)
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelButton(_ sender: Any) {
		self.delegate?.cancelButtonTapped()
		self.dismiss(animated: true, completion: nil)
	}
}
