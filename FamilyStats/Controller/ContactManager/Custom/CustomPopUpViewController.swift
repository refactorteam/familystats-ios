import UIKit
import VisualEffectView

protocol CustomPopUpProtocol {
	func doneButtonTapped(with status: Bool)
}

class CustomPopUpViewController: UIViewController {
	
	// MARK: - Outlets
	@IBOutlet weak var popView: UIView!
	@IBOutlet weak var blurView: VisualEffectView!
	@IBOutlet weak var btnDone: UIButton!
	@IBOutlet weak var lblShowAllNumber: UILabel!
	@IBOutlet weak var lblShowWONumber: UILabel!
	@IBOutlet weak var imgShowAllNumber: UIImageView!
	@IBOutlet weak var imgShowWONumber: UIImageView!
	
	var tapGesture = UITapGestureRecognizer()
	var selectionStatus = true
	var delegate: CustomPopUpProtocol?
	var isSelectedAllContact = true

	override func viewDidLoad() {
		super.viewDidLoad()
		setUI()
		setLocalization()
	}
	
	func setUI() {
		self.blurView.colorTint = .white
		self.blurView.colorTintAlpha = 0.2
		self.blurView.blurRadius = 5
		self.blurView.scale = 1
		popView.layer.cornerRadius = 40
		popView.clipsToBounds = true
		popView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
		
		tapGesture = UITapGestureRecognizer(target: self, action: #selector(CustomPopUpViewController.viewTapped(_:)))
		tapGesture.numberOfTapsRequired = 1
		tapGesture.numberOfTouchesRequired = 1
		self.blurView.addGestureRecognizer(tapGesture)
		self.blurView.isUserInteractionEnabled = true
		
		if isSelectedAllContact {
			selectionStatus = true
			self.imgShowAllNumber.image = UIImage(named: "iconSelection")
			self.imgShowWONumber.image = UIImage(named: "iconNoneSelect")
		} else {
			selectionStatus = false
			self.imgShowWONumber.image = UIImage(named: "iconSelection")
			self.imgShowAllNumber.image = UIImage(named: "iconNoneSelect")
		}
	}
	
	func setLocalization() {
		self.lblShowAllNumber.text = LocKey.View.AlertContacts.btnAllTitle.localize
		self.lblShowWONumber.text = LocKey.View.AlertContacts.woNumbers.localize
		LocKey.View.AlertContacts.buttonDoneTitle.localize.UILocalize(btnDone)
		btnDone.titleLabel?.adjustsFontSizeToFitWidth = true
		self.lblShowAllNumber.font = Theme.Font.poppinsRegular.of(size: 18)
		self.lblShowAllNumber.textColor = Theme.Color.navyBlue
		
		self.lblShowWONumber.font = Theme.Font.poppinsRegular.of(size: 18)
		self.lblShowWONumber.textColor = Theme.Color.navyBlue
		
	}
	
	@objc func viewTapped(_ sender: UITapGestureRecognizer) {
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: - Action
	
	@IBAction func showAllNumber(_ sender: Any) {
		self.imgShowAllNumber.image = UIImage(named: "iconSelection")
		self.imgShowWONumber.image = UIImage(named: "iconNoneSelect")
		selectionStatus = true
	}
	
	@IBAction func showWONumber(_ sender: Any) {
		self.imgShowAllNumber.image = UIImage(named: "iconNoneSelect")
		self.imgShowWONumber.image = UIImage(named: "iconSelection")
		selectionStatus = false
	}
	
	@IBAction func doneButtonTapped(_ sender: Any) {
		self.delegate?.doneButtonTapped(with: selectionStatus)
		self.dismiss(animated: true, completion: nil)
	}
}

