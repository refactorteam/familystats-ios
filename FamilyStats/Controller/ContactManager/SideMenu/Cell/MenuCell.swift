import UIKit

class MenuCell: UITableViewCell {
	
	// MARK: - Outlet
	
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var imgIcon: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
	func configure(with model: MenuItem) {
		lblTitle.text = model.title
		imgIcon.image = model.image
		
		lblTitle.textColor = Theme.Color.navyBlue
		lblTitle.font = Theme.Font.poppinsRegular.of(size: 16)
	}
}
