import SideMenu
import Contacts
import UIKit
import ContactsUI
import CORELIB

class SideMenuViewController: BaseViewController<BaseViewModel> {
	
	// MARK: - Outlet
	
	@IBOutlet weak var tableView: UITableView!
	var items = [MenuItem]()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.loadItems()
	}
	
	private func loadItems() {
		self.tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
		self.tableView.register(UINib(nibName: "LineCell", bundle: nil), forCellReuseIdentifier: "LineCell")
		self.tableView.register(UINib(nibName: "EmptyCell", bundle: nil), forCellReuseIdentifier: "EmptyCell")
		
		items = MenuItem.allCases
		self.tableView.reloadData()
	}
	
	override func setLocalization() {
		super.setLocalization()
		 
	}
	 
	func openVC(with viewController: UIViewController) {
		  self.navigationController?.pushViewController(viewController, animated: true)
	  }
}
