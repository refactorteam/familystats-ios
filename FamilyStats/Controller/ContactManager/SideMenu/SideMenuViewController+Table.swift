import Foundation
import UIKit
import CORELIB

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		let item = self.items[indexPath.row]
		switch item {
		case .line0:
			return 65.0
		default:
			return 65.0
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let item = self.items[indexPath.row]
		
		switch item {
		case .empty0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath) as! EmptyCell
			cell.selectionStyle = .none
			return cell
		case .line0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "LineCell", for: indexPath) as! LineCell
			cell.selectionStyle = .none
			return cell
		case .empty1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath) as! EmptyCell
			cell.selectionStyle = .none
			return cell
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
			cell.configure(with: item)
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		let item = self.items[indexPath.row]
		
		switch item {
		case .deletedContact:
			let deletedContact = UIStoryboard(name: StoryboardConstant.menu, bundle: nil).instantiateViewController(withIdentifier: DeletedViewController.nameOfClass) as! DeletedViewController
			self.openVC(with: deletedContact)
		case .stats:
			let statsVC = UIStoryboard(name: StoryboardConstant.menu, bundle: nil).instantiateViewController(withIdentifier: StatusViewController.nameOfClass) as! StatusViewController
			self.openVC(with: statsVC)
		case .mergeContact:
			let mergeVC = UIStoryboard(name: StoryboardConstant.menu, bundle: nil).instantiateViewController(withIdentifier: MergeContactViewController.nameOfClass) as! MergeContactViewController
			mergeVC.engagementScreen = SideMenuViewController.nameOfClass
			self.openVC(with: mergeVC)
		case .privacyPolicy:
            guard let privacyURL = CORELIB.shared.getPrivacyURL(), let webView = CORELIB.shared.getWebView(with: privacyURL) else { return }
            self.openVC(with: webView)
            return
		case .termsOfUse:
            guard let termsURL = CORELIB.shared.getTermsURL(), let webView = CORELIB.shared.getWebView(with: termsURL) else { return }
            self.openVC(with: webView)
            return
		case .contactUs:
            let contactViewController = CORELIB.shared.getContactViewController()
            self.openVC(with: contactViewController)
		default:
			break
		}
	}
}

