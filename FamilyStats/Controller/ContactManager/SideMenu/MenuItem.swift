import UIKit

enum MenuItem: CaseIterable {
	
	case empty0
	case stats
	case mergeContact
	case deletedContact
	case line0
	case contactUs
	case termsOfUse
	case privacyPolicy
	case empty1
	
	init?(index: Int) {
		guard let item = MenuItem.allCases.first(where: { $0.index == index }) else { return nil }
		self = item
	}
}

// MARK: - Helpers
extension MenuItem {
	
	var title: String? {
		switch self {
		case .empty0:
			return nil
		case .stats:
			return LocKey.View.SideMenu.stats.localize
		case .mergeContact:
			return LocKey.View.SideMenu.mergeContacts.localize
		case .deletedContact:
			return LocKey.View.SideMenu.deletedContacts.localize
		case .line0:
			return nil
		case .contactUs:
				return LocKey.View.SideMenu.contactUs.localize
		case .termsOfUse:
			return LocKey.View.SideMenu.terms.localize
		case .privacyPolicy:
			return LocKey.View.SideMenu.privacy.localize
		case .empty1:
			return nil
		}
	}
	
	var image: UIImage? {
		switch self {
		case .empty0:
			return nil
		case .stats:
			return #imageLiteral(resourceName: "iconMenuStats")
		case .mergeContact:
			return #imageLiteral(resourceName: "iconMenuMergeContact")
		case .deletedContact:
			return #imageLiteral(resourceName: "iconMenuDeletedContact")
		case .line0:
			return nil
		case .contactUs:
			return #imageLiteral(resourceName: "iconMenuTermsOfUse")
		case .termsOfUse:
			return #imageLiteral(resourceName: "iconMenuTermsOfUse")
		case .privacyPolicy:
			return #imageLiteral(resourceName: "iconMenuPrivacyPolicy")
		case .empty1:
			return nil
		}
	}
	
	var index: Int {
		switch self {
		case .empty0:
			return 0
		case .stats:
			return 1
		case .mergeContact:
			return 2
		case .deletedContact:
			return 3
		case .line0:
			return 4
		case .contactUs:
			return 5
		case .termsOfUse:
			return 6
		case .privacyPolicy:
			return 7
		case .empty1:
			return 8
		}
	}
}
