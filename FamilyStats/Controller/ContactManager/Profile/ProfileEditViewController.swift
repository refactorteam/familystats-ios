import UIKit
import ContactsUI
import CORELIB

class ProfileEditViewController: BaseViewController<BaseViewModel>, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	//MARK: - Outlets
	@IBOutlet weak var tblView: UITableView!
	@IBOutlet weak var imgPhoto: UIImageView!
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var saveButton: UIButton!
	
	var contact: CustomTestModel?
	var type: String?
	let store = CNContactStore()
	var imgProfile = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setUp()
	}
	
	func setUp() {
		if let imageData = contact?.image {
			imgProfile = true
			imgPhoto.image = UIImage(data: imageData)
		} else {
			imgProfile = false
			imgPhoto.image = UIImage(named: "iconMenuAvatar")
		}
		
		tblView.delegate = self
		tblView.dataSource = self
		tblView.register(UINib(nibName: ProfileEditCell.nameOfClass, bundle: nil), forCellReuseIdentifier: ProfileEditCell.nameOfClass)
		tblView.reloadData()
	}
	
	override func setLocalization() {
		super.setLocalization()
		LocKey.View.ProfileEdit.title.localize.UILocalize(lblTitle)
		LocKey.View.ProfileEdit.btnSaveTitle.localize.UILocalize(saveButton)
	}
	
	// MARK: - Action
	@IBAction func back(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func selectImgButton(_ sender: Any) {
		let picker = UIImagePickerController()
		picker.delegate = self
		picker.sourceType = .photoLibrary
		picker.allowsEditing = true
		self.present(picker, animated: true, completion: nil)
	}
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		self.imgPhoto.image = info[.editedImage] as? UIImage
		self.imgProfile = true
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func save(_ sender: Any) {
		
		let predicate = CNContact.predicateForContacts(withIdentifiers: [(contact?.contactId ?? "")])
		let keys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey,CNContactEmailAddressesKey,CNContactIdentifierKey,CNContactImageDataKey]
		
		do {
			let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keys as [CNKeyDescriptor])
			guard contacts.count > 0 else { print("No contacts found"); return }
			guard let contact = contacts.first else { return }
			
			let request = CNSaveRequest()
			let mutableContact = contact.mutableCopy() as! CNMutableContact
			
			if let textPhone = self.view.viewWithTag(999) as? UITextField {
				mutableContact.phoneNumbers = [CNLabeledValue(
					label:CNLabelPhoneNumberiPhone,
					value:CNPhoneNumber(stringValue:textPhone.text ?? ""))]
			}
			
			if let textName = self.view.viewWithTag(888) as? UITextField {
				mutableContact.givenName = textName.text ?? ""
			}
			
			if let textSurname = self.view.viewWithTag(666) as? UITextField {
				mutableContact.familyName = textSurname.text ?? ""
			}
			
			if let textEmail = self.view.viewWithTag(777) as? UITextField {
				if textEmail.text != "" && !(textEmail.text?.isEmpty ?? false) {
					let workEmail = CNLabeledValue(label:CNLabelOther, value:textEmail.text! as NSString)
					mutableContact.emailAddresses = [workEmail]
				}
			}
			if imgProfile {
				mutableContact.imageData = imgPhoto.image?.pngData()
			}
			
			request.update(mutableContact)
			do {
				try store.execute(request)
				
				print("The contact was successfully updated!")
				self.getContactFromAdressBook(with: self.contact?.contactId ?? "")
				
			} catch let err {
				print("[Error]: --> ", err.localizedDescription)
				self.navigationController?.popViewController(animated: true)
			}
		} catch let error {
			//            TODO: make and use an alert instead of just printing it to the console
			print(error.localizedDescription)
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	func getContactFromAdressBook(with identifier: String) {
		let predicate = CNContact.predicateForContacts(withIdentifiers: [(identifier)])
		let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactImageDataKey]
		
		do {
			let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keys as [CNKeyDescriptor])
			guard contacts.count > 0 else { print("No contacts found"); return }
			guard let newContact = contacts.first else { return }
			
			print(newContact)
			
			self.updateContact(with: newContact)
			
		} catch let error {
			print(error.localizedDescription)
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	func updateContact(with newContact: CNContact) {
        self.logEvent(eventRouter: EventRouter.contactEdit(logModel: ContactEditLogModel()))
		
		let cnContact = CustomTestModel(firstName: newContact.givenName, lastName: newContact.familyName, telephone: newContact.phoneNumbers.first?.value.stringValue ?? "", contactId: newContact.identifier, mail: (newContact.emailAddresses.first?.value ?? "") as String , status: StorageConstant.unknown, image: newContact.imageData, isSelectedContact: false)
		
		CacheManager.shared.contactContentUpdateWithCache(with: cnContact, type: self.type ?? StorageConstant.unknown)
		
        AppSingleton.shared.resetStatus = true
		
		self.navigationController?.popViewController(animated: true)
	}
}

// MARK: - Bind

extension ProfileEditViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 4
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: ProfileEditCell.nameOfClass, for: indexPath) as! ProfileEditCell
		if indexPath.row == 0 {
			cell.txtData.tag = 999
			cell.btnEdit.tag = 999
			cell.configure(with: contact?.telephone, tag: 999)
		} else if indexPath.row == 1 {
			cell.txtData.tag = 888
			cell.btnEdit.tag = 888
			cell.configure(with: contact?.firstName, tag: 888)
		}  else if indexPath.row == 2 {
			cell.txtData.tag = 666
			cell.btnEdit.tag = 666
			cell.configure(with: contact?.lastName, tag: 666)
		} else {
			cell.txtData.tag = 777
			cell.btnEdit.tag = 777
			cell.configure(with: contact?.mail, tag: 777)
		}
		return cell
	}
}
