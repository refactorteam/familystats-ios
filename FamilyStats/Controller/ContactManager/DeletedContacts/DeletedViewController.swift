import UIKit
import Contacts
import ContactsUI
import CORELIB

class DeletedViewController: BaseViewController<BaseViewModel>, Cacheable, CustomAlertProtocol {
	
	//MARK: - Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var buttonDelete: UIButton!
	@IBOutlet weak var lblNoData: UILabel!
	@IBOutlet weak var buttonUndo: UIButton!

	var contacts = [CustomTestModel]()
	let store = CNContactStore()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setUp()
	}
	
	func setUp() {
		tableView.delegate = self
		tableView.dataSource = self
		tableView.register(UINib(nibName: DeletedContactsTableViewCell.nameOfClass, bundle: nil), forCellReuseIdentifier: DeletedContactsTableViewCell.nameOfClass)
		
		if let data = UserDefaults.standard.data(forKey: StorageConstant.deleted) {
			self.lblNoData.isHidden = true
			self.contacts.removeAll()
			self.contacts = CacheManager.shared.getContacts(with: StorageConstant.deleted, data: data, nativeAd: false)
			
			if self.contacts.count == 0 {
				self.tableView.isHidden = true
				self.buttonDelete.isHidden = true
				self.buttonUndo.isHidden = true
				self.lblNoData.isHidden = false
			} else {
				self.tableView.reloadData()
			}
		} else {
			self.tableView.isHidden = true
			self.buttonDelete.isHidden = true
			self.buttonUndo.isHidden = true
			self.lblNoData.isHidden = false
		}
	}
	
	override func setLocalization() {
		super.setLocalization()
		LocKey.View.DeletedContacts.title.localize.UILocalize(titleLbl)
		LocKey.View.DeletedContacts.btnDeleteTitle.localize.UILocalize(buttonDelete)
		LocKey.View.DeletedContacts.noDataInfo.localize.UILocalize(lblNoData)
		LocKey.View.DeletedContacts.btnUndoTitle.localize.UILocalize(buttonUndo)
		buttonUndo.titleLabel?.adjustsFontSizeToFitWidth = true
		buttonDelete.titleLabel?.adjustsFontSizeToFitWidth = true
		self.lblNoData.font? = Theme.Font.poppinsRegular.of(size: 17)
		self.lblNoData.textColor = Theme.Color.darkSkyBlue
		
	}
	
	// MARK: - Action
	@IBAction func back(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}

// MARK: - Bind

extension DeletedViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.contacts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: DeletedContactsTableViewCell.nameOfClass, for: indexPath) as! DeletedContactsTableViewCell
		let model = self.contacts[indexPath.row]
		cell.btnRemove.addTarget(self, action: #selector(self.removeContact), for: .touchUpInside)
		cell.btnRestore.addTarget(self, action: #selector(self.restoreContact), for: .touchUpInside)
		cell.btnRemove.tag = indexPath.row
		cell.btnRestore.tag = indexPath.row
		cell.configure(with: model)
		return cell
	}
	
	@objc
	func removeContact(sender: UIButton) {
		if !checkPermission() {
			showAlert()
		} else {
			guard let ID = self.contacts[sender.tag].contactId else { return }
			self.logEvent(eventRouter: EventRouter.recycleBin(logMode: RecycleBinLogModel(recycleBinAction: "Delete")))
			print(ID as Any)
			self.removeContacts(with: ID)
			CacheManager.shared.removeContactInCache(with: self.contacts[sender.tag])
			self.contacts.remove(at: sender.tag)
			if self.contacts.count == 0 {
				self.tableView.isHidden = true
				self.buttonDelete.isHidden = true
				self.lblNoData.isHidden = false
				self.buttonUndo.isHidden = true
			} else {
				self.tableView.reloadData()
			}
		}
	}
	
	@objc
	func restoreContact(sender: UIButton) {
		if !checkPermission() {
			showAlert()
		} else {
			self.logEvent(eventRouter: EventRouter.recycleBin(logMode: RecycleBinLogModel(recycleBinAction: "Undo")))
			let ID = self.contacts[sender.tag].contactId
			print(ID as Any)
			CacheManager.shared.setContactsForUndo(with: self.contacts[sender.tag], type: StorageConstant.unknown)
			self.contacts.remove(at: sender.tag)
			if self.contacts.count == 0 {
				self.tableView.isHidden = true
				self.buttonDelete.isHidden = true
				self.buttonUndo.isHidden = true
				self.lblNoData.isHidden = false
			} else {
				self.tableView.reloadData()
			}
            AppSingleton.shared.resetStatus = true
		}
	}
	
	@IBAction func deleteAll(_ sender: Any) {
		if !checkPermission() {
			showAlert()
		} else {
			let vc =  UIStoryboard(name: StoryboardConstant.custom, bundle: nil).instantiateViewController(withIdentifier: CustomAlertViewController.nameOfClass) as! CustomAlertViewController
			vc.titleText = LocKey.View.DeletedContacts.alertTitle.localize
			vc.descriptionText = LocKey.View.DeletedContacts.alertDescripton.localize
			vc.okBtnTitle = LocKey.View.DeletedContacts.okBtnTitle.localize
			vc.cancelBtnTitle = LocKey.View.DeletedContacts.noBtnTitle.localize
			vc.imgIconName = "iconDeleteAll"
			vc.type = true
			vc.delegate = self
			self.present(vc, animated: true, completion: nil)
		}
	}
	
	@IBAction func undoAll(_ sender: Any) {
		if !checkPermission() {
			showAlert()
		} else {
			let vc =  UIStoryboard(name: StoryboardConstant.custom, bundle: nil).instantiateViewController(withIdentifier: CustomAlertViewController.nameOfClass) as! CustomAlertViewController
			vc.titleText = LocKey.View.DeletedContacts.alertTitleUndo.localize
			vc.descriptionText = LocKey.View.DeletedContacts.alertDescriptionUndo.localize
			vc.okBtnTitle = LocKey.View.DeletedContacts.okBtnTitleUndo.localize
			vc.cancelBtnTitle = LocKey.View.DeletedContacts.noBtnTitleUndo.localize
			vc.imgIconName = "iconReload"
			vc.delegate = self
			vc.type = false
			self.present(vc, animated: true, completion: nil)
		}
	}
	
	func checkPermission() -> Bool {
		switch CNContactStore.authorizationStatus(for: .contacts) {
		case .notDetermined:
			return false
		case .authorized:
			return true
		case .denied:
			return false
		default: return false
		}
	}
	
	func okButtonTapped(type: Bool) {
		if type {
			self.logEvent(eventRouter: EventRouter.recycleBin(logMode: RecycleBinLogModel(recycleBinAction: "Delete_all")))
			self.deleteAll()
		} else {
			self.logEvent(eventRouter: EventRouter.recycleBin(logMode: RecycleBinLogModel(recycleBinAction: "Undo_all")))
			for index in 0...self.contacts.count - 1 {
				CacheManager.shared.setContactsForUndo(with: self.contacts[index], type: StorageConstant.unknown)
			}
			self.contacts.removeAll()
			self.tableView.isHidden = true
			self.buttonDelete.isHidden = true
			self.lblNoData.isHidden = false
			self.tableView.reloadData()
            AppSingleton.shared.resetStatus = true
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	func cancelButtonTapped() {
		
	}
	
	func deleteAll() {
		if self.contacts.count > 0 {
			for i in 0...self.contacts.count - 1 {
				self.removeContacts(with: self.contacts[i].contactId!)
			}
		}
		self.contacts.removeAll()
		self.tableView.isHidden = true
		self.buttonDelete.isHidden = true
		self.buttonUndo.isHidden = true
		self.lblNoData.isHidden = false
        self.removeCache(for: StorageConstant.deleted)
	}
	
	func removeContacts(with identifier: String) {
		
		let predicate = CNContact.predicateForContacts(withIdentifiers: [(identifier)])
		
		let keys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey,CNContactEmailAddressesKey,CNContactIdentifierKey]
		
		do {
			let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keys as [CNKeyDescriptor])
			guard contacts.count > 0 else { print("No contacts found"); return }
			guard let contact = contacts.first else { return }
			
			let request = CNSaveRequest()
			let mutableContact = contact.mutableCopy() as! CNMutableContact
			
			request.delete(mutableContact)
			
			do {
				try store.execute(request)
				
				print("The contact was successfully updated!")
                AppSingleton.shared.resetStatus = true
			} catch let err {
				print("[Error]: --> ", err.localizedDescription)
			}
		} catch let error {
			print(error.localizedDescription)
		}
	}
	
	func showAlert() {
		let alert = UIAlertController(title: LocKey.View.Alert.contactPermissionTitle.localize, message: LocKey.View.Alert.contactPermissionDesc.localize, preferredStyle: .alert)
		
		let ok = UIAlertAction(title: LocKey.View.Alert.contactPermissionOk.localize, style: .default, handler: { action in
			UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
		})
		alert.addAction(ok)
		
		let cancel = UIAlertAction(title: LocKey.View.Alert.contactPermissionCancel.localize, style: .default, handler: { action in
			self.navigationController?.popViewController(animated: true)
		})
		alert.addAction(cancel)
		
		DispatchQueue.main.async(execute: {
			self.present(alert, animated: true)
		})
	}
}
