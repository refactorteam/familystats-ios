import UIKit

class MergeContactTableViewCell: UITableViewCell {
	
	@IBOutlet weak var mergeContactView: UIView!
	@IBOutlet weak var selectedImg: UIImageView!
	@IBOutlet weak var contactName: UILabel!
	
	public var isChecked: Bool = false
	
	override func awakeFromNib() {
		super.awakeFromNib()
		mergeContactView.layer.borderWidth = 1
		mergeContactView.layer.borderColor = Theme.Color.lightGray.cgColor
		mergeContactView.layer.cornerRadius = 4
		contactName.translatesAutoresizingMaskIntoConstraints = false
	}
	
	func configure() {
		self.contactName.textColor = Theme.Color.navyBlue
		self.contactName.font = Theme.Font.poppinsRegular.of(size: 13)
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
}


