import UIKit
import ContactsUI
import CORELIB

class MergeContactViewController: BaseViewController<BaseViewModel>, CustomAlertProtocol {
	
	// MARK: - Outlet
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var mergeButton: UIButton!
	@IBOutlet weak var descriptionLbl: UILabel!
	
	var contacts = [CustomTestModel]()
	let store = CNContactStore()
	var groupedContacts = [[CustomTestModel]]()
	var mergeContacts = [CustomTestModel]()
	var contact: CustomTestModel?
	var selectedItemCount = 0
	var engagementScreen: String? = ""

	override func viewDidLoad() {
		super.viewDidLoad()
		setUp()
	}
	
	func setUp() {
		tableView.register(UINib(nibName: MergeContactTableViewCell.nameOfClass, bundle: nil), forCellReuseIdentifier: MergeContactTableViewCell.nameOfClass)
		if checkPermission() {
			fetchContacts()
		} else {
			self.showAlert()
		}
	}
	
	func fetchContacts() {
		store.requestAccess(for: .contacts) { (granted, error) in
			if let error = error {
				print("failed to request access", error)
				self.showAlert()
			} else if UserDefaults.standard.data(forKey: StorageConstant.unknown) != nil {
				self.contacts.removeAll()
				self.contacts = CacheManager.shared.getContactsAll(with: false)
				if self.contacts.count > 0 {
					self.tableView.delegate = self
					self.tableView.dataSource = self
					self.sort(with: self.contacts) { _ in }
				}
			} else if granted {
				self.contacts.removeAll()
				let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactImageDataKey]
				let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
				do {
					try self.store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
						if let phone = contact.phoneNumbers.first?.value.stringValue {
							self.contacts.append(CustomTestModel(firstName: contact.givenName, lastName: contact.familyName, telephone: phone, contactId: contact.identifier, mail: contact.emailAddresses.first?.value as String? ?? "" as String, status: StorageConstant.unknown, image: contact.imageData, isSelectedContact: true))
						}
					})
				} catch let error {
					print("Failed to enumerate contact", error)
				}
				
				if self.contacts.count > 0 {
					self.tableView.delegate = self
					self.tableView.dataSource = self
					self.sort(with: self.contacts) { _ in }
				}
			} else {
				print("access denied")
			}
		}
	}
	
	func showAlert() {
		let alert = UIAlertController(title: LocKey.View.Alert.contactPermissionTitle.localize, message: LocKey.View.Alert.contactPermissionDesc.localize, preferredStyle: .alert)
		
		let ok = UIAlertAction(title: LocKey.View.Alert.contactPermissionOk.localize, style: .default, handler: { action in
			UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
		})
		alert.addAction(ok)
		
		let cancel = UIAlertAction(title: LocKey.View.Alert.contactPermissionCancel.localize, style: .default, handler: { action in
			self.navigationController?.popViewController(animated: true)
		})
		alert.addAction(cancel)
		
		DispatchQueue.main.async(execute: {
			self.present(alert, animated: true)
		})
	}
	
	func checkPermission() -> Bool {
		switch CNContactStore.authorizationStatus(for: .contacts) {
		case .notDetermined:
			return false
		case .authorized:
			return true
		case .denied:
			return false
		default: return false
		}
	}
	
	override func setLocalization() {
		super.setLocalization()
		LocKey.View.MergeContacts.title.localize.UILocalize(titleLbl)
		LocKey.View.MergeContacts.btnMergeTitle.localize.UILocalize(mergeButton)
		LocKey.View.MergeContacts.description.localize.UILocalize(descriptionLbl)
		
	}
	
	func sort(with model: [CustomTestModel], completion: @escaping (_ status: Bool) -> Void) {
		groupedContacts.removeAll()
		let groupedDict = Dictionary(grouping: model) { (contact) -> Character in
			return (contact.firstName?.first ?? "Z")
		}
		
		let keys = groupedDict.keys.sorted()
		
		keys.forEach({
			self.groupedContacts.append(groupedDict[$0]!)
		})
		
		self.tableView.reloadData()
		completion(true)
	}
	
	// MARK: - Action
	@IBAction func back(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func mergeButton(_ sender: Any) {
		if mergeContacts.count == 2 {
			let vc =  UIStoryboard(name: StoryboardConstant.custom, bundle: nil).instantiateViewController(withIdentifier: CustomAlertViewController.nameOfClass) as! CustomAlertViewController
			vc.titleText = LocKey.View.MergeContacts.mergeInfoTitle.localize
			let desc = LocKey.View.MergeContacts.mergeInfoDescription.localize
			vc.descriptionText = String(format: desc + " %@" + " %@" , mergeContacts[1].firstName ?? "", mergeContacts[0].firstName ?? "")
            vc.okBtnTitle = LocKey.View.MergeContacts.mergeInfoBtnOkTitle.localize
			vc.cancelBtnTitle = LocKey.View.MergeContacts.mergeInfoBtnCancelTitle.localize
			vc.imgIconName = "iconMergeContacts"
			
			vc.delegate = self
			self.present(vc, animated: true, completion: nil)
		}
	}
	
	func okButtonTapped(type: Bool) {
		self.logEvent(eventRouter: EventRouter.merge(logModel: MergeLogModel(engagementScreen: engagementScreen ?? "", actionStatus: "yes")))
		self.mergeContacts(with: true, identifier: self.mergeContacts.first?.contactId ?? "")
	}
	
	func cancelButtonTapped() {
		print("[Cancel]")
		self.logEvent(eventRouter: EventRouter.merge(logModel: MergeLogModel(engagementScreen: engagementScreen ?? "", actionStatus: "no")))
	}
	
	func mergeContacts(with isMerge: Bool, identifier: String) {
		
		let predicate = CNContact.predicateForContacts(withIdentifiers: [(identifier)])
		
		let keys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey,CNContactEmailAddressesKey,CNContactIdentifierKey]
		
		do {
			let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keys as [CNKeyDescriptor])
			guard contacts.count > 0 else { print("No contacts found"); return }
			guard let contact = contacts.first else { return }
			
			let request = CNSaveRequest()
			let mutableContact = contact.mutableCopy() as! CNMutableContact
			
			if isMerge {
				mutableContact.phoneNumbers.append(CNLabeledValue(
					label:CNLabelOther,
					value:CNPhoneNumber(stringValue:mergeContacts.last?.telephone ?? "")))
				
				request.update(mutableContact)
			} else {
				request.delete(mutableContact)
			}
			
			
			do {
				try store.execute(request)
				
				print("Successfully updated")
				
				if isMerge {
					self.mergeContacts(with: false, identifier: self.mergeContacts.last?.contactId ?? "")
				} else {
                    AppSingleton.shared.resetStatus = true
					self.contacts.removeAll()
					CacheManager.shared.updatCacheAfterMerge(with: mergeContacts.last?.contactId ?? "", type: mergeContacts.last?.status ?? "")
					
					self.contacts = CacheManager.shared.getContactsAll(with: false)
					self.sort(with: self.contacts) { _ in }
				}
			} catch let err {
				print("[Error]: --> ", err.localizedDescription)
			}
		} catch let error {
			print(error.localizedDescription)
			
		}
	}
}

extension MergeContactViewController: UITableViewDelegate, UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return groupedContacts.count > 0 ? groupedContacts.count : 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return groupedContacts[section].count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MergeContactTableViewCell", for: indexPath) as! MergeContactTableViewCell
		let item = groupedContacts[indexPath.section][indexPath.row]
		cell.translatesAutoresizingMaskIntoConstraints = false
		cell.configure()
		cell.contactName.text = "\(String(item.firstName ?? LocKey.View.ProfileEdit.placeHolderNoName.localize)) \(String(item.lastName ?? ""))"
		cell.selectedImg.isHidden = item.isSelectedContact ?? true
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let label = UILabel()
		if let name = groupedContacts[section].first?.firstName?.first {
			label.text = "   \(name)"
		}
		label.backgroundColor = UIColor(displayP3Red: 240.0/255.0, green: 243.0/255.0, blue: 248.0/255.0, alpha: 1.0)
		label.layer.cornerRadius = 4
		label.textColor = Theme.Color.navyBlue
		label.font = Theme.Font.poppinsRegular.of(size: 13)
		label.clipsToBounds = true
		label.layer.borderWidth = 1
		label.layer.borderColor = Theme.Color.lightGray.cgColor
		return label
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return groupedContacts.count == 0 ? 0 : 40
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if groupedContacts[indexPath.section][indexPath.row].isSelectedContact ?? true {
			if selectedItemCount < 2 {
				selectedItemCount += 1
				
				groupedContacts[indexPath.section][indexPath.row].isSelectedContact = false
				let item = self.groupedContacts[indexPath.section][indexPath.row]
				self.mergeContacts.append(item)
			} else {
				self.sort(with: self.contacts) { _ in
					self.mergeContacts.removeAll()
					self.selectedItemCount = 1
					self.groupedContacts[indexPath.section][indexPath.row].isSelectedContact = false
					let item = self.groupedContacts[indexPath.section][indexPath.row]
					self.mergeContacts.append(item)
				}
			}
		} else {
			selectedItemCount -= 1
			groupedContacts[indexPath.section][indexPath.row].isSelectedContact = true
			self.mergeContacts.remove(at: selectedItemCount)
		}
		tableView.reloadData()
	}
}
