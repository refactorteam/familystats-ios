import UIKit
import RxSwift
import RxCocoa
import Contacts
import ContactsUI
import CORELIB

class StatusViewController: BaseViewController<BaseViewModel> {
	
	//MARK: - Outlets
	@IBOutlet weak var tblStatus: UITableView!
	@IBOutlet weak var btnReset: UIButton!
	var items = [StatusModel]()
	@IBOutlet weak var lblTitle: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setUp()
	}
	
	// MARK: - Required
	override func setBinding() {
		super.setBinding()
		
		// action
		actionButtonReset()
	}
	
	func setUp() {
		tblStatus.delegate = self
		tblStatus.dataSource = self
		tblStatus.register(UINib(nibName: StatusCell.nameOfClass, bundle: nil), forCellReuseIdentifier: StatusCell.nameOfClass)
		tblStatus.register(UINib(nibName: StatusCellOther.nameOfClass, bundle: nil), forCellReuseIdentifier: StatusCellOther.nameOfClass)
		items = StatusModel.allCases
		self.tblStatus.reloadData()
	}
	
	override func setLocalization() {
		super.setLocalization()
		LocKey.View.Stats.title.localize.UILocalize(lblTitle)
		LocKey.View.Stats.btnResetTitle.localize.UILocalize(btnReset)
	}
	
	// MARK: - Action
	@IBAction func back(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	private func actionButtonReset() {
		btnReset.rx.tap.bind(onNext: { _ in
			if !self.checkPermission() {
				self.showAlert()
			} else {
				self.logEvent(eventRouter: EventRouter.stats(logModel: StatsLogModel(engagementScreen: StatusViewController.nameOfClass, actionStatus: "yes")))
				CacheManager.shared.resetContactTypes(with: AppSingleton.shared.isNativeAdForCards)
                AppSingleton.shared.resetStatus = true
				self.tblStatus.reloadData()
			}
		}).disposed(by: disposeBag)
	}
	
	func checkPermission() -> Bool {
		switch CNContactStore.authorizationStatus(for: .contacts) {
		case .notDetermined:
			return false
		case .authorized:
			return true
		case .denied:
			return false
		default: return false
		}
	}
	
	func showAlert() {
		let alert = UIAlertController(title: LocKey.View.Alert.contactPermissionTitle.localize, message: LocKey.View.Alert.contactPermissionDesc.localize, preferredStyle: .alert)
		
		let ok = UIAlertAction(title: LocKey.View.Alert.contactPermissionOk.localize, style: .default, handler: { action in
			UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
		})
		alert.addAction(ok)
		
		let cancel = UIAlertAction(title: LocKey.View.Alert.contactPermissionCancel.localize, style: .default, handler: { action in
			self.navigationController?.popViewController(animated: true)
		})
		alert.addAction(cancel)
		
		DispatchQueue.main.async(execute: {
			self.present(alert, animated: true)
		})
	}
}

// MARK: - Bind

extension StatusViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.items.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 150.0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if indexPath.row == 0 {
			let cell = tableView.dequeueReusableCell(withIdentifier: StatusCellOther.nameOfClass, for: indexPath) as! StatusCellOther
			let item = self.items[indexPath.row]
			cell.configureItem(with: item)
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: StatusCell.nameOfClass, for: indexPath) as! StatusCell
			let item = self.items[indexPath.row]
			cell.configureItem(with: item)
			return cell
		}
	}
}
