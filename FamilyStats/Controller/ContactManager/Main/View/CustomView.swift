import UIKit

class stackView: UIView {
	
	lazy var initialLabel: UILabel = {
		   let label = UILabel()
		   label.translatesAutoresizingMaskIntoConstraints = false
		   label.textAlignment = .center
		   label.clipsToBounds = true
		   label.layer.cornerRadius = 40
		   label.font = UIFont.systemFont(ofSize: 30, weight: .black)
		   label.textColor = Theme.Color.navyBlue
		   label.backgroundColor = UIColor(white: 0.1, alpha: 0.3)
		   return label
	   }()
	
	lazy var bgImage: UIImageView = {
		let image = UIImageView()
		image.translatesAutoresizingMaskIntoConstraints = false
		image.clipsToBounds = true
		image.image = #imageLiteral(resourceName: "iconHomeCardBg")
		image.contentMode = .scaleAspectFill
		return image
	}()
	
	lazy var profilImage: UIImageView = {
		let image = UIImageView()
		image.translatesAutoresizingMaskIntoConstraints = false
		image.clipsToBounds = true
		image.layer.cornerRadius = 40
		image.backgroundColor = .gray
		image.contentMode = .scaleAspectFill
		return image
	}()
	
	lazy var nameLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		label.clipsToBounds = true
		label.textColor = Theme.Color.navyBlue
		label.font = Theme.Font.poppinsBold.of(size: 22)
		label.text = ""
		label.numberOfLines = 1
		label.adjustsFontSizeToFitWidth = true
		label.minimumScaleFactor = 0.5
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	lazy var phoneNumberLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .left
		label.clipsToBounds = true
		label.textColor = Theme.Color.navyBlue
		label.font = Theme.Font.poppinsRegular.of(size: 16)
		label.text = ""
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	lazy var mailLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .left
		label.clipsToBounds = true
		label.textColor = Theme.Color.navyBlue
		label.font = Theme.Font.poppinsRegular.of(size: 14)
		label.text = ""
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	lazy var emailImage: UIImageView = {
		let image = UIImageView()
		image.image = #imageLiteral(resourceName: "mail")
		image.clipsToBounds = true
		image.layer.cornerRadius = 10
		image.translatesAutoresizingMaskIntoConstraints = false
		return image
	}()
	
	lazy var seperatorView: UIView = {
		let view = UIView()
		view.backgroundColor = Theme.Color.lightGray
		view.translatesAutoresizingMaskIntoConstraints = false
		view.clipsToBounds = true
		return view
	}()
	
//	lazy var moreButton: UIButton = {
//		let button = UIButton()
//		button.setTitle(LocKey.View.ContactManager.cardsMoreButtonTitle.localize, for: .normal)
//		button.setTitleColor(Theme.Color.navyBlue, for: .normal)
//		button.titleLabel?.font = Theme.Font.poppinsMedium.of(size: 16)
//		button.translatesAutoresizingMaskIntoConstraints = false
//		button.addTarget(self, action: #selector(moreTapped), for: .touchUpInside)
//		return button
//	}()
	
	lazy var moreLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		label.clipsToBounds = true
		label.textColor = Theme.Color.navyBlue
		label.font = Theme.Font.poppinsMedium.of(size: 16)
		label.text = LocKey.View.ContactManager.cardsMoreButtonTitle.localize
		label.numberOfLines = 0
		label.sizeToFit()
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	@objc func moreTapped() {
		print("More Button Tapped")
	}
	
	init(with name: String?, phone: String?, initial: String?, mail: NSString?, image: Data?) {
		super.init(frame: CGRect.zero)
		
		backgroundColor = .white
		layer.cornerRadius = 10
		clipsToBounds = true
		isOpaque = false
		
		initialize(name: name ?? "", phone: phone ?? "", initial: initial ?? "", mail: mail ?? "", image: image)
	}
	
	private func initialize(name: String, phone: String, initial: String, mail: NSString, image: Data?) {
		profilImage.isHidden = false
//		initialLabel.isHidden = true
//		initialLabel.text = initial
		
		if let imgData = image  {
			profilImage.image = UIImage(data: imgData)
		} else {
			profilImage.image = UIImage(named: "iconMenuAvatar")
		}
		nameLabel.text = name
		phoneNumberLabel.text = phone
		if mail != "" {
			mailLabel.text = "\(mail)"
		} else {
			mailLabel.text = LocKey.View.ContactManager.cardsNoEmailDesc.localize
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		return nil
	}
	
	override func layoutSubviews() {
		setupLayout()
	}
}

extension stackView {
	
	func setupLayout() {
		 
		addSubview(bgImage)
		
		NSLayoutConstraint.activate([
			bgImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
			bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -0),
			bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
			bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -44)
		])
		
		addSubview(initialLabel)
        
        NSLayoutConstraint.activate([
            initialLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 7),
            initialLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            initialLabel.widthAnchor.constraint(equalToConstant: 80),
            initialLabel.heightAnchor.constraint(equalTo: initialLabel.widthAnchor)
        ])
		
		addSubview(profilImage)

		NSLayoutConstraint.activate([
			  profilImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 7),
			  profilImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
			  profilImage.widthAnchor.constraint(equalToConstant: 80),
			  profilImage.heightAnchor.constraint(equalTo: initialLabel.widthAnchor)
		  ])
		
		addSubview(nameLabel)
		
		NSLayoutConstraint.activate([
			nameLabel.topAnchor.constraint(equalTo: initialLabel.bottomAnchor, constant: 10),
			nameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
			nameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
			nameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
			phoneNumberLabel.heightAnchor.constraint(equalToConstant: 27)
		])
		
		addSubview(phoneNumberLabel)
		
		NSLayoutConstraint.activate([
			phoneNumberLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
			phoneNumberLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
			phoneNumberLabel.heightAnchor.constraint(equalToConstant: 19)
		])
		
		addSubview(mailLabel)
		
		NSLayoutConstraint.activate([
			mailLabel.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor, constant: 30),
			mailLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
			mailLabel.heightAnchor.constraint(equalToConstant: 20)
			
		])
		
		addSubview(emailImage)
		
		NSLayoutConstraint.activate([
			emailImage.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor, constant: 30),
			emailImage.widthAnchor.constraint(equalToConstant: 20),
			emailImage.trailingAnchor.constraint(equalTo: mailLabel.leadingAnchor, constant: -10),
			emailImage.heightAnchor.constraint(equalToConstant: 20)
		])
		
		addSubview(seperatorView)
		
		NSLayoutConstraint.activate([
			seperatorView.topAnchor.constraint(equalTo: mailLabel.bottomAnchor, constant: 23),
			seperatorView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -1),
			seperatorView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 1),
			seperatorView.heightAnchor.constraint(equalToConstant: 1)
		])
		
//		addSubview(moreButton)
//
//		NSLayoutConstraint.activate([
//			moreButton.topAnchor.constraint(equalTo: seperatorView.bottomAnchor, constant: 10),
//			moreButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
//			moreButton.heightAnchor.constraint(equalToConstant: 20)
//		])
		
		addSubview(moreLabel)
			
			NSLayoutConstraint.activate([
				moreLabel.topAnchor.constraint(equalTo: seperatorView.bottomAnchor, constant: 10),
				moreLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
				moreLabel.heightAnchor.constraint(equalToConstant: 20)
			])
	}
}

extension UIColor {
	static var random: UIColor {
		return UIColor(red: .random(in: 0...1),
					   green: .random(in: 0...1),
					   blue: .random(in: 0...1),
					   alpha: 1.0)
	}
}
