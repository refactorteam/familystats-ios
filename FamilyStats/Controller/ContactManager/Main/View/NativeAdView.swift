import UIKit
import RxCocoa
import RxSwift

class nativeAdView: UIView {
	
	lazy var nameLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		label.clipsToBounds = true
		label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
		label.textColor = .black
		label.text = ""
		label.numberOfLines = 0
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	init(with test: String?) {
		super.init(frame: CGRect.zero)
		
		backgroundColor = .white
		layer.cornerRadius = 10
		clipsToBounds = true
		isOpaque = false
		
		initialize(test: test ?? "")
	}
	
	private func initialize(test: String) {
		nameLabel.text = test

	}
	
	required init?(coder aDecoder: NSCoder) {
		return nil
	}
	 
	override func layoutSubviews() {
		setupLayout()
	}
}


extension nativeAdView {
	
	func setupLayout() {
		
		addSubview(nameLabel)
		
		NSLayoutConstraint.activate([
			nameLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
			nameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
			nameLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            nameLabel.widthAnchor.constraint(equalToConstant: 200)
		])
		
	}
}
