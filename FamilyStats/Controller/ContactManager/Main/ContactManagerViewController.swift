//
//  ContactManagerViewController.swift
//  CoreLib
//
//  Created by Can KOÇ on 17.01.2022.
//  Copyright © 2022 Can KOÇ. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Firebase
import SideMenu
import ContactsUI
import PopBounceButton
import AVFoundation
import CORELIB

class ContactManagerViewController: BaseViewController<ContactManagerViewModel>, CustomPopUpProtocol, Cacheable {
	
	internal var isUpdated = false
	private var firstTimeInitialize = false
	
    internal var interstitialDisposer: Disposable?
    internal var nativeAdManager: NativeAdInterface?
    internal var nativeAdManagerDisposer: [Disposable] = []
    internal var nativeTimer: Timer?
    internal var nativeAdReturnCount: Int = 0
    internal var nativeAdManagerContact: NativeAdInterface?
    internal var nativeAdManagerContactDisposer: [Disposable] = []
    
	var contacts = [CustomTestModel]()
	var cardStack = SwipeCardStack()
	let store = CNContactStore()
	var woContact = false
	var contactCount = 0
	var woContactList = [CustomTestModel]()
	var player: AVAudioPlayer?
	var didTapSwipeButton = false

	var swipeCount = 0
	var deletedCount = 0
	var unknownCount = 0
	var totalContact = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
        self.screenName = EventScreenName.contactManager.rawValue
        
		self.setUp()
        
        /*if self.getBoolFromCache(for: StorageConstant.guideCompleted) == false {
            let viewController = UIStoryboard(name: StoryboardConstant.guide, bundle: nil).instantiateViewController(withIdentifier: GuideLineViewController.nameOfClass)
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }*/
	}
		
	override func setLocalization() {
		super.setLocalization()
        
		LocKey.View.ContactManager.title.localize.UILocalize(lblStats)
		LocKey.View.ContactManager.allDone.localize.UILocalize(allDoneLbl)
		LocKey.View.ContactManager.allDoneDescription.localize.UILocalize(allDoneDescription)
		LocKey.View.ContactManager.btnStartOver.localize.UILocalize(buttonStartOver)
		LocKey.View.ContactPermission.title.localize.UILocalize(contactPermissionTitle)
		LocKey.View.ContactPermission.description.localize.UILocalize(contactPermissionDescription)
		LocKey.View.ContactPermission.btnSettings.localize.UILocalize(settingsButton)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		didTapSwipeButton = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.shadowImage = UIImage()
	}
	
	// MARK: - Configure
    
	func setUp() {
		self.viewShow(with: true, viewAllCards: true, viewPermission: true)
		self.layoutCardStackViewForMain()
		cardStack.delegate = self
		cardStack.dataSource = self
		rightBarButtonItemMerge.imageInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -15)
		self.viewAllCards.isHidden = true
	}
	
	// MARK: - Binding
    
	override func setBinding() {
		super.setBinding()
        
        AppSingleton.shared.updateWithAppConfig()
        CORELIB.shared.startMainViewController(on: self, disposeBag)
        
		// action
        actionButtonBack()
        actionButtonDeletedContacts()
		actionButtonStartOver()
		actionButtonSettings()
        actionButtonStatistics()
		
		self.fetchContacts { _ in }
        
        // observe
        self.observeNativeAd()
	}
	 
	// MARK: - View
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
        
		if AppSingleton.shared.resetStatus {
            AppSingleton.shared.resetStatus = false
			if let data = UserDefaults.standard.data(forKey: StorageConstant.unknown) {
				self.contacts.removeAll()
				self.woContact = false
				self.contacts = CacheManager.shared.getContacts(with: StorageConstant.unknown, data: data, nativeAd: AppSingleton.shared.isNativeAdForCards)
				self.contactCount = self.contacts.count
				if contactCount > 0 {
					cardStack.reloadData()
					self.calculatorSwipeCount()
					self.viewShow(with: true, viewAllCards: false, viewPermission: true)
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
						self.viewStatus(with: self.contactCount)
					}
				} else {
					self.calculatorSwipeCount()
					self.viewShow(with: false, viewAllCards: true, viewPermission: true)
				}
			}
		}
	}
	
	override func setUI() {
		super.setUI()		
		cardView3.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
		cardView2.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
		cardView1.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
		
		viewTop.layer.borderWidth = 1.0
		viewTop.layer.borderColor = Theme.Color.lightGray.cgColor
		viewAdContainer.layer.borderWidth = 1.0
		viewAdContainer.layer.borderColor = Theme.Color.lightGray.cgColor
		viewStartOver.isHidden = false
	}
	
	func calculatorSwipeCount() {
		swipeCount = CacheManager.shared.getContactTypeCount(with: StorageConstant.swipe)
		deletedCount = CacheManager.shared.getContactTypeCount(with: StorageConstant.deleted)
		unknownCount = CacheManager.shared.getContactTypeCount(with: StorageConstant.unknown)
		
		setContactStatus(with: swipeCount, deleted: deletedCount, unknown: unknownCount)
	}
	
	func calculatorSwipeCountForWOContact() {
		swipeCount = CacheManager.shared.getContactTypeCountForWOContact(with: StorageConstant.swipe)
		deletedCount = CacheManager.shared.getContactTypeCountForWOContact(with: StorageConstant.deleted)
		unknownCount = CacheManager.shared.getContactTypeCountForWOContact(with: StorageConstant.unknown)
		
		setContactStatus(with: swipeCount, deleted: deletedCount, unknown: unknownCount)
	}
	
	func setContactStatus(with swipe: Int, deleted: Int, unknown: Int) {
		totalContact = unknownCount + swipeCount + deletedCount
		
		self.lblSwipeCount.text = String(swipe)
		self.lblDeletedCount.text = String(deleted)
		
		let totalSwipe = swipe + deleted
		let contactsCount = String(totalSwipe) + "/" + String(totalContact)
		
		self.lblContactsCount.text = contactsCount
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: - Outlets
	
	@IBOutlet weak internal var viewAdContainer: UIView!
	@IBOutlet weak internal var constViewAdHeight: NSLayoutConstraint!
	@IBOutlet internal var viewAdContainerBig: UIView!
	
	@IBOutlet weak var cardView3: UIView!
	@IBOutlet weak var cardView2: UIView!
	@IBOutlet weak var cardView1: UIView!
	@IBOutlet weak var viewTop: UIView!
	@IBOutlet weak var viewStartOver: UIView!
	@IBOutlet weak var viewAllCards: UIView!
	@IBOutlet weak var viewPermission: UIView!
	@IBOutlet weak var lblStats: UILabel!
	@IBOutlet weak var viewStartOverAndAllCards: UIView!
	
	@IBOutlet weak var lblContactsCount: UILabel!
	@IBOutlet weak var lblDeletedCount: UILabel!
	@IBOutlet weak var lblSwipeCount: UILabel!
	
	@IBOutlet weak var iconDelete: UIImageView!
	@IBOutlet weak var iconNotDelete: UIImageView!
	@IBOutlet weak var rightBarButtonItemMerge: UIBarButtonItem!

	@IBOutlet weak var allDoneLbl: UILabel!
	@IBOutlet weak var allDoneDescription: UILabel!
	@IBOutlet weak var buttonStartOver: UIButton!
	@IBOutlet weak var buttonDelete: UIButton!
	@IBOutlet weak var buttonSwipe: UIButton!
	@IBOutlet weak var buttonUndo: UIButton!
    @IBOutlet weak var btnDeletedContacts: UIButton!
    
	@IBOutlet weak var contactPermissionTitle: UILabel!
	@IBOutlet weak var settingsButton: UIButton!
	@IBOutlet weak var contactPermissionDescription: UILabel!
    @IBOutlet weak var btnStats: UIButton!
    @IBOutlet weak var btnBack: UIBarButtonItem!
}

extension ContactManagerViewController {
	
	// MARK: - Action
	
	private func actionButtonStartOver() {
		buttonStartOver.rx.tap.bind(onNext: { _ in
			if let data = CacheManager.shared.startOver(with: AppSingleton.shared.isNativeAdForCards) {
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
					if data.count > 0 {
						self.woContact = false
						self.contacts.removeAll()
						self.woContactList.removeAll()
						self.contacts = data
						self.contactCount = self.contacts.count
						self.calculatorSwipeCount()
						self.cardStack.reloadData()
						if self.contacts.count > 0 {
							self.viewShow(with: true, viewAllCards: false, viewPermission: true)
							DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
								self.viewStatus(with: self.contactCount)
							}
						} else {
							self.viewShow(with: false, viewAllCards: true, viewPermission: true)
						}
					}
				}
			}
		}).disposed(by: disposeBag)
	}
    
    private func actionButtonDeletedContacts() {
        btnDeletedContacts.rx.tap.bind(onNext: { [weak self] _ in
            let deletedContact = UIStoryboard(name: StoryboardConstant.menu, bundle: nil).instantiateViewController(withIdentifier: DeletedViewController.nameOfClass) as! DeletedViewController
            self?.navigationController?.pushViewController(deletedContact, animated: true)
        }).disposed(by: disposeBag)
    }
    
    private func actionButtonStatistics() {
        btnStats.rx.tap.bind(onNext: { [weak self] _ in
            let stats = UIStoryboard(name: StoryboardConstant.menu, bundle: nil).instantiateViewController(withIdentifier: StatusViewController.nameOfClass)
            self?.navigationController?.pushViewController(stats, animated: true)
        }).disposed(by: disposeBag)
    }
	
	private func actionButtonSettings() {
		settingsButton.rx.tap.bind(onNext: { _ in
			UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
		}).disposed(by: disposeBag)
	}
    
    private func actionButtonBack() {
        btnBack.rx.tap.bind(onNext: { [weak self] _ in
            self?.navigationController?.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
	
	@IBAction func didTapUndoButton(_ sender: Any) {
		didTapSwipeButton = true
		if self.woContact {
			if woContactList.count > 0 {
				cardStack.undoLastSwipe(animated: true)
			}
		} else {
			if self.contacts.count > 0 {
				cardStack.undoLastSwipe(animated: true)
			}
		}
		print("[didTapUndoButton]")
	}
	
	@IBAction func didTapCancelButton(_ sender: Any) {
		didTapSwipeButton = true
		cardStack.swipe(.left, animated: true)
		self.iconAnimation(with: self.iconDelete)
		print("[didTapCancelButton]")
	}
	
/// Removed
//	@IBAction func didTapFavButton(_ sender: Any) {
//		cardStack.swipe(.up, animated: true)
//		self.iconAnimation(with: self.iconFavorite)
//		print("[didTapCancelButton]")
//	}
	
	@IBAction func didTapOkButton(_ sender: Any) {
		didTapSwipeButton = true
		cardStack.swipe(.right, animated: true)
		self.iconAnimation(with: self.iconNotDelete)
		print("[didTapCancelButton]")
	}
	
	func iconAnimation(with img: UIImageView) {
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
			img.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
			
			UIView.animate(withDuration: 0.5,
						   delay: 0,
						   usingSpringWithDamping: 0.2,
						   initialSpringVelocity: 5.0,
						   options: .allowUserInteraction,
						   animations: {
							img.transform = .identity
			},	
						   completion: nil)
		}
	}
	
	@IBAction func mergeButtonAction(_ sender: Any) {
		if !self.checkPermission() {
			self.showAlert()
		} else {
			let mergeVC = UIStoryboard(name: StoryboardConstant.menu, bundle: nil).instantiateViewController(withIdentifier: MergeContactViewController.nameOfClass) as! MergeContactViewController
			mergeVC.engagementScreen = MainViewController.nameOfClass
			self.navigationController?.pushViewController(mergeVC, animated: true)
		}
	}
	
	@IBAction func filterButonAction(_ sender: Any) {
		if !self.checkPermission() {
			self.showAlert()
		} else {
			let vc =  UIStoryboard(name: StoryboardConstant.custom, bundle: nil).instantiateViewController(withIdentifier: CustomPopUpViewController.nameOfClass) as! CustomPopUpViewController
			vc.delegate = self
			
			if woContactList.count > 0 {
				vc.isSelectedAllContact = false
			} else {
				vc.isSelectedAllContact = true
			}
			self.present(vc, animated: true, completion: nil)
		}
	}
	
	func doneButtonTapped(with status: Bool) {
		if status {
			self.getWOContact(with: false)
		} else {
			self.getWOContact(with: true)
		}
	}
}

extension ContactManagerViewController {
	
	func getWOContact(with status: Bool) {

		if status {

			if let data = UserDefaults.standard.data(forKey: StorageConstant.unknown) {
				let woList = CacheManager.shared.getContactsForWOContact(with: StorageConstant.unknown, data: data, nativeAd: AppSingleton.shared.isNativeAdForCards)
				
				if woList.count > 0 {
					woContactList.removeAll()
					woContactList = woList
					self.contacts.removeAll()
					self.woContact = true
					self.contactCount = self.woContactList.count
					self.cardStack.reloadData()
					self.calculatorSwipeCountForWOContact()
					self.viewShow(with: true, viewAllCards: false, viewPermission: false)
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
							self.viewStatus(with: self.contactCount)
					}
				} else {
					self.showAlertWoContact(with: LocKey.View.Alert.woContactAlertTitle.localize, Desc: LocKey.View.Alert.woContactAlertDesc.localize, okButton: LocKey.View.Alert.woContactAlertButtonTitle.localize)
				}
			}
		} else {
			if let data = UserDefaults.standard.data(forKey: StorageConstant.unknown) {
				let list = CacheManager.shared.getContacts(with: StorageConstant.unknown, data: data, nativeAd: AppSingleton.shared.isNativeAdForCards)
	
				if list.count > 0 {
					self.contacts.removeAll()
					contacts = list
					self.contactCount = self.contacts.count
					self.woContactList.removeAll()
					self.woContact = false
					self.viewShow(with: true, viewAllCards: false, viewPermission: false)
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
							 	self.viewStatus(with: self.contactCount)
					}
					self.cardStack.reloadData()
					self.calculatorSwipeCount()
				} else {
					self.showAlertWoContact(with: LocKey.View.Alert.allContactAlertTitle.localize, Desc: LocKey.View.Alert.allContactAlertTitle.localize, okButton: LocKey.View.Alert.allContactAlertButtonTitle.localize)
				}
			}
		}
	}
	
	
	func showAlertWoContact(with title: String, Desc: String, okButton: String) {
		let alert = UIAlertController(title: title, message: Desc, preferredStyle: .alert)
		
		let ok = UIAlertAction(title: okButton, style: .default, handler: { action in

		})
		alert.addAction(ok)
		DispatchQueue.main.async(execute: {
			self.present(alert, animated: true)
		})
	}
	
	func checkPermission() -> Bool {
		switch CNContactStore.authorizationStatus(for: .contacts) {
		case .notDetermined:
			return false
		case .authorized:
			return true
		case .denied:
			return false
		default: return false
		}
	}
	
	func showAlert() {
		let alert = UIAlertController(title: LocKey.View.Alert.contactPermissionTitle.localize, message: LocKey.View.Alert.contactPermissionDesc.localize, preferredStyle: .alert)
		
		let ok = UIAlertAction(title: LocKey.View.Alert.contactPermissionOk.localize, style: .default, handler: { action in
			UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
		})
		alert.addAction(ok)
		
		let cancel = UIAlertAction(title: LocKey.View.Alert.contactPermissionCancel.localize, style: .default, handler: { action in
		})
		alert.addAction(cancel)
		
		DispatchQueue.main.async(execute: {
			self.present(alert, animated: true)
		})
	}
}
