//
//  ContactManagerNavigationViewController.swift
// FamilyStats
//
//  Created by Can KOÇ on 1.10.2022.
//  Copyright © 2022 Can Koç. All rights reserved.
//

import UIKit

class ContactManagerNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.backgroundColor = .clear
        navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.backgroundColor = .clear
        navigationBar.shadowImage = UIImage()
    }
}
