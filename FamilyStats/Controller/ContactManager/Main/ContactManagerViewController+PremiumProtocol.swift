//
//  MainViewController+PremiumProtocol.swift
//  SmartLens
//
//  Created by Can KOÇ on 13.04.2022.
//  Copyright © 2022 Can Koç. All rights reserved.
//

import Foundation
import CORELIB

// MARK: - PremiumProtocol

extension ContactManagerViewController: PremiumProtocol {
	
	func paymentSuccess() {
		self.constViewAdHeight.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
	}
	
	func paymentFail() {}
	func paymentCancel() {}
}
