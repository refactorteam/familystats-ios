import Foundation
import UIKit

public struct FetchedContact: Equatable {
    var firstName: String
    var lastName: String
    var telephone: String
    var contactId: String
    var mail: NSString
    var image: Data

    public static func ==(lhs:FetchedContact, rhs:FetchedContact) -> Bool { // Implement Equatable
        return lhs.contactId == rhs.contactId
    }
}
