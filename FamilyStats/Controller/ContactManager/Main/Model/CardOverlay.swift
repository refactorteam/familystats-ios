import UIKit

class CardOverlay: UIView {
	
	init(direction: SwipeDirection) {
		super.init(frame: .zero)
		switch direction {
		case .left:
			createLeftOverlay()
		case .up:
			createUpOverlay()
		case .right:
			createRightOverlay()
		default:
			break
		}
	}
	
	required init?(coder: NSCoder) {
		return nil
	}
	
	private func createLeftOverlay() {
		let leftTextView = TinderCardOverlayLabelView(withTitle: LocKey.View.GuideLine.deleteInfo.localize, color: .sampleRed, rotation: 0)
		addSubview(leftTextView)
		leftTextView.backgroundColor = .white
		leftTextView.anchor(top: topAnchor, right: rightAnchor, paddingTop: 15, paddingRight: 40)
	}
	
	private func createUpOverlay() {
		let upTextView = TinderCardOverlayLabelView(withTitle: LocKey.View.GuideLine.favoriteInfo.localize, color: .sampleYellow, rotation: 0)
		addSubview(upTextView)
		upTextView.backgroundColor = .white
		upTextView.translatesAutoresizingMaskIntoConstraints = false
		upTextView.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 10).isActive = true
		upTextView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
	}
	
	private func createRightOverlay() {
		let rightTextView = TinderCardOverlayLabelView(withTitle: LocKey.View.GuideLine.swipeInfo.localize,
													   color: .sampleGreen,
													   rotation: 0)
		addSubview(rightTextView)
		rightTextView.backgroundColor = .white
		rightTextView.anchor(top: topAnchor, left: leftAnchor, paddingTop: 15, paddingLeft: 40)
	}
}

private class TinderCardOverlayLabelView: UIView {
	
	private let titleLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		return label
	}()
	
	init(withTitle title: String, color: UIColor, rotation: CGFloat) {
		super.init(frame: CGRect.zero)
		self.frame.size = CGSize(width: titleLabel.frame.width + 10, height: 15)
		layer.borderColor = color.cgColor
		layer.borderWidth = 1
		layer.cornerRadius = 4
		transform = CGAffineTransform(rotationAngle: rotation)
		
		addSubview(titleLabel)
		titleLabel.textColor = color
		titleLabel.text = title
		titleLabel.font = Theme.Font.poppinsMedium.of(size: 20)
		
		titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 8)
	}
	
	required init?(coder aDecoder: NSCoder) {
		return nil
	}
}

extension UIColor {
	static var sampleGreen = UIColor(red: 89/255, green: 225/255, blue: 140/255, alpha: 1)
	static var sampleRed = UIColor(red: 249/255, green: 111/255, blue: 109/255, alpha: 1)
	static var sampleYellow = UIColor(red: 243/255, green: 209/255, blue: 112/255, alpha: 1)
}

extension UIView {
	func anchor(top: NSLayoutYAxisAnchor? = nil,
				left: NSLayoutXAxisAnchor? = nil,
				bottom: NSLayoutYAxisAnchor? = nil,
				right: NSLayoutXAxisAnchor? = nil,
				paddingTop: CGFloat = 0,
				paddingLeft: CGFloat = 0,
				paddingBottom: CGFloat = 0,
				paddingRight: CGFloat = 0,
				width: CGFloat = 0,
				height: CGFloat = 0) -> [NSLayoutConstraint] {
		translatesAutoresizingMaskIntoConstraints = false
		
		var anchors = [NSLayoutConstraint]()
		
		if let top = top {
			anchors.append(topAnchor.constraint(equalTo: top, constant: paddingTop))
		}
		if let left = left {
			anchors.append(leftAnchor.constraint(equalTo: left, constant: paddingLeft))
		}
		if let bottom = bottom {
			anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom))
		}
		if let right = right {
			anchors.append(rightAnchor.constraint(equalTo: right, constant: -paddingRight))
		}
		if width > 0 {
			anchors.append(widthAnchor.constraint(equalToConstant: width))
		}
		if height > 0 {
			anchors.append(heightAnchor.constraint(equalToConstant: height))
		}
		
		anchors.forEach({$0.isActive = true})
		
		return anchors
	}
}
