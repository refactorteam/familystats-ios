//
//  ContactManagerViewController+Ad.swift
// FamilyStats
//
//  Created by Can KOÇ on 13.04.2022.
//  Copyright © 2022 Can Koç. All rights reserved.
//

import Foundation
import RxSwift
import CORELIB

// MARK: - Interstitial Ad

extension ContactManagerViewController: InterstitialAdShowable, InterstitialAdLoadingProtocol, NativeAdShowable {
    
    func interstitialAdDidFinish(with state: Bool) {
    }
    
    // MARK: - Native Contact
    internal func observeNativeAdForContact() {
        nativeAdManagerContact = nil
        nativeAdManagerContact = createNativeAdManager(with: getAdOptionsForContact(), delegate: nil)
        nativeAdManagerContactDisposer = []
        viewAdContainerBig.removeAllSubviews()
        
        nativeAdManagerContactDisposer.append(isNativeAdPossible(for: getAdOptionsForContact()).subscribe(onNext: { [weak self] result in
            if result {
                guard let nativeAdManager = self?.nativeAdManagerContact,
                      let adOptions = self?.getAdOptionsForContact() else { return }
                self?.nativeAdManagerContactDisposer.append((self?.bindNativeAd(with: nativeAdManager, for: adOptions))!)
            }
        }))
    }
    
    private func createNativeAdManagerForContact() {
        nativeAdManagerContact = createNativeAdManager(with: getAdOptionsForContact(), delegate: nil)
    }
    
    private func getAdOptionsForContact() -> AdOptions {
        return AdOptions(viewController: self,
                         adContainerView: self.viewAdContainerBig,
                         adNibNameForFacebook: "FacebookNativeBigCellView", adNibNameForGoogle: "AdMobNativeBigCellView",
                         adSource: .appSpecific(ShowAdPageEnum.contactCard.rawValue))
    }
    
    
	//For left swipe
	internal func actionShowInterstitialForLeftSwipe() {
        interstitialDisposer = showInterstitialAd(on: self, for: .appSpecific(ShowAdPageEnum.leftSwipe.rawValue))
	}
	
	
	//For right swipe
	internal func actionShowInterstitialForRightSwipe() {
        interstitialDisposer = showInterstitialAd(on: self, for: .appSpecific(ShowAdPageEnum.rightSwipe.rawValue))
	}
    
    func interstitialAdDidFinished(withState: Bool) {
        DispatchQueue.main.async {
            self.interstitialDisposer?.dispose()
        }
    }
    
    // MARK: - Native Main
    
    internal func observeNativeAd() {
        nativeAdManager = nil
        nativeAdManager = createNativeAdManager(with: getNativeAdOptions(), delegate: nil)
        nativeAdManagerDisposer = []
        viewAdContainer.subviews.forEach({ $0.removeFromSuperview() })
        
        nativeAdManagerDisposer.append(isNativeAdPossible(for: getNativeAdOptions()).subscribe(onNext: { [weak self] result in
            if result {
                if let timerInterval = self?.getNativeAdTimer(), timerInterval > 0 {
                    self?.nativeTimer = Timer.scheduledTimer(timeInterval: TimeInterval(timerInterval), target: self!,
                                                       selector: #selector((self?.actionShowNativeAd)!), userInfo: nil, repeats: true)
                    self?.nativeTimer?.fire()
                } else {
                    self?.actionShowNativeAd()
                }
            }
        }))
    }
    
    private func getNativeAdOptions() -> AdOptions {
        return AdOptions(viewController: self,
                                    adContainerView: viewAdContainer, adNibNameForFacebook: "FacebookNativeCellView",
                                    adNibNameForGoogle: "AdMobNativeCellView", adSource: .main)
    }
    
    @objc private func actionShowNativeAd() {
        guard nativeAdReturnCount < getNativeAdMaxReturnCount(), let nativeAdManager = self.nativeAdManager else {  return }
        self.nativeAdManagerDisposer.append(self.bindNativeAd(with: nativeAdManager, for: self.getNativeAdOptions()))
        nativeAdReturnCount += 1
    }
}
