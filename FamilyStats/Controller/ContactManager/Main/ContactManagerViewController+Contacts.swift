import Foundation
import UIKit
import ContactsUI

extension ContactManagerViewController {
	
	func layoutCardStackViewForMain() {
		cardView1.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
		
		view.addSubview(cardStack)
		cardStack.isUserInteractionEnabled = true
		cardStack.translatesAutoresizingMaskIntoConstraints = false
		cardStack.centerYAnchor.constraint(equalTo: cardView1.centerYAnchor).isActive = true
		cardStack.leadingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.leadingAnchor, constant: -10).isActive = true
		cardStack.trailingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
		cardStack.heightAnchor.constraint(equalToConstant: 285).isActive = true
	}
	
	func fetchContacts(completion: @escaping (_ status: Bool) -> Void) {
		contacts.removeAll()
		store.requestAccess(for: .contacts) { (granted, error) in
			if let error = error {
				print("failed to request access", error)
				self.viewShow(with: true, viewAllCards: true, viewPermission: false)
				completion(true)
				return
			}
			if let data = UserDefaults.standard.data(forKey: StorageConstant.unknown) {
				self.contacts.removeAll()
				self.contacts = CacheManager.shared.getContacts(with: StorageConstant.unknown, data: data, nativeAd: AppSingleton.shared.isNativeAdForCards)
				self.contactCount = self.contacts.count
				if self.contacts.count > 0 {
					self.viewShow(with: true, viewAllCards: false, viewPermission: true)
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
						self.viewStatus(with: self.contactCount)
					}
				} else {
					self.viewShow(with: false, viewAllCards: false, viewPermission: true)
				}
				self.cardStack.reloadData()
				self.calculatorSwipeCount()
				self.updateCache()
				completion(true)
			} else if granted {
				DispatchQueue.main.async {
					var cnContact = [CustomTestModel]()
					let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactImageDataKey]
					let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
					do {
						try self.store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
							
							cnContact.append(CustomTestModel(firstName: contact.givenName, lastName: contact.familyName, telephone: contact.phoneNumbers.first?.value.stringValue ?? "", contactId: contact.identifier, mail: contact.emailAddresses.first?.value as String? ?? "" as String, status: StorageConstant.unknown, image: contact.imageData))
						})


					} catch let error {
						print("Failed to enumerate contact", error)
					}
					 
					let sortedContact = cnContact.sorted { (initial, next) -> Bool in
						  guard initial.firstName != nil else { return true }
						  guard next.firstName != nil else { return true }
						  return initial.firstName!.compare(next.firstName!) == .orderedAscending
					}
					
					if AppSingleton.shared.isNativeAdForCards && sortedContact.count > 0 {
						for index in 0...sortedContact.count - 1 {
							if (index % AppSingleton.shared.modNativeAds == 0) && (self.contacts.count > 0) {
								self.contacts.append(CustomTestModel(firstName: "", lastName: "", telephone: "", contactId: "", mail: "", status: StorageConstant.ads, image: nil))
								
								self.contacts.append(CustomTestModel(firstName: sortedContact[index].firstName, lastName: sortedContact[index].lastName, telephone: sortedContact[index].telephone, contactId: sortedContact[index].contactId, mail: sortedContact[index].mail, status: StorageConstant.unknown, image: sortedContact[index].image))
							} else {
								self.contacts.append(CustomTestModel(firstName: sortedContact[index].firstName, lastName: sortedContact[index].lastName, telephone: sortedContact[index].telephone, contactId: sortedContact[index].contactId, mail: sortedContact[index].mail, status: StorageConstant.unknown, image: sortedContact[index].image))
							}
						}
						self.contactCount = self.contacts.count
						
					} else {
						self.contacts = sortedContact
						self.contactCount = self.contacts.count
					}
					
					CacheManager.shared.saveContacts(with: self.contacts, type: StorageConstant.unknown)
					if self.contacts.count > 0 {
						self.viewShow(with: true, viewAllCards: false, viewPermission: true)
						DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
							self.viewStatus(with: self.contactCount)
						}
					} else {
						self.viewShow(with: false, viewAllCards: false, viewPermission: true)
					}
					self.calculatorSwipeCount()
					self.cardStack.reloadData()
					completion(true)
				}
			} else {
				self.viewShow(with: true, viewAllCards: true, viewPermission: false)
				completion(true)
				print("access denied")
			}
		}
	}
	
	func updateCache() {
		var newContact = [CustomTestModel]()
		var contactUpdated  = [CustomTestModel]()
		
		DispatchQueue.main.async {
			let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactImageDataKey]
			let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
			let list = CacheManager.shared.getContactsAll(with: AppSingleton.shared.isNativeAdForCards)
			var update = false
			do {
				
				try self.store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
					var control = false
					if list.count > 0 {
						for index in 0...list.count - 1 {
							if list[index].contactId == contact.identifier {
								control = true
								contactUpdated.append(CustomTestModel(firstName: contact.givenName, lastName: contact.familyName, telephone: contact.phoneNumbers.first?.value.stringValue ?? "", contactId: contact.identifier, mail: contact.emailAddresses.first?.value as String? ?? "" as String, status: list[index].status, image: contact.imageData))
								break
							}
						}
						
						if control == false {
							update = true
							newContact.append(CustomTestModel(firstName: contact.givenName, lastName: contact.familyName, telephone: contact.phoneNumbers.first?.value.stringValue ?? "", contactId: contact.identifier, mail: contact.emailAddresses.first?.value as String? ?? "" as String, status: StorageConstant.unknown, image: contact.imageData))
						}
						
					} else {
						update = true
						newContact.append(CustomTestModel(firstName: contact.givenName, lastName: contact.familyName, telephone: contact.phoneNumbers.first?.value.stringValue ?? "", contactId: contact.identifier, mail: contact.emailAddresses.first?.value as String? ?? "" as String, status: StorageConstant.unknown, image: contact.imageData))
					}
				})
				
				CacheManager.shared.updateContactInCache(with: contactUpdated) { _ in
					if update == true {
						CacheManager.shared.updateCacheForNewContact(with: newContact, type: StorageConstant.unknown) { _ in
							self.getData()
						}
					} else {
						self.getData()
					}
				}
			} catch let error {
				print("Failed to enumerate contact", error)
			}
		}
	}
	
	func getData() {
		self.contacts.removeAll()
		if let data = UserDefaults.standard.data(forKey: StorageConstant.unknown) {
			self.contacts = CacheManager.shared.getContacts(with: StorageConstant.unknown, data: data, nativeAd: AppSingleton.shared.isNativeAdForCards)
		}
		self.contactCount = self.contacts.count
		if self.contacts.count > 0 {
			self.viewShow(with: true, viewAllCards: false, viewPermission: true)
		} else {
			self.viewShow(with: false, viewAllCards: false, viewPermission: true)
		}
		self.cardStack.reloadData()
		self.calculatorSwipeCount()
	}
 
	func viewShow(with startOver: Bool, viewAllCards: Bool, viewPermission: Bool) {
		self.buttonSwipe.isEnabled = true
		self.buttonUndo.isEnabled = true
		self.buttonDelete.isEnabled = true

		self.buttonDelete.isUserInteractionEnabled = true
		self.buttonUndo.isUserInteractionEnabled = true
		self.buttonSwipe.isUserInteractionEnabled = true

		if !startOver {
			self.setButtonIsUserInteractionEnabled(with: false)
			UIView.animate(withDuration: 0.3, animations: {
				self.viewStartOver.alpha = 1.0
				self.viewTop.alpha = 1.0
				self.lblStats.alpha = 1.0
				self.viewPermission.alpha = 0.0
				self.viewAllCards.alpha = 0.0
			}, completion: {(_) -> Void in })
		} else if !viewAllCards {
			self.setButtonIsUserInteractionEnabled(with: true)
			UIView.animate(withDuration: 0.3, animations: {
				self.viewTop.alpha = 1.0
				self.lblStats.alpha = 1.0
				self.viewAllCards.alpha = 1.0
				self.viewPermission.alpha = 0.0
				self.viewStartOver.alpha = 0.0
				self.viewStatus(with: self.contactCount)
			}, completion: {(_) -> Void in	})
		} else if !viewPermission {
			UIView.animate(withDuration: 0.3, animations: {
				self.viewPermission.alpha = 1.0
				self.viewTop.alpha = 0.0
				self.lblStats.alpha = 0.0
				self.viewAllCards.alpha = 0.0
				self.viewStartOver.alpha = 0.0
				self.buttonSwipe.isEnabled = false
				self.buttonUndo.isEnabled = false
				self.buttonDelete.isEnabled = false
				self.buttonDelete.isUserInteractionEnabled = false
				self.buttonUndo.isUserInteractionEnabled = false
				self.buttonSwipe.isUserInteractionEnabled = false
			}, completion: {(_) -> Void in })
		}
		
		if !startOver {
			self.viewStartOver.isHidden = false
			self.viewTop.isHidden = false
			self.lblStats.isHidden = false
			self.viewPermission.isHidden = true
			self.viewAllCards.isHidden = true
			cardStack.isHidden = true
			viewStartOverAndAllCards.isHidden = false
		} else if !viewAllCards {
			self.viewTop.isHidden = false
			self.lblStats.isHidden = false
			self.viewAllCards.isHidden = false
			self.viewPermission.isHidden = true
			self.viewStartOver.isHidden = true
			cardStack.isHidden = false
			viewStartOverAndAllCards.isHidden = false
		} else if !viewPermission {
			self.viewPermission.isHidden = false
			self.viewTop.isHidden = true
			self.lblStats.isHidden = true
			self.viewAllCards.isHidden = true
			self.viewStartOver.isHidden = true
			cardStack.isHidden = true
			viewStartOverAndAllCards.isHidden = true
		} else {
			self.viewStartOver.isHidden = true
			self.viewTop.isHidden = true
			self.lblStats.isHidden = true
			self.buttonSwipe.isEnabled = false
			self.buttonUndo.isEnabled = false
			self.buttonDelete.isEnabled = false
			self.buttonDelete.isUserInteractionEnabled = false
			self.buttonUndo.isUserInteractionEnabled = false
			self.buttonSwipe.isUserInteractionEnabled = false
		}
	}
}

