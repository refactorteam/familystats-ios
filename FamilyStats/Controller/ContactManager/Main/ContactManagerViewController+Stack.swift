import Foundation
import UIKit
import ContactsUI
import AVFoundation
import CORELIB

// MARK: - Bind

extension ContactManagerViewController: Hapticable, SwipeCardStackDelegate, SwipeCardStackDataSource {
	
	func numberOfCards(in cardStack: SwipeCardStack) -> Int {
		return self.contactCount
	}
	
	func cardStack(_ cardStack: SwipeCardStack, cardForIndexAt index: Int) -> SwipeCard {
		let card = SwipeCard()
		card.swipeDirections = [.left, .right]
		
		var model = CustomTestModel()
		if woContact {
			if woContactList.count > 0 {
				model = self.woContactList[index]
			}
		} else {
			model = self.contacts[index]
		}
		
		/// if native ads  active
		if model.status == StorageConstant.ads && AppSingleton.shared.isNativeAdForCards {
            self.observeNativeAdForContact()
            card.content = self.viewAdContainerBig
		} else {
			card.content = stackView(with: "\(model.firstName ?? LocKey.View.ProfileEdit.placeHolderNoName.localize) \(model.lastName ?? "")", phone: model.telephone ?? "", initial: "\(String(model.firstName?.initials ?? ""))\(String(model.lastName?.initials ?? ""))", mail: (model.mail ?? "") as NSString, image: model.image)
			
			for direction in card.swipeDirections {
				card.setOverlay(CardOverlay(direction: direction), forDirection: direction)
			}
		}
        
        card.model = model
		return card
	}
	
	//MARK: - Did select
	func cardStack(_ cardStack: SwipeCardStack, didSelectCardAt index: Int) {
		
		var model = CustomTestModel()
		if woContact {
			model = self.woContactList[index]
		} else {
			model = self.contacts[index]
		}
		
		if model.status == StorageConstant.ads { return }
		
		let edit = UIStoryboard(name: StoryboardConstant.contactManager, bundle: nil).instantiateViewController(withIdentifier: ProfileEditViewController.nameOfClass) as! ProfileEditViewController
		edit.contact = model
		edit.type = model.status
		self.navigationController?.pushViewController(edit, animated: true)
	}
	
	//MARK: - Did undo cards
	func cardStack(_ cardStack: SwipeCardStack, didUndoCardAt index: Int, from direction: SwipeDirection) {
		
		var model = CustomTestModel()
		if woContact {
			model = self.woContactList[index]
		} else {
			model = self.contacts[index]
		}
		
		self.viewStatus(with: ((self.contactCount - 1) - index) + 1)

		if model.status == StorageConstant.ads {
			self.logEvent(eventRouter: EventRouter.buttonSwipe(logModel: ButtonSwipeLogModel(buttonSwipeAction: "Undo", cardType: "Ad")))
			return
		}
		self.logEvent(eventRouter: EventRouter.buttonSwipe(logModel: ButtonSwipeLogModel(buttonSwipeAction: "Undo", cardType: "Contact")))
		self.playSound(with: "undo")
		
		if woContact {
			self.undoOptimization(with: self.woContactList[index])
			self.woContactList[index].status = StorageConstant.unknown
		} else {
			self.undoOptimization(with: self.contacts[index])
			self.contacts[index].status = StorageConstant.unknown
		}
	}
	
	func undoOptimization(with model: CustomTestModel) {
		unknownCount += 1
		if model.status == StorageConstant.swipe && swipeCount > 0 {
			swipeCount -= 1
		} else if model.status == StorageConstant.deleted && deletedCount > 0 {
			deletedCount -= 1
		}
		
		setContactStatus(with: swipeCount, deleted: deletedCount, unknown: unknownCount)
		CacheManager.shared.setContactsForUndo(with: model, type: StorageConstant.unknown)
		//self.viewShow(with: true, viewAllCards: false, viewPermission: true)
	}
	
	//MARK: - Did swipe cards
	func cardStack(_ cardStack: SwipeCardStack, didSwipeCardAt index: Int, with direction: SwipeDirection) {
		self.createHaptic(with: .medium)
		
		var model = CustomTestModel()
		if woContact {
			model = self.woContactList[index]
		} else {
			model = self.contacts[index]
		}

		self.viewStatus(with: (self.contactCount - 1) - index)

		if model.status == StorageConstant.ads {
            
            if let card = self.cardStack.loadCard(at: index) {
                card.removeAllSubviews()
            }
            
			switch direction {
			case .left:
				if didTapSwipeButton {
					self.logEvent(eventRouter: EventRouter.buttonSwipe(logModel: ButtonSwipeLogModel(buttonSwipeAction: "Delete", cardType: "Ad")))
				} else {
					self.logEvent(eventRouter: EventRouter.contactSwipe(logModel: ContactSwipeLogModel(swipeAction: "Delete", cardType: "Ad")))
				}
				self.actionShowInterstitialForLeftSwipe()
			case .right:
				if didTapSwipeButton {
					self.logEvent(eventRouter: EventRouter.buttonSwipe(logModel: ButtonSwipeLogModel(buttonSwipeAction: "Keep", cardType: "Ad")))
				} else {
					self.logEvent(eventRouter: EventRouter.contactSwipe(logModel: ContactSwipeLogModel(swipeAction: "Keep", cardType: "Ad")))
				}
				self.actionShowInterstitialForRightSwipe()
			default:
				break
			}
			return
		}
		
		var contactStatus = StorageConstant.unknown
		var selectedContact = CustomTestModel()
		unknownCount -= 1
		switch direction {
		case .left:
			self.playSound(with: "delete")
			contactStatus = StorageConstant.deleted
			if woContact {
				self.woContactList[index].status = StorageConstant.deleted
				selectedContact = self.woContactList[index]
			} else {
				self.contacts[index].status = StorageConstant.deleted
				selectedContact = self.contacts[index]
			}
			
			deletedCount += 1
			setContactStatus(with: swipeCount, deleted: deletedCount, unknown: unknownCount)
			self.iconAnimation(with: self.iconDelete)
			
			if didTapSwipeButton {
				self.logEvent(eventRouter: EventRouter.buttonSwipe(logModel: ButtonSwipeLogModel(buttonSwipeAction: "Delete", cardType: "Contact")))
			} else {
				self.logEvent(eventRouter: EventRouter.contactSwipe(logModel: ContactSwipeLogModel(swipeAction: "Delete", cardType: "Contact")))
			}
		case .right:
			self.playSound(with: "keep")
			contactStatus = StorageConstant.swipe
			if woContact {
				self.woContactList[index].status = StorageConstant.swipe
				selectedContact = self.woContactList[index]
			} else {
				self.contacts[index].status = StorageConstant.swipe
				selectedContact = self.contacts[index]
			}
			swipeCount += 1
			setContactStatus(with: swipeCount, deleted: deletedCount, unknown: unknownCount)
			self.iconAnimation(with: self.iconNotDelete)
			
			if didTapSwipeButton {
				self.logEvent(eventRouter: EventRouter.buttonSwipe(logModel: ButtonSwipeLogModel(buttonSwipeAction: "Keep", cardType: "Contact")))
			} else {
				self.logEvent(eventRouter: EventRouter.contactSwipe(logModel: ContactSwipeLogModel(swipeAction: "Keep", cardType: "Contact")))
			}
		default:
			break
		}
		
		didTapSwipeButton = false
		DispatchQueue.main.async {
			CacheManager.shared.setContacts(with: selectedContact, type: contactStatus)
		}
	}
	
	func viewStatus(with index: Int) {
		if index == 0 {
            logEvent(eventRouter: EventRouter.contactCompleted)
			self.viewStartOver.isHidden = false
			self.viewStartOver.alpha = 1.0
			self.setButtonIsUserInteractionEnabled(with: false)
			UIView.animate(withDuration: 0.3, animations: {
				self.cardView3.alpha = 0.0
				self.cardView2.alpha = 0.0
				self.cardView1.alpha = 1.0
			}, completion: {(_) -> Void in
				self.cardStack.isHidden = true
			})
		} else if index == 1 {
			self.setButtonIsUserInteractionEnabled(with: true)
			self.viewStartOver.isHidden = false
			self.viewStartOver.alpha = 1.0
			UIView.animate(withDuration: 0.3, animations: {
				self.cardView3.alpha = 0.0
				self.cardView2.alpha = 0.0
				self.cardView1.alpha = 1.0
			}, completion: {(_) -> Void in
				self.cardStack.isHidden = false
			})
		} else if index == 2 {
			self.setButtonIsUserInteractionEnabled(with: true)
			self.viewStartOver.isHidden = true
			self.viewStartOver.alpha = 0.0
			UIView.animate(withDuration: 0.3, animations: {
				self.cardView3.alpha = 0.0
				self.cardView2.alpha = 1.0
				self.cardView1.alpha = 1.0
			}, completion: {(_) -> Void in
				self.cardStack.isHidden = false
			})
		} else {
			self.setButtonIsUserInteractionEnabled(with: true)
			UIView.animate(withDuration: 0.3, animations: {
				self.cardView3.alpha = 1.0
				self.cardView2.alpha = 1.0
				self.cardView1.alpha = 1.0
			}, completion: {(_) -> Void in
			})
		}
	}
	
	func setButtonIsUserInteractionEnabled(with isActive: Bool) {
		self.buttonUndo.isUserInteractionEnabled = true
		if isActive {
			self.buttonSwipe.isEnabled = true
			self.buttonDelete.isEnabled = true
		} else {
			self.buttonSwipe.isEnabled = false
			self.buttonDelete.isEnabled = false
		}
	}
	
	func playSound(with sound: String) {
		guard let url = Bundle.main.url(forResource: sound, withExtension: "wav") else { return }
		
		do {
			try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
			try AVAudioSession.sharedInstance().setActive(true)
			
			player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
			guard let player = player else { return }
			player.volume = 0.5
			player.play()
			
		} catch let error {
			print(error.localizedDescription)
		}
	}
}
