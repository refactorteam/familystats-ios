import UIKit

enum GuideItem: CaseIterable {
	
	case swipe
	case deleted
	case undo
	
	init?(index: Int) {
		guard let item = GuideItem.allCases.first(where: { $0.index == index }) else { return nil }
		self = item
	}
}

// MARK: - Helpers
extension GuideItem {
	
	var name: String? {
		switch self {
		case .swipe:
			return LocKey.View.GuideLine.cardName.localize
		case .deleted:
			return  LocKey.View.GuideLine.cardName.localize
		case .undo:
			return LocKey.View.GuideLine.cardName.localize
		}
	}
	
	var surname: String? {
		switch self {
		case .swipe:
			return LocKey.View.GuideLine.cardSurname.localize
		case .deleted:
			return LocKey.View.GuideLine.cardSurname.localize
		case .undo:
			return LocKey.View.GuideLine.cardSurname.localize
		}
	}
	
	var email: String? {
		switch self {
		case .swipe:
			return LocKey.View.GuideLine.cardEmail.localize
		case .deleted:
			return LocKey.View.GuideLine.cardEmail.localize
		case .undo:
			return LocKey.View.GuideLine.cardEmail.localize
		}
	}
	
	var telephone: String? {
		switch self {
		case .swipe:
			return LocKey.View.GuideLine.cardPhone.localize
		case .deleted:
			return LocKey.View.GuideLine.cardPhone.localize
		case .undo:
			return LocKey.View.GuideLine.cardPhone.localize
		}
	}
	
	var image: UIImage? {
		switch self {
		case .swipe:
			return UIImage(named: "iconStatusSwipe")
		case .deleted:
			return UIImage(named: "iconStatusSwipe")
		case .undo:
            return UIImage(named: "iconStatusSwipe")
		}
	}
	
	var index: Int {
		switch self {
		case .swipe:
			return 0
		case .deleted:
			return 1
		case .undo:
			return 2
		}
	}
}
