import UIKit
import ContactsUI
import CORELIB

class GuideLineViewController: BaseViewController<BaseViewModel>, UICollectionViewDelegate, UICollectionViewDataSource,
                               UICollectionViewDelegateFlowLayout, guideLineCellProtocol, guideLineDeleteCellProtocol, guideLineUndoCellProtocol,
                               Cacheable {
	
	//MARK: - Outlets
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var continueButton: UIButton!
	let store = CNContactStore()

	var items = [GuideItem]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setUp()
	}
	
	override func setLocalization() {
		super.setLocalization()
	}
    
    override func setUI() {
        super.setUI()
    }
	
	func setUp() {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .horizontal
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		layout.sectionInset = .zero
		layout.itemSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height)
		self.collectionView.collectionViewLayout = layout
		self.collectionView.register(UINib(nibName: GuideLineCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: GuideLineCell.nameOfClass)
		self.collectionView.register(UINib(nibName: GuideLineDeleteCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: GuideLineDeleteCell.nameOfClass)
		self.collectionView.register(UINib(nibName: GuideLineUndoCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: GuideLineUndoCell.nameOfClass)
		
		items = GuideItem.allCases
		self.collectionView.reloadData()
		
		// action
		self.actionbtnContunie()
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.items.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if indexPath.row == 0 {
			let cell: GuideLineCell = collectionView.dequeueReusableCell(withReuseIdentifier: GuideLineCell.nameOfClass, for: indexPath) as! GuideLineCell
			let model = items[indexPath.row]
			cell.delegate = self
			cell.configure(with: model)
			return cell
		} else if  indexPath.row == 1 {
			let cell: GuideLineDeleteCell = collectionView.dequeueReusableCell(withReuseIdentifier: GuideLineDeleteCell.nameOfClass, for: indexPath) as! GuideLineDeleteCell
			let model = items[indexPath.row]
			cell.delegate = self
			cell.configure(with: model)
			return cell
		} else {
			let cell: GuideLineUndoCell = collectionView.dequeueReusableCell(withReuseIdentifier: GuideLineUndoCell.nameOfClass, for: indexPath) as! GuideLineUndoCell
			let model = items[indexPath.row]
			cell.delegate = self
			cell.configure(with: model)
			return cell
		}
	}
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x >= 0 {
			print("translation: \(scrollView.panGestureRecognizer.translation(in: scrollView.superview).x)")
			print("scrollView: \(String(describing: scrollView.frame))")
		} else {
			guard let item = self.collectionView.indexPathsForVisibleItems.last?.item else { return }
			guard (item + 1) < self.items.count else {
				// self.home()
				return
			}
		}
	}
	
	func home() {
		self.openViewController(with: StoryboardConstant.contactManager, viewController: ContactManagerViewController.nameOfClass, option: .transitionFlipFromRight)
	}
	
	func openViewController(with storyBoardName: String, viewController: String, option: UIView.AnimationOptions) {
		
		DispatchQueue.main.async {
			guard let window = UIApplication.shared.keyWindow else {
				return
			}
			
			let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
			let vc = storyboard.instantiateViewController(withIdentifier: viewController)
			let nav = UINavigationController(rootViewController: vc)
			
			UIApplication.shared.delegate?.window??.rootViewController = nav
			window.rootViewController = nav
			let options: UIView.AnimationOptions = option
			let duration: TimeInterval = 0.3
			UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
				{ completed in
					
			})
		}
	}
	
	func setScroll() {
		guard let item = self.collectionView.indexPathsForVisibleItems.first?.item else { return }
		guard (item + 1) < self.items.count else {
			self.fetchContacts()
			return
		}
		self.collectionView.scrollToItem(at: .init(item: item + 1, section: 0), at: .centeredHorizontally, animated: true)
	}
	
	func fetchContacts() {
		store.requestAccess(for: .contacts) { (granted, error) in
			if let error = error {
				print("failed to request access", error)
                self.saveToCache(with: true, for: StorageConstant.guideCompleted)
				self.home()
			}  else if granted {
                self.saveToCache(with: true, for: StorageConstant.guideCompleted)
				self.home()
			} else {
				print("access denied")
                self.saveToCache(with: true, for: StorageConstant.guideCompleted)
				self.home()
			}
		}
	}
	
	private func actionbtnContunie() {
		continueButton.rx.tap.bind(onNext: { _ in
			guard let item = self.collectionView.indexPathsForVisibleItems.first?.item else { return }
			self.nextAnimation(with: item)
		}).disposed(by: disposeBag)
	}
	
	func nextAnimation(with index: Int) {
		if index == 0 {
			NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didTapNext"), object: nil)
		} else if index == 1 {
			NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didTapNextDelete"), object: nil)
		} else {
			NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didTapNextUndo"), object: nil)
		}
	}
	
	// MARK: - Action
	
	func didTapSwipeButton() {
		self.setScroll()
	}
	
	func didTapDeleteButton() {
		self.setScroll()
	}
	
	func didTapUndoButton() {
		self.setScroll()
	}
}
