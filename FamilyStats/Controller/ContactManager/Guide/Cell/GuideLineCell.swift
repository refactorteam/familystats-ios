import UIKit
import VisualEffectView

protocol guideLineCellProtocol: class {
	func didTapSwipeButton()
}

class GuideLineCell: UICollectionViewCell, SwipeCardStackDelegate, SwipeCardStackDataSource {
	
	// MARK: - Outlet
	
	@IBOutlet weak var cardView3: UIView!
	@IBOutlet weak var cardView2: UIView!
	@IBOutlet weak var cardView1: UIView!
	var cardStack = SwipeCardStack()
	var items: GuideItem?
	@IBOutlet weak var swipeButton: UIButton!
    weak var delegate: guideLineCellProtocol?
    @IBOutlet weak var blurView: VisualEffectView?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		NotificationCenter.default.addObserver(self, selector: #selector(swipe(notification:)), name:NSNotification.Name(rawValue: "didTapNext"), object: nil)

		DispatchQueue.main.async {
			self.cardView3.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			self.cardView2.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			self.cardView1.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			
			self.blurView?.colorTint = .white
			self.blurView?.colorTintAlpha = 0.2
			self.blurView?.blurRadius = 5
			self.blurView?.scale = 1
			
			self.layoutCardStackView()
		}
    }

	func configure(with model: GuideItem) {
		self.items = model
		self.cardStack.delegate = self
		self.cardStack.dataSource = self
		self.cardStack.reloadData()
	}

	func layoutCardStackView() {
        self.addSubview(cardStack)
		cardStack.translatesAutoresizingMaskIntoConstraints = false
		cardStack.centerYAnchor.constraint(equalTo: cardView1.centerYAnchor).isActive = true
		cardStack.leadingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.leadingAnchor, constant: -10).isActive = true
		cardStack.trailingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
		cardStack.heightAnchor.constraint(equalToConstant: 285).isActive = true
	}
	
	func numberOfCards(in cardStack: SwipeCardStack) -> Int {
		return 2
	}
	
	func cardStack(_ cardStack: SwipeCardStack, cardForIndexAt index: Int) -> SwipeCard {
		let card = SwipeCard()
			card.swipeDirections = [.right]
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		for direction in card.swipeDirections {
			card.setOverlay(CardOverlay(direction: direction), forDirection: direction)
		}
		return card
	}
	
    func cardStack(_ cardStack: SwipeCardStack, didSwipeCardAt index: Int, with direction: SwipeDirection) {
		switch direction {
		case .right:
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
				self.delegate?.didTapSwipeButton()
			}
        default:
            break
        }
    }
	
	@objc func swipe(notification: NSNotification) {
		self.cardStack.swipe(.right, animated: true)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didTapNext"), object: nil)
	}
	
    @IBAction func didTapSwipeButton(_ sender: Any) {
		self.cardStack.swipe(.right, animated: true)
	}
}

