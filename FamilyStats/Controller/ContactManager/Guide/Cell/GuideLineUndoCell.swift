import UIKit
import VisualEffectView

protocol guideLineUndoCellProtocol: class {
	func didTapUndoButton()
}

class GuideLineUndoCell: UICollectionViewCell, SwipeCardStackDelegate, SwipeCardStackDataSource {
	
	// MARK: - Outlet
	
	@IBOutlet weak var cardView3: UIView!
	@IBOutlet weak var cardView2: UIView!
	@IBOutlet weak var cardView1: UIView!
	var cardStack = SwipeCardStack()
	var items: GuideItem?
	
	@IBOutlet weak var undoButton: UIButton!
	weak var delegate: guideLineUndoCellProtocol?
	@IBOutlet weak var blurView: VisualEffectView?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		NotificationCenter.default.addObserver(self, selector: #selector(swipe(notification:)), name:NSNotification.Name(rawValue: "didTapNextUndo"), object: nil)
		
		// Initialization code
		DispatchQueue.main.async {
			self.cardView3.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			self.cardView2.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			self.cardView1.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			
			self.blurView?.colorTint = .white
			self.blurView?.colorTintAlpha = 0.2
			self.blurView?.blurRadius = 5
			self.blurView?.scale = 1
			
			// action
			self.undoButton.addTarget(self, action: #selector(self.didTapUndoButton), for: .touchUpInside)
			self.layoutCardStackView()
		}
	}
	
	func configure(with model: GuideItem) {
		DispatchQueue.main.async {
			self.items = model
			self.cardStack.delegate = self
			self.cardStack.dataSource = self
			self.cardStack.reloadData()
			self.cardStack.swipe(.left, animated: false)
		}
	}
	
	func layoutCardStackView() {
		self.addSubview(cardStack)
		cardStack.translatesAutoresizingMaskIntoConstraints = false
		cardStack.centerYAnchor.constraint(equalTo: cardView1.centerYAnchor).isActive = true
		cardStack.leadingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.leadingAnchor, constant: -10).isActive = true
		cardStack.trailingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
		cardStack.heightAnchor.constraint(equalToConstant: 285).isActive = true
	}
	
	func numberOfCards(in cardStack: SwipeCardStack) -> Int {
		return 3
	}
	
	func cardStack(_ cardStack: SwipeCardStack, cardForIndexAt index: Int) -> SwipeCard {
		let card = SwipeCard()
		card.swipeDirections = []
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		for direction in card.swipeDirections {
			card.setOverlay(CardOverlay(direction: direction), forDirection: direction)
		}
		return card
	}
	
	//MARK: - Did undo cards
	func cardStack(_ cardStack: SwipeCardStack, didUndoCardAt index: Int, from direction: SwipeDirection) {
		//TODO: Cacheable => DegaSingleton.shared.device.isGuideLine = false
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
			self.delegate?.didTapUndoButton()
		}
	}
	
	@objc func swipe(notification: NSNotification) {
		cardStack.undoLastSwipe(animated: true)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didTapNextUndo"), object: nil)
	}
	
	@objc
	func didTapUndoButton() {
		cardStack.undoLastSwipe(animated: true)
	}
}

