import UIKit
import VisualEffectView

protocol guideLineDeleteCellProtocol: class {
	func didTapDeleteButton()
}

class GuideLineDeleteCell: UICollectionViewCell, SwipeCardStackDelegate, SwipeCardStackDataSource {
	
	// MARK: - Outlet
	
	@IBOutlet weak var cardView3: UIView!
	@IBOutlet weak var cardView2: UIView!
	@IBOutlet weak var cardView1: UIView!
	var cardStack = SwipeCardStack()
	var items: GuideItem?
	@IBOutlet weak var swipeButton: UIButton!
	weak var delegate: guideLineDeleteCellProtocol?
	@IBOutlet weak var blurView: VisualEffectView?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		NotificationCenter.default.addObserver(self, selector: #selector(swipe(notification:)), name:NSNotification.Name(rawValue: "didTapNextDelete"), object: nil)
		
		DispatchQueue.main.async {
			self.cardView3.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			self.cardView2.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			self.cardView1.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: -1, height: 20), radius: 10)
			
			self.blurView?.colorTint = .white
			self.blurView?.colorTintAlpha = 0.2
			self.blurView?.blurRadius = 5
			self.blurView?.scale = 1
			
			// action
			self.swipeButton.addTarget(self, action: #selector(self.didTapSwipeButton), for: .touchUpInside)
		}
	}
	
	func configure(with model: GuideItem) {
		self.items = model
		self.layoutCardStackView()
		self.cardStack.delegate = self
		self.cardStack.dataSource = self
		self.cardStack.reloadData()
	}
	
	func layoutCardStackView() {
		self.addSubview(cardStack)
		cardStack.translatesAutoresizingMaskIntoConstraints = false
		cardStack.centerYAnchor.constraint(equalTo: cardView1.centerYAnchor).isActive = true
		cardStack.leadingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.leadingAnchor, constant: -10).isActive = true
		cardStack.trailingAnchor.constraint(equalTo: cardView1.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
		cardStack.heightAnchor.constraint(equalToConstant: 285).isActive = true
	}
	
	func numberOfCards(in cardStack: SwipeCardStack) -> Int {
		return 2
	}
	
	func cardStack(_ cardStack: SwipeCardStack, cardForIndexAt index: Int) -> SwipeCard {
		let card = SwipeCard()
		card.swipeDirections = [.left]
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		card.content = stackView(with: "\(items?.name ?? "") \(items?.surname ?? "")", phone: items?.telephone, initial: "\(items?.name?.initials ?? "")\(items?.surname?.initials ?? "")", mail: items?.email as NSString?, image: items?.image?.pngData())
		for direction in card.swipeDirections {
			card.setOverlay(CardOverlay(direction: direction), forDirection: direction)
		}
		return card
	}
	
	func cardStack(_ cardStack: SwipeCardStack, didSwipeCardAt index: Int, with direction: SwipeDirection) {
		switch direction {
		case .left:
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
				self.delegate?.didTapDeleteButton()
			}
		default:
			break
		}
	}
	
	@objc func swipe(notification: NSNotification) {
		cardStack.swipe(.left, animated: true)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didTapNextDelete"), object: nil)
	}
	
	@objc
	func didTapSwipeButton() {
		cardStack.swipe(.left, animated: true)
	}
}

