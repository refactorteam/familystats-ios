//
//  OnboardingViewController.swift

//
//  Created by Can Koç on 24.12.2018.
//  Copyright © 2018 Can KOÇ. All rights reserved.
//

import UIKit
import StoreKit
import CORELIB
import ViewAnimator

class OnboardingViewController: BaseViewController<OnboardingViewModel> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.onboarding.rawValue
    }
    
    override func setBinding() {
        super.setBinding()
        
        let options = OnboardingOptions(list: viewModel.onboardingList.value,
                                        viewController: self,
                                        mainScreen: (storyboard: StoryboardConstant.main, viewController: MainNavigationViewController.nameOfClass),
                                        collectionView: clcView, collectionViewCell: OnboardingViewCell.self,
                                        pager: pager, labelDescription: lblTermsDescription,
                                        buttonNext: btnNext, buttonPrivacy: btnPrivacy, buttonTerms: btnTerms,
                                        manualSwitchToApp: true, delegate: self)
        CORELIB.shared.startOnboardingViewController(with: options, disposeBag)
        
        clcView.rx.setDelegate(self).disposed(by: disposeBag)
    }
    
    override func setLocalization() {
        super.setLocalization()
    }
    
    override func setUI() {
        super.setUI()
        
        if #available(iOS 11.0, *) {
            clcView.contentInsetAdjustmentBehavior = .never
        }
        clcView.backgroundColor = .clear
        
        btnTerms.textSizeToFit()
        btnPrivacy.textSizeToFit()
    }
    
    // MARK: - View
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var clcView: UICollectionView!
    @IBOutlet weak fileprivate var btnNext: UIButton!
    @IBOutlet weak fileprivate var pager: UIPageControl!
    @IBOutlet weak fileprivate var lblTermsDescription: UILabel!
    @IBOutlet weak fileprivate var btnPrivacy: UIButton!
    @IBOutlet weak fileprivate var btnTerms: UIButton!
}

// MARK: - UICollectionView Delegate

extension OnboardingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: clcView.frame.width, height: clcView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cellItem = cell as? OnboardingViewCell else { return }
        let fromAnimation = AnimationType.vector(CGVector(dx: 45, dy: 0))
        UIView.animate(views: [cellItem.imageView], animations: [fromAnimation], duration: 1)
        
        let fromAnimation2 = AnimationType.vector(CGVector(dx: 1, dy: 0))
        UIView.animate(views: [cellItem.labelDescription, cellItem.labelTitle],
                       animations: [fromAnimation2], delay: 0.2, duration: 1)
    }
}

// MARK: Observe

extension OnboardingViewController {
    
    private func observePrivacyPage() {
        AppSingleton.shared.showPrivacy.subscribe(onNext: { [weak self] state in
            if state, #available(iOS 14, *) {
                self?.performSegue(withIdentifier: SegueConstant.privacy, sender: nil)
            } else {
                CORELIB.shared.swithToAppFromOnboardingViewController()
            }
        }).disposed(by: disposeBag)
    }
    
    private func registerRemoteNotification() {
        #if !targetEnvironment(simulator)
        let options = UNAuthorizationOptions(arrayLiteral: [.alert, .badge, .sound])
        
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (granted, error) in
        }
        UIApplication.shared.registerForRemoteNotifications()
        #endif
    }
}

// MARK: - PrivacyDelegate

extension OnboardingViewController: PrivacyPageDelegate {
    
    func privacyPageClosed() {
        CORELIB.shared.swithToAppFromOnboardingViewController()
    }
}

// MARK: - OnboardingDelegate

extension OnboardingViewController: OnboardingDelegate {
    
    func onboardingStarted() {
    }
    
    func onboardingTapped(nextIndex: Int) {
        if nextIndex == 1 {
            registerRemoteNotification()
        }
    }
    
    func onboardingCompleted() {
        self.observePrivacyPage()
    }
}

// MARK: - Segue

extension OnboardingViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let viewController = segue.destination as? PrivacyViewController else { return }
        viewController.delegate = self
    }
}
