//
//  RegisterViewController.swift
// FamilyStats
//
//  Created by Can Koç on 11.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import CORELIB
import AppTrackingTransparency
import AdSupport
import RxSwift

class RegisterViewController: BaseViewController<RegisterViewModel> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = EventScreenName.register.rawValue
    }
    
    override func setBinding() {
        super.setBinding()
        
        let registerOptions = RegisterOptions(onboardingScreen: (storyboard: StoryboardConstant.onboarding, viewController: OnboardingViewController.nameOfClass),
                                              mainScreen: (storyboard: StoryboardConstant.main, viewController: MainNavigationViewController.nameOfClass),
                                              labelRegisterError: lblError, automaticallyOpen: false, delegate: self)
        CORELIB.shared.startRegisterViewController(with: registerOptions, self.disposeBag)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblError: UILabel!
}

extension RegisterViewController: RegisterDelegate, Cacheable {
    
    func registerCompleted() {
        actionShowAppTrack()
        
        ApiRequest.hello().subscribe(onNext: { _ in
            CORELIB.shared.openApp()
        }).disposed(by: disposeBag)
    }
    
    private func actionShowAppTrack() {
        if CORELIB.shared.readAppInfoValue(for: "apptrack_on_register") == "true" {
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization(completionHandler: { _ in
                    debugPrint("ATTrackingManager shown.")
                })
            }
        }
    }
}
