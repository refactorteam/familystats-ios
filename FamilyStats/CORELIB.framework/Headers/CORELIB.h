//
//  CORELIB.h
//  CORELIB
//
//  Created by Can KOÇ on 9.09.2020.
//  Copyright © 2020 Can KOÇ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Adjust/Adjust.h>
#import <CommonCrypto/CommonHMAC.h>

//! Project version number for CORELIB.
FOUNDATION_EXPORT double CORELIBVersionNumber;

//! Project version string for CORELIB.
FOUNDATION_EXPORT const unsigned char CORELIBVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CORELIB/PublicHeader.h>


