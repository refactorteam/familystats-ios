//
//  PrivacyPageDelegate.swift
//  ChatCircle
//
//  Created by Can Koç on 22.05.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

protocol PrivacyPageDelegate {
    func privacyPageClosed()
}

