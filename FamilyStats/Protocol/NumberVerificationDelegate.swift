//
//  NumberVerificationDelegate.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

protocol NumberVerificationDelegate {
    func numberVerificationCompleted()
}
