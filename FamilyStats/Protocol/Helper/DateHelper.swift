//
//  DateHelper.swift
//  FamilyStats
//
//  Created by Can Koç on 23.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

class DateHelper {
    
    func getReadableFormat(timeStamp: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        
        if Calendar.current.isDateInTomorrow(date) {
            return LocKey.View.Main.tomorrow.localize
        } else if Calendar.current.isDateInYesterday(date) {
            dateFormatter.dateFormat = "HH:mm"
            return LocKey.View.Main.yesterday.localize + ", " + dateFormatter.string(from: date)
        } else if dateFallsInCurrentWeek(date: date) {
            if Calendar.current.isDateInToday(date) {
                dateFormatter.dateFormat = "HH:mm"
                return dateFormatter.string(from: date)
            } else {
                dateFormatter.dateFormat = "EEEE"
                return dateFormatter.string(from: date)
            }
        } else {
            dateFormatter.dateFormat = "dd MMM yyyy"
            return dateFormatter.string(from: date)
        }
    }
    
    func getReadableHour(timeStamp: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    func getReadableDate(timeStamp: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: date)
    }
    
    func getReadable(date: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: date)
    }
    
    func compareTwoDates(timeStamp1: TimeInterval, timeStamp2: TimeInterval) -> String {
        let date1 = getDate(timeStamp: timeStamp1)
        let date2 = getDate(timeStamp: timeStamp2)
        
        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: date1)
        let nowComponents = calendar.dateComponents([.hour, .minute, .second], from: date2)
        
        let difference = calendar.dateComponents([.hour, .minute, .second], from: timeComponents, to: nowComponents)
        
        var result = "-"
        if let hour = difference.hour, hour > 0 {
            result = String(hour) + " " + LocKey.View.Report.hour.localize
        } else if let minute = difference.minute, minute > 0 {
            result = String(minute) + " " + LocKey.View.Report.minutes.localize
        } else if let second = difference.second, second > 0 {
            result = String(second) + " " + LocKey.View.Report.seconds.localize
        }
        
        return result
    }
    
    func getDate(timeStamp: TimeInterval) -> Date {
        return Date(timeIntervalSince1970: timeStamp)
    }
    
    private func dateFallsInCurrentWeek(date: Date) -> Bool {
        let currentWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: Date())
        let datesWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: date)
        return (currentWeek == datesWeek)
    }
}
