//
//  EmailValidator.swift
// FamilyStats
//
//  Created by Can Koç on 7.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

protocol Validatable {
    func isValid(email: String) -> Bool
    func isValid(text: String?) -> Bool
}

extension Validatable {
    
    func isValid(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValid(text: String?) -> Bool {
        if text?.trim == nil {
            return false
        } else if text!.trim.isEmpty == true {
            return false
        } else {
            return true
        }
    }
}
