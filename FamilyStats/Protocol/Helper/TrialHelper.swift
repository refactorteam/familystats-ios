//
//  TrialHelper.swift
//  FamilyStats
//
//  Created by Can Koç on 21.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import KeychainSwift

class TrialHelper {

    private static var trialUsed: Bool?
    static func getDidUserUseTrial() -> Bool {
        let keyChainSwift = KeychainSwift()
        guard let onMemoryTrialUsed = trialUsed else {
            if let keyChainResult = keyChainSwift.getBool(StorageConstant.trialUsed) {
                trialUsed = keyChainResult
                return keyChainResult
            } else {
                return false
            }
        }
        return onMemoryTrialUsed
    }
    
    static func setUserTrialUsed() {
        let keyChainSwift = KeychainSwift()
        trialUsed = true
        _ = keyChainSwift.set(true, forKey: StorageConstant.trialUsed)
    }
    
    static func removeTrialUsed() {
        let keyChainSwift = KeychainSwift()
        keyChainSwift.delete(StorageConstant.trialUsed)
    }
}
