//
//  GroupCreateProtocol.swift
//  FamilyStats
//
//  Created by Can Koç on 11.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

protocol GroupCreateProtocol {
    func groupCreated()
}
