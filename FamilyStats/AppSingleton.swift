//
//  AppSingleton.swift
// FamilyStats
//
//  Created by Can Koç on 2.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxCocoa
import CORELIB

class AppSingleton {
    
    static let shared = AppSingleton()
    
    var showPrivacy = BehaviorRelay<Bool>(value: false)
    
    // MARK: - Router
    var routePage = BehaviorRelay<RoutePageEnum?>(value: nil)
    var userTrackingList = BehaviorRelay<HelloResponseModel?>(value: nil)
    
    // MARK: - ContactManager
    
    var resetStatus: Bool = false
    var isNativeAdForCards: Bool = true
    var modNativeAds = 5
    
    func updateWithAppConfig() {
        guard let nativeAdModString = CORELIB.shared.readAppInfoValue(for: "native_ad_mod"),
              let nativeAdMod = Int(nativeAdModString) else {
            return
        }
        modNativeAds = nativeAdMod
        if nativeAdMod <= 0 {
            isNativeAdForCards = false
        }
    }
}
