import UIKit

class ProfileEditCell: UITableViewCell {

	// MARK: - Outlets
	@IBOutlet weak var viewBg: UIView!
	@IBOutlet weak var txtData: UITextField!
	@IBOutlet weak var btnEdit: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
		viewBg.layer.borderWidth = 1.0
		viewBg.layer.borderColor = Theme.Color.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
	
	// MARK: - Action
	@IBAction func editButton(_ sender: Any) {
		if (sender as AnyObject).tag == 999 {
			if let textPhone = self.viewWithTag(999) as? UITextField {
				textPhone.becomeFirstResponder()
			}
		} else if (sender as AnyObject).tag == 888 {
			if let textName = self.viewWithTag(888) as? UITextField {
				textName.becomeFirstResponder()
			}
		}else if (sender as AnyObject).tag == 666 {
			if let textSurname = self.viewWithTag(666) as? UITextField {
				textSurname.becomeFirstResponder()
			}
		}  else {
			if let textEmail = self.viewWithTag(777) as? UITextField {
				textEmail.becomeFirstResponder()
			}
		}
	}
 
	func configure(with data: String?, tag: Int) {
		if data != nil && !(data?.isEmpty ?? true) {
			self.txtData.text = data
		} else {
			if tag == 999 {
				self.txtData.placeholder = LocKey.View.ProfileEdit.placeHolderNoPhone.localize
			} else if tag == 888 {
				self.txtData.placeholder = LocKey.View.ProfileEdit.placeHolderNoName.localize
			}  else if tag == 666 {
				self.txtData.placeholder = LocKey.View.ProfileEdit.placeHolderNoSurname.localize
			} else {
				self.txtData.placeholder = LocKey.View.ProfileEdit.placeHolderNoEmail.localize
			}
		}
	}
}
