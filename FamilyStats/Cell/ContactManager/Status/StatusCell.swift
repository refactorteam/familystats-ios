import UIKit

class StatusCell: UITableViewCell {
	
	@IBOutlet weak var viewBg: UIView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		viewBg.layer.borderWidth = 1.0
		viewBg.layer.borderColor = Theme.Color.lightGray.cgColor
		lblTitle.textColor = Theme.Color.navyBlue
		lblTitle.font = Theme.Font.poppinsRegular.of(size: 12)
		lblCount.textColor = Theme.Color.darkSkyBlue
		lblCount.font = Theme.Font.poppinsBold.of(size: 30)
		lblDescription.textColor = Theme.Color.navyBlue
		lblDescription.font = Theme.Font.poppinsRegular.of(size: 14)
    }

	func configureItem(with listItem: StatusModel) {
		lblTitle.text = listItem.title
		DispatchQueue.main.async {
			self.lblCount.countFrom(0, to: CGFloat(listItem.totalCount), withDuration: 1.0)
		}
		lblDescription.text = listItem.description
		imgIcon.image = listItem.image
    }
	
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
	
	// MARK: - Outlet
	
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: EFCountingLabel!
    @IBOutlet weak var lblDescription: UILabel!
	@IBOutlet weak var imgIcon: UIImageView!
}
