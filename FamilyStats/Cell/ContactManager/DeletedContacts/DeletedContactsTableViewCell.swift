import UIKit

class DeletedContactsTableViewCell: UITableViewCell {

	// MARK: - Outlet
	
	@IBOutlet weak var viewBg: UIView!
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var btnRestore: UIButton!
	@IBOutlet weak var btnRemove: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
		viewBg.layer.borderWidth = 1.0
		viewBg.layer.borderColor = Theme.Color.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
	func configure(with model: CustomTestModel) {
		if model.firstName != "" && model.firstName != nil {
			lblTitle.text = "\(String(model.firstName ?? LocKey.View.ProfileEdit.placeHolderNoName.localize)) \(String(model.lastName ?? ""))"
		} else if model.telephone != "" && model.telephone != nil {
			lblTitle.text = model.telephone
		} else {
			lblTitle.text = LocKey.View.ProfileEdit.placeHolderNoName.localize
		}
	}
}
