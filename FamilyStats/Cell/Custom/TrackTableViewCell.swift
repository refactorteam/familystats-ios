//
//  TrackTableViewCell.swift
//  FamilyStats
//
//  Created by Can Koç on 19.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxSwift
import CORELIB

class TrackTableViewCell: UITableViewCell {

    var disposeBag = DisposeBag()
    
    private var timer : Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .clear
        
        imgProfile.layer.cornerRadius = 20
        imgProfile.layer.masksToBounds = true
        imgProfile.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
        timer?.invalidate()
        timer = nil
    }
    
    var cellItem: UserTrackingModel? {
        didSet {
            lblName.text = cellItem?.name
            lblPhoneNumber.text = cellItem?.phoneNumber
            
            if cellItem?.isOnline == true {
                lblOnlineStatus.text = LocKey.View.Main.userOnline.localize
                viewOnlineStatus.backgroundColor = UIColor(red: 0/255, green: 191/255, blue: 118/255, alpha: 255)
                animateOnlineView()
            } else {
                lblOnlineStatus.text = LocKey.View.Main.userOffline.localize
                viewOnlineStatus.backgroundColor = UIColor(red: 239/255, green: 68/255, blue: 69/255, alpha: 255)
                if (cellItem?.lastSeen ?? 0) > 0 {
                    lblLastSeen.text = DateHelper().getReadableFormat(timeStamp: TimeInterval(cellItem?.lastSeen ?? 0))
                } else {
                    lblLastSeen.text = String()
                }
                stopAnimateOnlineView()
            }
            
            if CORELIB.shared.readAppInfoValue(for: "mute_button_status") == "true" {
                btnMute.isHidden = false
                btnMute.isUserInteractionEnabled = true
                if cellItem?.notf == true {
                    btnMute.setImage(UIImage(named: "iconUnmuted"), for: .normal)
                } else {
                    btnMute.setImage(UIImage(named: "iconMuted"), for: .normal)
                }
            } else {
                btnMute.isHidden = true
                btnMute.isUserInteractionEnabled = false
            }
            
            if CORELIB.shared.readAppInfoValue(for: "image_checker") == "true" {
                guard let stringURL = cellItem?.image, let imageURL = URL(string: stringURL) else { return }
                imgProfile.kf.setImage(with: imageURL)
            }
        }
    }
    
    private func secondsToMinutesSeconds (seconds : Int) -> (Int, Int) {
      return ((seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    private func animateOnlineView() {
        viewAnimation.isHidden = false
        self.viewAnimation.alpha = 1
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.viewAnimation.alpha = 0
        }, completion: nil)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target:self, selector:#selector(prozessTimer), userInfo: nil, repeats: true)
    }
    
    private func stopAnimateOnlineView() {
        viewAnimation.isHidden = true
        timer?.invalidate()
        timer = nil
    }
    
    @objc func prozessTimer() {
        let lastSeenDate = Date(timeIntervalSince1970: TimeInterval(cellItem?.lastSeen ?? 0))
        let diffComponents = Calendar.current.dateComponents([.second], from: lastSeenDate, to: Date())
        guard let seconds = diffComponents.second else { return }
            
        let durationTime = secondsToMinutesSeconds(seconds: seconds)
        let firstString = String(durationTime.0).count > 1 ? String(durationTime.0) : "0" + String(durationTime.0)
        let lastString = String(durationTime.1).count > 1 ? String(durationTime.1) : "0" + String(durationTime.1)
        lblLastSeen.text = firstString + ":" + lastString
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblName: UILabel!
    @IBOutlet weak fileprivate var lblPhoneNumber: UILabel!
    @IBOutlet weak fileprivate var lblOnlineStatus: UILabel!
    @IBOutlet weak fileprivate var viewOnlineStatus: BorderedCornerView!
    @IBOutlet weak fileprivate var lblLastSeen: UILabel!
    @IBOutlet weak fileprivate var imgProfile: UIImageView!
    @IBOutlet weak fileprivate var viewAnimation: BorderedCornerView!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnMore: UIButton!
}
