//
//  CreateGroupTableViewCell.swift
//  FamilyStats
//
//  Created by Can Koç on 6.02.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit

class CreateGroupTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        selectionStyle = .none
    }
    
    var cellItem: UserTrackingModel? {
        didSet {
            lblName.text = cellItem?.name
            lblPhone.text = cellItem?.phoneNumber
        }
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
}
