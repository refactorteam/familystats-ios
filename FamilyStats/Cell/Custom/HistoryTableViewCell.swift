//
//  HistoryTableViewCell.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
    }
    
    var cellItem: UserHistoryModel? {
        didSet {
            lblIn.text = cellItem?.inTime
            lblInDate.text = cellItem?.inTimeDate
            lblOut.text = cellItem?.outTime
            lblOutDate.text = cellItem?.outTimeDate
            lblDuration.text = cellItem?.duration
        }
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak fileprivate var lblIn: UILabel!
    @IBOutlet weak fileprivate var lblInDate: UILabel!
    @IBOutlet weak fileprivate var lblOut: UILabel!
    @IBOutlet weak fileprivate var lblOutDate: UILabel!
    @IBOutlet weak fileprivate var lblDuration: UILabel!
}
