//
//  DefaultTableViewCell.swift
//  CORELIB
//
//  Created by Can Koç on 31.10.2019.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import CORELIB

class DefaultTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .clear
    }
    
    var menuItem: OtherMenuModel? {
        didSet {
            lblTitle.text = menuItem?.getTitle()
        }
    }
    
    // MARK: - Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
    
}
