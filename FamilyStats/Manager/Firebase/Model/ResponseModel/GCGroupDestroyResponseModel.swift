//
//  GCGroupDestroyResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 13.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCGroupDestroyResponseModel: Codable {
    var error: Bool?
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case error = "err"
        case message
    }
}
