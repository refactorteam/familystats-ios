//
//  GCDeleteNumberResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 23.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCDeleteNumberResponseModel: Codable {
    var error: Bool?
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case error = "err"
        case message
    }
}
