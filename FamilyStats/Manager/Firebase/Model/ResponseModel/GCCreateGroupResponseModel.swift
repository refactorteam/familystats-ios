//
//  GCCreateGroupResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCCreateGroupResponseModel: Codable {
    var error: Bool?
    var message: String?
    var groupKey: String?
    
    enum CodingKeys: String, CodingKey {
        case error = "err"
        case message
        case groupKey = "group_key"
    }
}
