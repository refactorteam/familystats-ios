//
//  GCGroupStatusModel.swift
//  FamilyStats
//
//  Created by Can Koç on 11.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCGroupHistoryModel: Codable {
    var data: [String: HistoryModel]
    var status: GCGroupStatusModel
    
    struct HistoryModel: Codable {
        var inTime: Int
        var outTime: Int
        
        enum CodingKeys: String, CodingKey {
            case inTime = "i"
            case outTime = "o"
        }
    }
}

struct GCGroupStatusModel: Codable {
    var isOnline: Bool
    var lastOnline: Int
    var lastDuration: Int?
    
    enum CodingKeys: String, CodingKey {
        case isOnline = "is_online"
        case lastOnline = "last_online"
        case lastDuration = "last_duration"
    }
}


