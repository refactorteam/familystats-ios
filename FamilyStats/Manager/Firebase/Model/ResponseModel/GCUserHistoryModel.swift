//
//  GCHistoryModel.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCHistoryModel: Codable {
    var inTime: Int
    var outTime: Int
    
    enum CodingKeys: String, CodingKey {
        case inTime = "i"
        case outTime = "o"
    }
}
