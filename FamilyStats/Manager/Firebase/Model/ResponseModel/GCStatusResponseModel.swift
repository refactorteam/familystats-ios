//
//  GCStatusResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 23.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCStatusResponseModel: Codable {
    var isOnline: Bool
    var lastOnline: Int
    var lastDuration: Int?
    
    enum CodingKeys: String, CodingKey {
        case isOnline = "is_online"
        case lastOnline = "last_online"
        case lastDuration = "last_duration"
    }
}
