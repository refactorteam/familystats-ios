//
//  GCUserResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 21.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCUserResponseModel: Codable {
    var isOnline: Bool
    var lastDuration: Int?
    var lastOnline: Double
    
    enum CodingKeys: String, CodingKey {
        case isOnline = "is_online"
        case lastDuration = "last_duration"
        case lastOnline = "last_online"
    }
}
