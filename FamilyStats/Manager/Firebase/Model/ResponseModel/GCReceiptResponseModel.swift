//
//  CGReceiptResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 22.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCReceiptResponseModel: Codable {
    var error: Bool?
    
    enum CodingKeys: String, CodingKey {
        case error = "err"
    }
}
