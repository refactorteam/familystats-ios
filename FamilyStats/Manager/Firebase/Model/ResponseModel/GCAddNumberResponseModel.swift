//
//  GCAddNumberResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 21.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCAddNumberResponseModel: Codable {
    var error: Bool?
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case error = "err"
        case message
    }
}
