//
//  GCStartTrialRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 21.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCStartTrialRequestModel: Codable {
    var hour: Int
}
