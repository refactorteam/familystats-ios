//
//  GCDeleteNumberRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 23.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCDeleteNumberRequestModel: Codable {
    var number: String
}
