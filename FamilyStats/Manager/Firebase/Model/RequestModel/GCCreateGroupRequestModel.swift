//
//  GCDestroyRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCCreateGroupRequestModel: Codable {
    var no1: String
    var no2: String
    var name: String
    var notf: Bool
}
