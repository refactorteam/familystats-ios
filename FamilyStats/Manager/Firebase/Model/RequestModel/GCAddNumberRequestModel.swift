//
//  GCAddNumberRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 20.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GCAddNumberRequestModel: Codable {
    var number: String
    var name: String
    var notf: Bool
}
