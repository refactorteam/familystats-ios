//
//  FirebaseManager.swift
//  FamilyStats
//
//  Created by Can Koç on 20.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import RxSwift
import CORELIB
import FirebaseFunctions

class FirebaseManager {
    
    static let shared = FirebaseManager()
}

// MARK: - Auth

extension FirebaseManager {
    
    func authUser() -> Observable<String> {
        return Observable.create({ observer in
            
            guard let currentUserID = Auth.auth().currentUser?.uid else {
                Auth.auth().signInAnonymously(completion: { (authResult, error) in
                    if error == nil {
                        guard let user = authResult?.user else {
                            observer.on(.error(AuthError.signInError))
                            return
                        }
                        observer.on(.next(user.uid))
                    } else {
                        observer.on(.error(AuthError.signInError))
                    }
                })
                return Disposables.create()
            }
            observer.on(.next(currentUserID))
            
            return Disposables.create()
        })
    }
}

// MARK: - Send Data

extension FirebaseManager {
    
    func sendData<RequestModel: Codable, ResponseModel: Codable>(_ serviceName: CloudFunctionsPath, requestModel: RequestModel) -> Observable<ResponseModel> {
        return Observable.create({ observer in
            
            do {
                let jsonData = try JSONEncoder().encode(requestModel)
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)

                Functions.functions(region: "europe-west1").httpsCallable(serviceName.rawValue).call(json) { (result, error) in
                    if let anyData = result?.data, error == nil {
                        do {
                            let data = try JSONSerialization.data(withJSONObject: anyData, options: .fragmentsAllowed)
                            let response = try JSONDecoder().decode(ResponseModel.self, from: data)
                            observer.on(.next(response))
                        } catch {
                            #if !Release
                            print("[Firebase Do Error] \(error) \nFoundedJson: \(anyData) [/Firebase Do Error]")
                            #endif
                            observer.on(.error(FirebaseError.jsonParseError))
                        }
                    } else {
                        #if !Release
                        print("[Firebase Error] \(String(describing: error?.localizedDescription)) [/FirebaseError]")
                        #endif
                        observer.on(.error(FirebaseError.generalError))
                    }
                }
            } catch {
                #if !Release
                print("[Firebase Do Error] \(error.localizedDescription) [/Firebase Do Error]")
                #endif
                observer.on(.error(FirebaseError.jsonParseError))
            }
            
            return Disposables.create()
        })
    }
}

// MARK: - Get Data

extension FirebaseManager {
    
    func getData<ResponseModel: Codable>(path: String) -> Observable<ResponseModel?> {
        return Observable.create({ observer in
            
            let userRef = Database.database().reference(withPath: path)
            userRef.observe(DataEventType.value) { snapshot in
                if let anyData = snapshot.value {
                    do {
                        let data = try JSONSerialization.data(withJSONObject: anyData, options: .fragmentsAllowed)
                        let response = try JSONDecoder().decode(ResponseModel.self, from: data)
                        
                        observer.on(.next(response))
                    } catch {
                        #if !Release
                        print("[Firebase Do Error] \(error.localizedDescription) [/Firebase Do Error]")
                        #endif
                        observer.on(.error(FirebaseError.jsonParseError))
                    }
                } else {
                    observer.on(.next(nil))
                }
            }
            return Disposables.create()
        })
    }
    
    func getDataForOnce<ResponseModel: Codable>(path: String) -> Observable<ResponseModel?> {
        return Observable.create({ observer in
            
            let userRef = Database.database().reference(withPath: path)
            userRef.observeSingleEvent(of: DataEventType.value) { snapshot in
                if let anyData = snapshot.value {
                    do {
                        let data = try JSONSerialization.data(withJSONObject: anyData, options: .fragmentsAllowed)
                        let response = try JSONDecoder().decode(ResponseModel.self, from: data)
                        
                        observer.on(.next(response))
                    } catch {
                        #if !Release
                        print("[Firebase Do Error] \(error.localizedDescription) [/Firebase Do Error]")
                        #endif
                        observer.on(.error(FirebaseError.jsonParseError))
                    }
                } else {
                    observer.on(.next(nil))
                }
            }
            return Disposables.create()
        })
    }
    
    func getDataForUserHistory(phoneNumber: String, lastKey: Int) -> Observable<[String: GCHistoryModel]?> {
       return Observable.create({ observer in
          
            var userRef = Database.database().reference(withPath: FirebaseGetDataPath.pathNumberHistory(phoneNumber: phoneNumber)).queryOrdered(byChild: "i")
        
            if lastKey > 0 {
                userRef = userRef.queryStarting(atValue: lastKey, childKey: "i")
            }

            userRef.observeSingleEvent(of: DataEventType.value) { snapshot in
               if let anyData = snapshot.value {
                   do {
                       let data = try JSONSerialization.data(withJSONObject: anyData, options: .fragmentsAllowed)
                       let response = try JSONDecoder().decode([String: GCHistoryModel]?.self, from: data)
                       
                       observer.on(.next(response))
                   } catch {
                       #if !Release
                       print("[Firebase Do Error] \(error.localizedDescription) [/Firebase Do Error]")
                       #endif
                       observer.on(.error(FirebaseError.jsonParseError))
                   }
               } else {
                   observer.on(.next(nil))
               }
           }
           return Disposables.create()
       })
    }
    
    func getDataForGroupHistory(groupKey: String, lastKey: Int) -> Observable<[String: GCHistoryModel]?> {
       return Observable.create({ observer in
          
            var userRef = Database.database().reference(withPath: FirebaseGetDataPath.pathGroupHistory(groupKey: groupKey)).queryOrdered(byChild: "i")
        
            if lastKey > 0 {
                userRef = userRef.queryStarting(atValue: lastKey, childKey: "i")
            }

            userRef.observeSingleEvent(of: DataEventType.value) { snapshot in
               if let anyData = snapshot.value {
                   do {
                       let data = try JSONSerialization.data(withJSONObject: anyData, options: .fragmentsAllowed)
                       let response = try JSONDecoder().decode([String: GCHistoryModel]?.self, from: data)
                       
                       observer.on(.next(response))
                   } catch {
                       #if !Release
                       print("[Firebase Do Error] \(error.localizedDescription) [/Firebase Do Error]")
                       #endif
                       observer.on(.error(FirebaseError.jsonParseError))
                   }
               } else {
                   observer.on(.next(nil))
               }
           }
           return Disposables.create()
       })
    }
}

