//
//  FirebaseStorageManager.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import Firebase
import RxSwift
 
class FirebaseStorageManager {
    
    let storage: Storage!
    
    init() {
        
        self.storage = Storage.storage()
    }
    
    func getImage(for phoneNumber: String, on imageView: UIImageView) -> Observable<Bool> {
        return Observable.create({ observer in
            
            let pathReference = self.storage.reference(withPath: "images/\(phoneNumber).jpg")
            
            pathReference.downloadURL { url, error in
                if error != nil {
                observer.on(.next(false))
              } else {
                imageView.kf.setImage(with: url, completionHandler:  { result in
                    switch result {
                    case .success(_):
                        observer.on(.next(true))
                    default:
                        observer.on(.next(false))
                    }
                })
              }
            }
            
            return Disposables.create()
        })
    }
}
