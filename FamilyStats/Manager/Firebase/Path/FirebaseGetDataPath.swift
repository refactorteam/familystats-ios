//
//  FirebaseGetDataPath.swift
//  FamilyStats
//
//  Created by Can Koç on 22.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

class FirebaseGetDataPath {
    
    static func pathNumberStatus(phoneNumber: String) -> String {
        return "reporters/\(phoneNumber)/public/status"
    }
    
    static func pathNumberHistory(phoneNumber: String) -> String {
        return "ReporterData/\(phoneNumber)"
    }
    
    static func pathGroupStatus(groupKey: String) -> String {
        return "groups/\(groupKey)"
    }
    
    static func pathGroupHistory(groupKey: String) -> String {
        return "GroupsData/\(groupKey)"
    }
}
