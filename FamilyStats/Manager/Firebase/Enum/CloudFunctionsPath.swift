//
//  CloudFunctionsPath.swift
//  FamilyStats
//
//  Created by Can Koç on 21.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum CloudFunctionsPath: String {
    case addNumber
    case startTrial = "startTrialIOS"
    case addPremium = "addPremiumIOS"
    case createGroup = "createGroup"
    case deleteNumber
    case destroyGroup
}
