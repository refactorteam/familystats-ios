//
//  CacheManager.swift
// FamilyStats
//
//  Created by Can KOÇ on 13.02.2022.
//  Copyright © 2022 Can Koç. All rights reserved.
//

import Foundation
import CORELIB

class CacheManager: Cacheable {
	
	static let shared = CacheManager()
	
	
	// MARK: - Contacts
	func saveContacts(with model: [CustomTestModel], type: String) {
        self.saveToCache(with: model, for: type)
	}
	
	func setContacts(with model: CustomTestModel, type: String) {
		DispatchQueue.main.async {
			var contacts = [CustomTestModel]()
            if var list: [CustomTestModel] = self.getJSONFromCache(for: type) {
                list.append(CustomTestModel(firstName: model.firstName, lastName: model.lastName, telephone: model.telephone ?? "", contactId: model.contactId, mail: model.mail ?? "" as String, status: type, image: model.image))
                self.saveContacts(with: list, type: type)
                self.changeStatus(with: model)
            } else {
                contacts.append(CustomTestModel(firstName: model.firstName, lastName: model.lastName, telephone: model.telephone ?? "", contactId: model.contactId, mail: model.mail ?? "" as String, status: type, image: model.image))
                self.saveContacts(with: contacts, type: type)
                self.changeStatus(with: model)
            }
		}
	}
	
	func changeStatus(with model: CustomTestModel) {
        if var list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.unknown),
           list.count > 0, let firstItemIndex = list.firstIndex(where: { $0.contactId == model.contactId }) {
            list.remove(at: firstItemIndex)
            self.saveContacts(with: list, type: StorageConstant.unknown)
        }
	}
	
	func updatCacheAfterMerge(with removedId: String, type: String) {
        if var list: [CustomTestModel] = getJSONFromCache(for: type),
           let firstItemIndex = list.firstIndex(where: { $0.contactId == removedId }) {
            list.remove(at: firstItemIndex)
            self.saveContacts(with: list, type: type)
        }
	}
	
	func getContacts(with key: String, data: Data, nativeAd: Bool) -> [CustomTestModel] {
		var contacts = [CustomTestModel]()
		var contactForAds = [CustomTestModel]()

		guard let list = try? JSONDecoder().decode([CustomTestModel].self, from: data) else { return contacts }
		if list.count > 0 {
			for index in 0...list.count - 1 {
				if list[index].status == key {
					contacts.append(CustomTestModel(firstName: list[index].firstName, lastName: list[index].lastName, telephone: list[index].telephone ?? "", contactId: list[index].contactId, mail: list[index].mail ?? "" as String, status: list[index].status, image: list[index].image))
				}
			}
		}
		
		let sortedContact = contacts.sorted { (initial, next) -> Bool in
						  guard initial.firstName != nil else { return true }
						  guard next.firstName != nil else { return true }
						  return initial.firstName!.compare(next.firstName!) == .orderedAscending
		}
		
		
		if nativeAd && sortedContact.count > 0 {
			for index in 0...sortedContact.count - 1 {
				 if (index % AppSingleton.shared.modNativeAds == 0) && index != 0 {
					 contactForAds.append(CustomTestModel(firstName: "", lastName: "", telephone: "", contactId: "", mail: "", status: StorageConstant.ads, image: nil))
					 
					 contactForAds.append(CustomTestModel(firstName: sortedContact[index].firstName, lastName: sortedContact[index].lastName,  telephone: sortedContact[index].telephone, contactId: sortedContact[index].contactId, mail: sortedContact[index].mail, status: StorageConstant.unknown, image: sortedContact[index].image))
				 } else {
					 contactForAds.append(CustomTestModel(firstName: sortedContact[index].firstName, lastName: sortedContact[index].lastName, telephone: sortedContact[index].telephone, contactId: sortedContact[index].contactId, mail: sortedContact[index].mail, status: StorageConstant.unknown, image: sortedContact[index].image))
				 }
			}
			return contactForAds
		} else {
			return sortedContact
		}
	}
	
	func getContactsForWOContact(with key: String, data: Data, nativeAd: Bool) -> [CustomTestModel] {
		var contacts = [CustomTestModel]()
		var contactForAds = [CustomTestModel]()

		guard let list = try? JSONDecoder().decode([CustomTestModel].self, from: data) else { return contacts}
		if list.count > 0 {
			for index in 0...list.count - 1 {
				if list[index].status == key && (list[index].telephone == "" || list[index].telephone == nil) {
					contacts.append(CustomTestModel(firstName: list[index].firstName, lastName: list[index].lastName, telephone: list[index].telephone ?? "", contactId: list[index].contactId, mail: list[index].mail ?? "" as String, status: list[index].status, image: list[index].image))
				}
			}
		}
		
		if nativeAd && contacts.count > 0 {
			for index in 0...contacts.count - 1 {
				if (index % AppSingleton.shared.modNativeAds == 0) && contactForAds.count > 0 {
					 contactForAds.append(CustomTestModel(firstName: "", lastName: "", telephone: "", contactId: "", mail: "", status: StorageConstant.ads, image: nil))
					 
					 contactForAds.append(CustomTestModel(firstName: contacts[index].firstName, lastName: contacts[index].lastName,  telephone: contacts[index].telephone, contactId: contacts[index].contactId, mail: contacts[index].mail, status: StorageConstant.unknown, image: contacts[index].image))
				 } else {
					 contactForAds.append(CustomTestModel(firstName: contacts[index].firstName, lastName: contacts[index].lastName, telephone: contacts[index].telephone, contactId: contacts[index].contactId, mail: contacts[index].mail, status: StorageConstant.unknown, image: contacts[index].image))
				 }
			}
			return contactForAds
		} else {
			return contacts
		}
	}
	
	func getContactTypeCount(with key: String) -> Int {
        guard let list: [CustomTestModel] = getJSONFromCache(for: key) else { return 0 }
        return list.filter({ $0.status == key }).count
	}

	func getContactTypeCountForWOContact(with key: String) -> Int {
        guard let list: [CustomTestModel] = getJSONFromCache(for: key) else { return 0 }
        return list.filter({ $0.status == key && ($0.telephone == "" || $0.telephone == nil) }).count
	}
	
	func resetContactTypes(with nativeAd: Bool) {
		var contacts = [CustomTestModel]()
		var contactsWithAds = [CustomTestModel]()
		
		DispatchQueue.main.async {
            if let list: [CustomTestModel] = self.getJSONFromCache(for: StorageConstant.unknown) {
                contacts.append(contentsOf: list.filter({ $0.status == StorageConstant.unknown }))
            }
            if let list: [CustomTestModel] = self.getJSONFromCache(for: StorageConstant.deleted) {
                contacts.append(contentsOf: list.filter({ $0.status == StorageConstant.deleted }))
            }
            if let list: [CustomTestModel] = self.getJSONFromCache(for: StorageConstant.favorite) {
                contacts.append(contentsOf: list.filter({ $0.status == StorageConstant.favorite }))
            }
            if let list: [CustomTestModel] = self.getJSONFromCache(for: StorageConstant.swipe) {
                contacts.append(contentsOf: list.filter({ $0.status == StorageConstant.swipe }))
            }
            
			if nativeAd {
				if contacts.count > 0 {
				for index in 0...contacts.count - 1 {
					if contacts.count > 0 {
					if (index % AppSingleton.shared.modNativeAds == 0) && index != 0 {
						contactsWithAds.append(CustomTestModel(firstName: "", lastName: "", telephone: "", contactId: "", mail: "", status: StorageConstant.ads, image: nil))
						
						contactsWithAds.append(CustomTestModel(firstName: contacts[index].firstName, lastName: contacts[index].lastName,  telephone: contacts[index].telephone, contactId: contacts[index].contactId, mail: contacts[index].mail, status: StorageConstant.unknown, image: contacts[index].image))
					} else {
						contactsWithAds.append(CustomTestModel(firstName: contacts[index].firstName, lastName: contacts[index].lastName, telephone: contacts[index].telephone, contactId: contacts[index].contactId, mail: contacts[index].mail, status: StorageConstant.unknown, image: contacts[index].image))
					}
				  }
				}
				self.saveContacts(with: contactsWithAds, type: StorageConstant.unknown)
			  }
			} else {
				self.saveContacts(with: contacts, type: StorageConstant.unknown)
			}
			
            self.removeCache(for: StorageConstant.favorite)
            self.removeCache(for: StorageConstant.deleted)
            self.removeCache(for: StorageConstant.swipe)
		}
	}
	
	func startOver(with nativeAd: Bool) -> [CustomTestModel]? {
		var contactStartOver = [CustomTestModel]()
		var contactsWithAds = [CustomTestModel]()
        
        if let list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.unknown) {
            contactStartOver.append(contentsOf: list.filter({ $0.status == StorageConstant.unknown }))
        }
        if let list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.swipe) {
            contactStartOver.append(contentsOf: list.filter({ $0.status == StorageConstant.swipe }))
        }
        
        self.removeCache(for: StorageConstant.swipe)
		
		if contactStartOver.count > 0 {
			if nativeAd {
				for index in 0...contactStartOver.count - 1 {
					if (index % AppSingleton.shared.modNativeAds == 0) && index != 0 {
						contactsWithAds.append(CustomTestModel(firstName: "", lastName: "", telephone: "", contactId: "", mail: "", status: StorageConstant.ads, image: nil))
						
						contactsWithAds.append(CustomTestModel(firstName: contactStartOver[index].firstName, lastName: contactStartOver[index].lastName, telephone: contactStartOver[index].telephone, contactId: contactStartOver[index].contactId, mail: contactStartOver[index].mail, status: StorageConstant.unknown, image: contactStartOver[index].image))
					} else {
						contactsWithAds.append(CustomTestModel(firstName: contactStartOver[index].firstName, lastName: contactStartOver[index].lastName, telephone: contactStartOver[index].telephone, contactId: contactStartOver[index].contactId, mail: contactStartOver[index].mail, status: StorageConstant.unknown, image: contactStartOver[index].image))
					}
				}
				
				self.saveContacts(with: contactsWithAds, type: StorageConstant.unknown)
				return contactsWithAds
			} else {
				self.saveContacts(with: contactStartOver, type: StorageConstant.unknown)
				return contactStartOver
			}
		} else {
			return [CustomTestModel]()
		}
	}
	
	func setContactsForUndo(with model: CustomTestModel, type: String) {
		var contacts = [CustomTestModel]()
        if var list: [CustomTestModel] = getJSONFromCache(for: type) {
            list.append(CustomTestModel(firstName: model.firstName, lastName: model.lastName, telephone: model.telephone ?? "", contactId: model.contactId, mail: model.mail ?? "" as String, status: type, image: model.image))
            self.saveContacts(with: list, type: type)
            self.changeStatusForUndo(with: model)
        } else {
            contacts.append(CustomTestModel(firstName: model.firstName, lastName: model.lastName, telephone: model.telephone ?? "", contactId: model.contactId, mail: model.mail ?? "" as String, status: type, image: model.image))
            self.saveContacts(with: contacts, type: type)
            self.changeStatusForUndo(with: model)
        }
	}
	
	func changeStatusForUndo(with model: CustomTestModel) {
        if var list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.deleted),
           list.count > 0, let firstItemIndex = list.firstIndex(where: { $0.contactId == model.contactId }) {
            list.remove(at: firstItemIndex)
            self.saveContacts(with: list, type: StorageConstant.deleted)
        }
        
        if var list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.favorite),
           list.count > 0, let firstItemIndex = list.firstIndex(where: { $0.contactId == model.contactId }) {
            list.remove(at: firstItemIndex)
            self.saveContacts(with: list, type: StorageConstant.favorite)
        }
        
        if var list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.swipe),
           list.count > 0, let firstItemIndex = list.firstIndex(where: { $0.contactId == model.contactId }) {
            list.remove(at: firstItemIndex)
            self.saveContacts(with: list, type: StorageConstant.swipe)
        }
	}
	
	func removeContactInCache(with model: CustomTestModel) {
        if var list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.deleted),
           list.count > 0, let firstItemIndex = list.firstIndex(where: { $0.contactId == model.contactId }) {
            list.remove(at: firstItemIndex)
            self.saveContacts(with: list, type: StorageConstant.deleted)
        }
	}
	
	func contactContentUpdateWithCache(with model: CustomTestModel, type: String) {
        if var list: [CustomTestModel] = getJSONFromCache(for: type),
           let firstItemIndex = list.firstIndex(where: { $0.contactId == model.contactId }) {
            list.remove(at: firstItemIndex)
            list.insert(model, at: 0)
            self.saveContacts(with: list, type: type)
        }
	}
	
	func updateCacheForNewContact(with model: [CustomTestModel], type: String, completion: @escaping (_ status: Bool) -> Void) {
        var contacts = [CustomTestModel]()
        DispatchQueue.main.async {
            if var list: [CustomTestModel] = self.getJSONFromCache(for: type) {
                model.forEach({ item in
                    list.append(CustomTestModel(firstName: item.firstName, lastName: item.lastName, telephone: item.telephone ?? "",
                                                contactId: item.contactId, mail: item.mail ?? "" as String, status: type, image: item.image))
                })
                
                let sortedContact = list.sorted { (initial, next) -> Bool in
                    guard initial.firstName != nil else { return true }
                    guard next.firstName != nil else { return true }
                    return initial.firstName!.compare(next.firstName!) == .orderedAscending
                }
                self.saveContacts(with: sortedContact, type: type)
                completion(true)
            } else {
                model.forEach({ item in
                    contacts.append(CustomTestModel(firstName: item.firstName, lastName: item.lastName, telephone: item.telephone ?? "",
                                                    contactId: item.contactId, mail: item.mail ?? "" as String, status: type, image: item.image))
                })
                self.saveContacts(with: contacts, type: type)
                completion(true)
            }
        }
	}
	
	func getContactsAll(with nativeAd: Bool) -> [CustomTestModel] {
		var contacts = [CustomTestModel]()
		var contactsWithAds = [CustomTestModel]()
		
        if let list: [CustomTestModel] = getJSONFromCache(for: StorageConstant.unknown), list.count > 0 {
            list.filter({ $0.status != StorageConstant.ads }).forEach({ item in
                contacts.append(CustomTestModel(firstName: item.firstName, lastName: item.lastName, telephone: item.telephone ?? "",
                                                contactId: item.contactId, mail: item.mail ?? "" as String, status: StorageConstant.unknown, image: item.image))
            })
        }
		
        if let data1: [CustomTestModel] = getJSONFromCache(for: StorageConstant.deleted) {
            contacts.append(contentsOf: data1)
        }
        if let data2: [CustomTestModel] = getJSONFromCache(for: StorageConstant.favorite) {
            contacts.append(contentsOf: data2)
        }
        if let data3: [CustomTestModel] = getJSONFromCache(for: StorageConstant.swipe) {
            contacts.append(contentsOf: data3)
        }
		
		if nativeAd && contacts.count > 0 {
			for index in 0...contacts.count - 1 {
				if (index % AppSingleton.shared.modNativeAds == 0) && index != 0 {
					contactsWithAds.append(CustomTestModel(firstName: "", lastName: "", telephone: "", contactId: "", mail: "", status: StorageConstant.ads, image: nil))
					
					contactsWithAds.append(CustomTestModel(firstName: contacts[index].firstName, lastName: contacts[index].lastName, telephone: contacts[index].telephone, contactId: contacts[index].contactId, mail: contacts[index].mail, status: contacts[index].status, image: contacts[index].image))
				} else {
					contactsWithAds.append(CustomTestModel(firstName: contacts[index].firstName, lastName: contacts[index].lastName, telephone: contacts[index].telephone, contactId: contacts[index].contactId, mail: contacts[index].mail, status: contacts[index].status, image: contacts[index].image))
				}
			}
			return contactsWithAds
		}
		
		return contacts
	}
	
	
	func updateContactInCache(with model: [CustomTestModel], completion: @escaping (_ status: Bool) -> Void) {
		var unknownContact = [CustomTestModel]()
		var deletedContact = [CustomTestModel]()
		var swipedContact = [CustomTestModel]()
		var favoriteContact = [CustomTestModel]()

		for item in model {
			if item.status == StorageConstant.deleted {
				deletedContact.append(item)
			} else if item.status == StorageConstant.swipe {
				swipedContact.append(item)
			}  else if item.status == StorageConstant.favorite {
				favoriteContact.append(item)
			} else {
				unknownContact.append(item)
			}
		}
        
        self.removeCache(for: StorageConstant.favorite)
        self.removeCache(for: StorageConstant.deleted)
        self.removeCache(for: StorageConstant.swipe)
        self.removeCache(for: StorageConstant.unknown)

		self.saveContacts(with: deletedContact, type: StorageConstant.deleted)
		self.saveContacts(with: swipedContact, type: StorageConstant.swipe)
		self.saveContacts(with: favoriteContact, type: StorageConstant.favorite)
		self.saveContacts(with: unknownContact, type: StorageConstant.unknown)
		completion(true)
	}
}
