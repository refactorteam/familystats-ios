//
//  StoryboardConstant.swift
// FamilyStats
//
//  Created by Can Koç on 11.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct StoryboardConstant {
    
    static let onboarding = "Onboarding"
    static let main = "Main"
    static let profile = "Profile"
    static let profileCreate = "ProfileCreate"
    static let search = "Search"
    static let circle = "Circle"
    static let message = "Message"
    static let verification = "Verification"
    static let other = "Other"
    
    // contact manager
    
    static let contactManager = "ContactManager"
    static let guide = "Guide"
    static let custom = "CustomView"
    static let menu = "Menu"
    static let interstitialAdLoader = "InterstitialAdLoaderView"
}
