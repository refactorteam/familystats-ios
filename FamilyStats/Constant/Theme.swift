//
//  Theme.swift
// FamilyStats
//
//  Created by Can Koç on 2.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct Theme {
    
    enum Font: String {
        case montserratBold = "Montserrat-Bold"
        case montserratLight = "Montserrat-Light"
        case montserratRegular = "Montserrat-Regular"
        
        case poppinsBold = "Poppins-Bold"
        case poppinsMedium = "Poppins-Medium"
        case poppinsRegular = "Poppins-Regular"
        
        func of(size: CGFloat) -> UIFont {
            return UIFont(name: self.rawValue, size: size)!
        }
    }
    
    
    struct Color {
        static let purple = UIColor(red: 90/255, green: 56/255, blue: 186/255, alpha: 1)
        static let navyBlue = UIColor(red: 0.0/255.0, green: 39.0/255.0, blue: 75.0/255.0, alpha: 1)
        static let darkSkyBlue = UIColor(red: 89.0/255.0, green: 140.0/255.0, blue: 225.0/255.0, alpha: 1)
        static let white = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        static let darkGray = UIColor(red: 127.0/255.0, green: 135.0/255.0, blue: 148.0/255.0, alpha: 1)
        static let lightGray = UIColor(red: 237.0/255.0, green: 241.0/255.0, blue: 247.0/255.0, alpha: 1)
        
        struct Chart {
            static let yellowBackgroundColor = UIColor(red: 254/255, green: 248/255, blue: 224/255, alpha: 1)
            static let yellowFillColor = UIColor(red: 255/255, green: 220/255, blue: 22/255, alpha: 1)
            
            static let redBackgroundColor = UIColor(red: 242/255, green: 172/255, blue: 172/255, alpha: 1)
            static let redFillColor = UIColor(red: 236/255, green: 72/255, blue: 38/255, alpha: 1)
            
            static let greenBackgroundColor = UIColor(red: 195/255, green: 242/255, blue: 172/255, alpha: 1)
            static let greenFillColor = UIColor(red: 126/255, green: 192/255, blue: 102/255, alpha: 1)
        }
    }
    
    
    struct Image {
        static let arrowRightWhite = UIImage(named: "icon-arrow-right-white")!
        static let arrowLeftWhite = UIImage(named: "icon-arrow-left-white")!
    }
}
