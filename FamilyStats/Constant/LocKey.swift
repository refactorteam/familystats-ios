//
//  LocKey.swift
// FamilyStats
//
//  Created by Can Koç on 18.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct LocKey {
    
    struct Alert {
        private static let dialogBase = "alert"
        
        struct General {
            private static let base = dialogBase + ".general"
            
            static let warning = base + ".warning"
            static let warningAlert = base + ".warningAlert"
            static let buttonOK = base + ".btnOK"
            static let buttonCancel = base + ".btnCancel"
        }
    }
    
    struct Dialog {
        private static let dialogBase = "alert"
        
        struct General {
            private static let base = dialogBase + ".general"
            
            static let warning = base + ".warning"
            static let buttonOK = base + ".btnOK"
            static let buttonCancel = base + ".btnCancel"
            static let buttonPackageUpgrade = base + ".btnPackageUpgrade"
        }
        
        struct SoftUpdate {
            private static let base = dialogBase + ".softUpdate"
            
            static let title = base + ".title"
            static let buttonUpdate = base + ".btnUpdate"
        }
        
        struct ForceUpdate {
            private static let base = dialogBase + ".forceUpdate"
            
            static let title = base + ".title"
            static let buttonUpdate = base + ".btnUpdate"
        }
        
        struct Receipt {
            private static let base = dialogBase + ".receipt"
            
            static let error = base + ".error"
            static let restoreError = base + ".restoreError"
        }
    }
    
    struct Loading {
        private static let base = "loading"
        
        static let contact = base + ".contact"
    }
    
    struct View {
        private static let viewBase = "view"
        
        struct General {
            private static let base = viewBase + ".general"
            
            static let done = base + ".done"
        }
        
        struct Onboarding {
            private static let base = viewBase + ".onboarding"
            
            static let title1 = base + ".title1"
            static let title2 = base + ".title2"
            static let title3 = base + ".title3"
            static let description1 = base + ".description1"
            static let description2 = base + ".description2"
            static let description3 = base + ".description3"
            
            static let buttonNext = base + ".btnNext"
            static let descriptionTerms = base + ".descriptionTerms"
            static let buttonPrivacy = base + ".btnPrivacy"
            static let buttonTerms = base + ".btnTerms"
        }
        
        struct Other {
            private static let base = viewBase + ".other"
            
            static let title = base + ".title"
            static let menuContact = base + ".menu.contact"
            static let menuPrivacy = base + ".menu.privacy"
            static let menuTerms = base + ".menu.terms"
            static let menuAboutSubscription = base + ".menu.aboutSubscription"
            static let menuPackage = base + ".menu.package"
            static let menuUsage = base + ".menu.usage"
            static let menuLogout = base + ".menu.logout"
            
            static let premiumTitle = base + ".premium.title"
            static let premiumDescription = base + ".premium.description"
            static let premiumButtonGetNow = base + ".premium.btnGetNow"
        }
        
        struct Contact {
            private static let base = viewBase + ".contact"
            
            static let title = base + ".title"
            static let txtName = base + ".txtName"
            static let txtEmail = base + ".txtEmail"
            static let txtMessage = base + ".txtMessage"
            static let buttonSendMessage = base + ".btnSendMessage"
            
            static let reasonKeyword = base + ".reasonKeyword"
            static let reasonPackage = base + ".reasonPackage"
            
            static let nameNullValidation = base + ".nameNullValidation"
            static let emailNullValidation = base + ".emailNullValidation"
            static let emailFormatValidation = base + ".emailFormatValidation"
            static let messageNullValidation = base + ".messageNullValidation"
        }
        
        struct Package {
            private static let base = viewBase + ".package"
            
            static let title = base + ".title"
            static let cancelDescription = base + ".cancelDescription"
            static let aboutSubsTitle = base + ".aboutSubsTitle"
            static let aboutSubsDescription = base + ".aboutSubsDescription"
            
            static let buttonSubscribe = base + ".btnSubscribe"
            static let buttonRestore = base + ".btnRestore"
            static let buttonPrivacy = base + ".btnPrivacy"
            static let buttonTerms = base + ".btnTerms"
        }
        
        struct Ad {
            private static let base = viewBase + ".ad"
            
            static let sponsored = base + ".sponsored"
            static let removeAd = base + ".removeAd"
        }
        
        struct Report {
            private static let base = viewBase + ".report"
            
            static let title = base + ".title"
            static let noData = base + ".noData"
            
            static let segmentToday = base + ".segmentToday"
            static let segmentWeekly = base + ".segmentWeekly"
            static let segmentMonthly = base + ".segmentMonthly"
            
            static let sessionCountTitle = base + ".sessionCountTitle"
            static let totalUsageTitle = base + ".totalUsageTitle"
            
            static let hour = base + ".hours"
            static let minutes = base + ".minutes"
            static let seconds = base + ".seconds"
        }
        
        struct FullImagePackage {
            private static let base = viewBase + ".fullImagePackage"
            
            static let description = base + ".description"
            static let buttonSubscribe = base + ".btnSubscribe"
            static let price = base + ".price"
            
            static let buttonSubscribeAdjust = base + ".btnSubscribeAdjust"
            static let priceAdjust = base + ".priceAdjust"
        }
        
        struct LogoPackage {
            private static let base = viewBase + ".logoPackage"
            
            static let description = base + ".description"
            static let buttonSubscribe = base + ".btnSubscribe"
            static let price = base + ".price"
            
            static let buttonSubscribeAdjust = base + ".btnSubscribeAdjust"
            static let priceAdjust = base + ".priceAdjust"
        }
        
        struct SwitchPackage {
            private static let base = viewBase + ".switchPackage"
            
            static let title = base + ".title"
            static let notSure = base + ".notSure"
            static let buttonSubscribe = base + ".btnSubscribe"
            static let price = base + ".price"
            static let adFree = base + ".adFree"
            static let backwardReport = base + ".backwardReport"
            static let notification = base + ".notification"
            
            static let sure = base + ".sure"
            static let buttonSubscribeSure = base + ".btnSubscribeSure"
            
            
            static let buttonSubscribeAdjust = base + ".btnSubscribeAdjust"
            static let buttonSubscribeSureAdjust = base + ".btnSubscribeSureAdjust"
            static let priceAdjust = base + ".priceAdjust"
            
            static let sureAdjust = base + ".sureAdjust"
            static let notSureAdjust = base + ".notSureAdjust"
        }
        
        struct Main {
            private static let base = viewBase + ".main"
            
            static let title = base + ".title"
            static let enterPhoneNumber = base + ".enterPhoneNumber"
            static let enterName = base + ".enterName"
            static let instantNotifications = base + ".instantNotifications"
            static let buttonStartTracking = base + ".btnStartTracking"
            
            static let noDataDescription = base + ".noDataDescription"
            
            static let nameNullValidation = base + ".nameNullValidation"
            static let phoneNumberNullValidation = base + ".phoneNumberNullValidation"
            static let phoneNumberNotValidValidation = base + ".phoneNumberNotValidValidation"
            
            static let maxNumberAlert = base + ".maxNumberAlert"
            static let duplicateNumberAlert = base + ".duplicateNumberAlert"
            static let generalErrorAlert = base + ".generalErrorAlert"
            static let freeTrialAlert = base + ".freeTrialAlert"
            
            static let userOnline = base + ".userOnline"
            static let userOffline = base + ".userOffline"
            
            static let tomorrow = base + ".tomorrow"
            static let yesterday = base + ".yesterday"
            
            static let deleteNumber = base + ".deleteNumber"
            static let muteNumber = base + ".muteNumber"
            static let unmuteNumber = base + ".unmuteNumber"
            
            static let createGroupTitle = base + ".createGroupTitle"
            static let alertGroupLimit = base + ".alertGroupLimit"
            
            static let ownNumberCautionTitle = base + ".ownNumberCautionTitle"
            static let ownNumberCaution = base + ".ownNumberCaution"
            
            static let groupName = base + ".groupRename"
        }
        
        struct History {
            private static let base = viewBase + ".history"
            
            static let duration = base + ".duration"
            static let report = base + ".report"
            
            static let title = base + ".title"
            static let onlineTime = base + ".onlineTime"
            static let total = base + ".total"
        }
        
        struct CreateGroup {
            private static let base = viewBase + ".createGroup"
            
            static let title = base + ".title"
            static let txtGroupName = base + ".txtGroupName"
            static let instantNotifications = base + ".instantNotifications"
            static let info1 = base + ".info1"
            static let info2 = base + ".info2"
            static let buttonStart = base + ".buttonStart"
            
            static let groupNameNullValidation = base + ".groupNameNullValidation"
            
            static let buttonRename = base + ".btnRename"
        }
        
        struct GroupHistory {
            private static let base = viewBase + ".groupHistory"
            
            static let title = base + ".title"
            static let btnEdit = base + ".buttonEdit"
            static let btnDelete = base + ".buttonDelete"
            static let removeDescription = base + ".removeDescription"
        }
        
        struct Privacy {
            private static let base = viewBase + ".privacy"
            
            static let title = base + ".title"
            static let description = base + ".description"
            static let settingsDescription = base + ".settingsDescription"
            static let buttonContinue = base + ".btnContinue"
            static let buttonCancel = base + ".btnCancel"
        }
        
        struct NumberVerification {
            private static let base = viewBase + ".numberVerification"
            
            static let title = base + ".title"
            static let buttonNext = base + ".btnNext"
            static let buttonClose = base + ".btnClose"
            
            static let alertNumberNull = base + ".alertNumberNull"
            static let alertGeneralError = base + ".alertGeneralError"
        }
        
        struct CodeVerification {
            private static let base = viewBase + ".codeVerification"
            
            static let title = base + ".title"
            static let buttonNext = base + ".btnNext"
            static let alertNumberNull = base + ".alertNumberNull"
        }
        
        // MARK: - ContactManager
        
        struct GuideLine {
            private static let base = viewBase + ".guideLine"
            
            static let cardName = base + ".cardName"
            static let cardSurname = base + ".cardSurname"
            static let cardPhone = base + ".cardPhone"
            static let cardEmail = base + ".cardEmail"
            static let cardMoreButton = base + ".cardMoreButton"
            static let swipeInfo = base + ".swipeInfo"
            static let nextButtonTitle = base + ".nextButtonTitle"
            static let deleteInfo = base + ".deleteInfo"
            static let favoriteInfo = base + ".favoriteInfo"
            static let undoInfo = base + ".undoInfo"
        }
        
        struct ContactManager {
            private static let base = viewBase + ".contactManager"
            
            static let title = base + ".title"
            static let allDone = base + ".allDone"
            static let allDoneDescription = base + ".allDoneDescription"
            static let btnStartOver = base + ".btnStartOver"
            static let cardsMoreButtonTitle = base + ".cardsMoreButtonTitle"
            static let cardsNoEmailDesc = base + ".cardsNoEmailDesc"
        }
        
        struct Language {
            private static let base = viewBase + ".language"
            
            static let title = base + ".title"
            static let txtLanguage = base + ".txtLanguage"
        }
        
        struct Welcome {
            private static let base = viewBase + ".welcome"
            
            static let title = base + ".title"
            static let description = base + ".description"
            static let btnContinueTitle = base + ".btnContinueTitle"
            static let bottomDescription = base + ".bottomDescription"
        }
        
        struct MergeContacts {
            private static let base = viewBase + ".mergeContacts"
            
            static let title = base + ".title"
            static let description = base + ".description"
            static let btnSearchTitle = base + ".btnSearchTitle"
            static let btnMergeTitle = base + ".btnMergeTitle"
            
            static let mergeInfoTitle = base + ".mergeInfoTitle"
            static let mergeInfoDescription = base + ".mergeInfoDescription"
            static let mergeInfoBtnOkTitle = base + ".mergeInfoBtnOkTitle"
            static let mergeInfoBtnCancelTitle = base + ".mergeInfoBtnCancelTitle"
            static let searchPlaceHolder = base + ".searchPlaceHolder"
        }
        
        struct AlertContacts {
            private static let base = viewBase + ".alertContacts"
            
            static let btnAllTitle = base + ".btnAllTitle"
            static let woNumbers = base + ".woNumbers"
            static let buttonDoneTitle = base + ".buttonDoneTitle"
        }
        
        struct Alert {
            private static let base = viewBase + ".alert"
            
            static let contactPermissionTitle = base + ".contactPermissionTitle"
            static let contactPermissionDesc = base + ".contactPermissionDesc"
            static let contactPermissionOk = base + ".contactPermissionOk"
            static let contactPermissionCancel = base + ".contactPermissionCancel"
            static let woContactAlertTitle = base + ".woContactAlertTitle"
            static let woContactAlertDesc = base + ".woContactAlertDesc"
            static let woContactAlertButtonTitle = base + ".woContactAlertButtonTitle"
            static let allContactAlertTitle = base + ".woContactAlertTitle"
            static let allContactAlertDesc = base + ".woContactAlertDesc"
            static let allContactAlertButtonTitle = base + ".woContactAlertButtonTitle"
        }
        
        struct ProfileEdit {
            private static let base = viewBase + ".profileEdit"
            
            static let title = base + ".title"
            static let description = base + ".description"
            static let btnSaveTitle = base + ".btnSaveTitle"
            static let placeHolderNoName = base + ".placeHolderNoName"
            static let placeHolderNoPhone = base + ".placeHolderNoPhone"
            static let placeHolderNoEmail = base + ".placeHolderNoEmail"
            static let placeHolderNoSurname = base + ".placeHolderNoSurname"
        }
        
        struct SideMenu {
            private static let base = viewBase + ".sideMenu"
            
            static let stats = base + ".stats"
            static let mergeContacts = base + ".mergeContacts"
            static let deletedContacts = base + ".deletedContacts"
            static let backupContacts = base + ".backupContacts"
            static let contactUs = base + ".contactUs"
            static let terms = base + ".terms"
            static let privacy = base + ".privacy"
            static let profile = base + ".profile"
        }
        
        struct ContactPermission {
            private static let base = viewBase + ".contactPermission"
            
            static let title = base + ".title"
            static let description = base + ".description"
            static let btnSettings = base + ".btnSettings"
        }
        
        struct Stats {
            private static let base = viewBase + ".stats"
            
            static let title = base + ".title"
            static let swipedTitle = base + ".swipedTitle"
            static let swipedDescription = base + ".swipedDescription"
            static let deletedTitle = base + ".deletedTitle"
            static let deletedDescription = base + ".deletedDescription"
            static let favoritedTitle = base + ".favoritedTitle"
            static let favoritedDescription = base + ".favoritedDescription"
            static let btnResetTitle = base + ".btnResetTitle"
        }
        
        struct DeletedContacts {
            private static let base = viewBase + ".deletedContacts"
            
            static let title = base + ".title"
            static let btnDeleteTitle = base + ".btnDeleteTitle"
            static let alertTitle = base + ".alertTitle"
            static let alertDescripton = base + ".alertDescription"
            static let okBtnTitle = base + ".okBtnTitle"
            static let noBtnTitle = base + ".noBtnTitle"
            static let noDataInfo = base + ".noDataInfo"
            static let btnUndoTitle = base + ".btnUndoTitle"
            static let alertTitleUndo = base + ".alertTitleUndo"
            static let alertDescriptionUndo = base + ".alertDescriptionUndo"
            static let okBtnTitleUndo = base + ".okBtnTitleUndo"
            static let noBtnTitleUndo = base + ".noBtnTitleUndo"
        }
    }
    
    struct TabBar {
        private static let base = "tabbar"
        
        static let dashboard = base + ".dashboard"
        static let contactManager = base + ".contactManager"
        static let other = base + ".other"
    }
}
