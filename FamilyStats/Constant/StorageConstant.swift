//
//  StorageConstant.swift
// FamilyStats
//
//  Created by Can Koç on 1.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct StorageConstant {
    static let userHistory = "user_history"
    static let groupHistory = "group_history"
    static let trialUsed = "trial_used"
    
    // MARK: - ContactManager
    
    static let unknown = "contact_unknown"
    static let deleted = "contact_deleted"
    static let favorite = "contact_favorite"
    static let swipe = "contact_swipe"
    static let ads = "contact_ads"
    
    static let guideCompleted = "guide_completed"
}


