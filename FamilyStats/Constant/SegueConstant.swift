//
//  SegueConstant.swift
// FamilyStats
//
//  Created by Can Koç on 26.02.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct SegueConstant {
    static let contact = "segContact"
    static let webView = "segWebView"
    
    static let history = "segHistory"
    static let report = "segReport"
    
    static let createGroup = "segCreateGroup"
    static let editGroup = "segEditGroup"
    static let groupHistory = "segGroupHistory"
    static let groupReport = "segGroupReport"
    static let privacy = "segPrivacy"
    static let codeVerification = "segCodeVerification"
}
