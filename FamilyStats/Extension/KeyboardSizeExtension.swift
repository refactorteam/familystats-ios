//
//  KeyboardSizeExtension.swift
// FamilyStats
//
//  Created by Can Koç on 1.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxSwift

class KeyboardService {
    
    static func keyboardHeight() -> Observable<CGFloat> {
        return Observable
                .from([NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification).map { notification -> CGFloat in
                    (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
                },
                NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification).map { _ -> CGFloat in
                    0
                }]).merge()
    }
}
