//
//  StringExtension.swift
//  FamilyStats
//
//  Created by Can Koç on 16.09.2022.
//  Copyright © 2022 Can Koç. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
	public var initials: String {
		var finalString = String()
		var words = components(separatedBy: .whitespacesAndNewlines)
		
		if let firstCharacter = words.first?.first {
			finalString.append(String(firstCharacter))
			words.removeFirst()
		}
		
		if let lastCharacter = words.last?.first {
			finalString.append(String(lastCharacter))
		}
		return finalString.uppercased()
	}
}
