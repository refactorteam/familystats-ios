//
//  UIImageExtension.swift
// FamilyStats
//
//  Created by Can Koç on 4.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit

extension UIImage {
    
  func mergeWith(topImage: UIImage) -> UIImage {
    let bottomImage = self

    UIGraphicsBeginImageContextWithOptions(size, false, 0)

    let areaSize = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
    bottomImage.draw(in: areaSize)

    let topAreaSize = CGRect(x: 24, y: 22, width: 60, height: 60)
    topImage.draw(in: topAreaSize, blendMode: .normal, alpha: 1.0)

    let mergedImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return mergedImage
  }
}
