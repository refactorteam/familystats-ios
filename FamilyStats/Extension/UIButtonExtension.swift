//
//  UIButtonExtension.swift
// FamilyStats
//
//  Created by Can Koç on 7.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit

extension UIButton {
    
    func toggleUserInteraction() {
        self.isUserInteractionEnabled = !self.isUserInteractionEnabled
        self.isEnabled = !self.isEnabled
    }
}
