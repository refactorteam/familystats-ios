//
//  UITextFieldExtension.swift
// FamilyStats
//
//  Created by Can Koç on 1.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit

class TextFieldWBackSpace: UITextField {
    
    var backSpaceDelegate: BackSpaceDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
    
        backSpaceDelegate?.didBackSpaceTapped(on: self)
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
}
