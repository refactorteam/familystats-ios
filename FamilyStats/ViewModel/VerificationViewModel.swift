//
//  VerificationViewModel.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB
import RxCocoa
import RxSwift
import FirebaseAuth
import PhoneNumberKit

class VerificationViewModel: BaseViewModel {
    
    private let phoneNumberKit = PhoneNumberKit()
    
    var phoneNumber = BehaviorRelay(value: String())
    
    override func viewModelDidLoad() {
        super.viewModelDidLoad()
    }
}

// MARK: - Action

extension VerificationViewModel: LoaderShowable, Validatable {
    
    func actionSendVerifyCode() -> Observable<(phoneNumber: String, verificationId: String)> {
        return Observable.create({ observer in
            
            if self.isValid(text: self.phoneNumber.value) == false {
                observer.on(.error(NumberRegistrationError.phoneNumberNull))
            } else {
                do {
                    let parsedNumber = try self.phoneNumberKit.parse(self.phoneNumber.value)
                    let numberString = parsedNumber.numberString
                    self.showLoader()
                    PhoneAuthProvider.provider().verifyPhoneNumber(numberString, uiDelegate: nil) { (verificationId, error) in
                        
                        self.hideLoader()
                        if let error = error {
                            observer.on(.error(NumberRegistrationError.firebaseError(errorMessage: error.localizedDescription)))
                            return
                        } else if let verificationId = verificationId {
                            observer.on(.next((phoneNumber: numberString, verificationId: verificationId)))
                        } else {
                            observer.on(.error(NumberRegistrationError.unknown))
                        }
                    }
                } catch {
                    observer.on(.error(NumberRegistrationError.parseError(errorMessage: error.localizedDescription)))
                }
            }
            
            return Disposables.create()
        })
        
    }
}

