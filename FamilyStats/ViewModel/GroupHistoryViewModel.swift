//
//  GroupHistoryViewModel.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxCocoa
import CORELIB

class GroupHistoryViewModel: BaseViewModel {
    
    var phoneNumber = BehaviorRelay<String>(value: String())
    var historyList = BehaviorRelay<[UserHistoryModel]>(value: [])
    
    override func viewModelDidLoad() {
        super.viewModelDidLoad()
        
        getAllHistory()
    }
}

// MARK: - Get

extension GroupHistoryViewModel: LoaderShowable {
    
    func getAllHistory() {
        guard let group = AppSingleton.shared.userTrackingList.value?.groups.first else {
            return
        }
        
        showLoader()
        let lastKey: Int = getDataToCache() ?? 0
        FirebaseManager.shared.authUser().flatMap({ _ in
            FirebaseManager.shared.getDataForGroupHistory(groupKey: group.key, lastKey: lastKey)
        }).subscribe(onNext: { [weak self] (result: [String: GCHistoryModel]?) in
            
            self?.saveDataToCache(with: result ?? [:])
            
            var currentList = self?.historyList.value ?? []
            let dateHelper = DateHelper()
            result?.forEach({ historyItem in
                
                if (currentList.filter({ item -> Bool in
                    return item.inTimeTimeStampForOrder == historyItem.value.inTime
                    }).isEmpty) {
                    let inTime = historyItem.value.inTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
                    let inTimeDate = historyItem.value.inTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
                    let outTime = historyItem.value.outTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
                    let outTimeDate = historyItem.value.outTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
                    
                    var duration = "-"
                    if historyItem.value.outTime != 0 {
                        duration = dateHelper.compareTwoDates(timeStamp1: TimeInterval(historyItem.value.inTime), timeStamp2: TimeInterval(historyItem.value.outTime))
                    }
                    
                    let newItem = UserHistoryModel(inTimeTimeStampForOrder: historyItem.value.inTime,
                                                   outTimeTimeStampForOrder: historyItem.value.outTime,
                                                   inTime: inTime,
                                                   inTimeDate: inTimeDate,
                                                   outTime: outTime,
                                                   outTimeDate: outTimeDate,
                                                   duration: duration)
                    currentList.append(newItem)
                }
                
            })
            currentList.sort { (item1, item2) -> Bool in
                return item1.inTimeTimeStampForOrder > item2.inTimeTimeStampForOrder
            }
            self?.historyList.accept(currentList)
            self?.hideLoader()
            
        }).disposed(by: disposeBag)
    }
}

// MARK: - Action

extension GroupHistoryViewModel {
    
    func actionRemoveGroup() {
        AlertManager.shared.show(title: LocKey.View.GroupHistory.removeDescription.localize, message: nil, action: [UIAlertAction(title: LocKey.View.GroupHistory.btnDelete.localize, style: .destructive, handler: { _ in
            self.actionRemoveGroupToFirebase()
        }), UIAlertAction(title: LocKey.Dialog.General.buttonCancel.localize, style: .cancel, handler: nil)])
    }
    
    private func actionRemoveGroupToFirebase() {
        self.showLoader()
        FirebaseManager.shared.authUser().flatMap({ _ in
            FirebaseManager.shared.sendData(.destroyGroup, requestModel: GCGroupDestroyRequestModel())
        }).subscribe(onNext: { [weak self] (result: GCGroupDestroyResponseModel?) in
            self?.hideLoader()
            if result?.error == false {
                /*var currentValue = AppSingleton.shared.userSettings.value
                currentValue.group = nil
                AppSingleton.shared.userSettings.accept(currentValue)
                self?.presentedViewController.navigationController?.popViewController(animated: true)
                 */
            }
        }).disposed(by: disposeBag)
    }
}

// MARK: - Cache

extension GroupHistoryViewModel: Cacheable {
    
    private func saveDataToCache(with newData: [String: GCHistoryModel]) {
        if !newData.isEmpty {
            var cachedData: [GroupHistoryCacheModel]? = getJSONFromCache(for: StorageConstant.groupHistory)
            /*let groupKey = AppSingleton.shared.userSettings.value.group?.key ?? String()
            if cachedData == nil {
                var currentList: [String: GroupHistoryCacheModel.HistoryModel] = [:]
                newData.forEach({ historyItem in
                    currentList[historyItem.key] = GroupHistoryCacheModel.HistoryModel(inTime: historyItem.value.inTime, outTime: historyItem.value.outTime)
                })
                cachedData = [GroupHistoryCacheModel(groupKey: self.phoneNumber.value, lastKey: newData.first?.key ?? String(), data: currentList)]
            } else {
                if let firstIndex: Int = cachedData?.firstIndex(where: { item in
                    item.groupKey == groupKey
                }) {
                    var currentList = cachedData?[firstIndex].data ?? [:]
                    newData.forEach({ historyItem in
                        currentList[historyItem.key] = GroupHistoryCacheModel.HistoryModel(inTime: historyItem.value.inTime, outTime: historyItem.value.outTime)
                    })
                    cachedData?[firstIndex].data = currentList
                    cachedData?[firstIndex].lastKey = newData.first?.key ?? String()
                    cachedData?[firstIndex].groupKey = groupKey
                }
                else {
                    var currentList: [String: GroupHistoryCacheModel.HistoryModel] = [:]
                    newData.forEach({ historyItem in
                        currentList[historyItem.key] = GroupHistoryCacheModel.HistoryModel(inTime: historyItem.value.inTime, outTime: historyItem.value.outTime)
                    })
                    cachedData?.append(GroupHistoryCacheModel(groupKey: groupKey, lastKey: newData.first?.key ?? String(), data: currentList))
                }
            }
            
            saveToCache(with: cachedData, for: StorageConstant.groupHistory)
             */
        }
        
    }
    
    private func getDataToCache() -> Int? {
        /*guard let cachedData: [GroupHistoryCacheModel] = getJSONFromCache(for: StorageConstant.groupHistory),
            let firstIndex = cachedData.firstIndex(where: { item in
                item.groupKey == AppSingleton.shared.userSettings.value.group?.key ?? String()
            }) else {
            return nil
        }
        
        createList(with: cachedData[firstIndex].data)
        return self.historyList.value.first?.inTimeTimeStampForOrder*/
        return nil
    }
    
    private func createList(with data: [String: GroupHistoryCacheModel.HistoryModel]) {
        var newList: [UserHistoryModel] = []
        let dateHelper = DateHelper()
        data.forEach({ historyItem in
           let inTime = historyItem.value.inTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
           let inTimeDate = historyItem.value.inTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
           let outTime = historyItem.value.outTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
           let outTimeDate = historyItem.value.outTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
           
           var duration = "-"
           if historyItem.value.outTime != 0 {
               duration = dateHelper.compareTwoDates(timeStamp1: TimeInterval(historyItem.value.inTime), timeStamp2: TimeInterval(historyItem.value.outTime))
           }
           
           let newItem = UserHistoryModel(inTimeTimeStampForOrder: historyItem.value.inTime,
                                          outTimeTimeStampForOrder: historyItem.value.outTime,
                                          inTime: inTime,
                                          inTimeDate: inTimeDate,
                                          outTime: outTime,
                                          outTimeDate: outTimeDate,
                                          duration: duration)
           newList.append(newItem)
        })
        newList.sort { (item1, item2) -> Bool in
            return item1.inTimeTimeStampForOrder > item2.inTimeTimeStampForOrder
        }
       self.historyList.accept(newList)
    }
}
