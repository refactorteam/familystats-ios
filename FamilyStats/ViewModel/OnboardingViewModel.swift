//
//  OnboardingViewModel.swift
// FamilyStats
//
//  Created by Can Koç on 11.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import CORELIB

class OnboardingViewModel: BaseViewModel {
    
    var onboardingList = BehaviorRelay<[OnboardingModel]>(value: [])
    
    override func viewModelDidLoad() {
        super.viewModelDidLoad()
        
        onboardingList.accept([
            OnboardingModel(image: UIImage(named: "imgOnboarding1")!, title: LocKey.View.Onboarding.title1.localize,
                            description: LocKey.View.Onboarding.description1.localize),
            OnboardingModel(image: UIImage(named: "imgOnboarding2")!, title: LocKey.View.Onboarding.title2.localize,
                            description: LocKey.View.Onboarding.description2.localize),
            OnboardingModel(image: UIImage(named: "imgOnboarding3")!, title: LocKey.View.Onboarding.title3.localize,
                            description: LocKey.View.Onboarding.description3.localize)
        ])
        
        actionGetPrivacyState()
    }
    
    func actionGetPrivacyState() {
        if let showPrivacyPage = CORELIB.shared.readAppInfoValue(for: "show_privacy_page"),
           (CORELIB.shared.readAppInfoValue(for: "apptrack_on_register") == nil ||
                CORELIB.shared.readAppInfoValue(for: "apptrack_on_register") == "false") {
            AppSingleton.shared.showPrivacy.accept((showPrivacyPage == "true"))
        }
    }
}
