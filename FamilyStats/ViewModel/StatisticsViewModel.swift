//
//  StatisticsViewModel.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxCocoa
import Charts
import CORELIB

class StatisticsViewModel: BaseViewModel {
    
    var userHistory = BehaviorRelay<[UserHistoryModel]>(value: [])
    var dateList = [String]()
    var countList = [Int]()
    
    var sessionCount = 0
    var usageSeconds = 0
    
    enum DateType: Int {
        case today = 0
        case week = 1
        case month = 2
    }
    
    func calculateData(for dateType: DateType) {
        let historyList = userHistory.value
        usageSeconds = 0
        sessionCount = 0
        
        dateList = []
        countList = []
        let dateHelper = DateHelper()
        historyList.forEach({ historyItem in
            let dateIn = dateHelper.getDate(timeStamp: TimeInterval(historyItem.inTimeTimeStampForOrder))
            let dateOut = dateHelper.getDate(timeStamp: TimeInterval(historyItem.outTimeTimeStampForOrder))

            switch dateType {
            case .today:
                self.calculateToday(with: dateIn, dateOut: dateOut)
            case .week:
                self.calculateWeek(with: dateIn, dateOut: dateOut)
            case .month:
                self.calculateMonth(with: dateIn, dateOut: dateOut)
            }
        })
    }
    
    func getBindableData() -> LineChartData {
        var values: [ChartDataEntry] = []
        var index: Double = 0
        
        countList.forEach({ item in
            values.append((ChartDataEntry(x: index, y: Double(item))))
            index += 1
            sessionCount += item
        })
        
        let set1 = LineChartDataSet(entries: values, label: "")
        set1.cubicIntensity = 0
        set1.lineWidth = 3
        set1.fillAlpha = 1
        set1.drawFilledEnabled = true
        set1.setColor(UIColor(red: 67/255, green: 97/255, blue: 238/255, alpha: 1))
        set1.setCircleColor(UIColor(red: 67/255, green: 97/255, blue: 238/255, alpha: 1))
        set1.highlightColor = UIColor(red: 67/255, green: 97/255, blue: 238/255, alpha: 1)
        set1.drawValuesEnabled = false
        
        let gradientColors = [UIColor(red: 67/255, green: 97/255, blue: 238/255, alpha: 0.5).cgColor,
                              UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0).cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        set1.fill = Fill(linearGradient: gradient, angle: 180) //.linearGradient(gradient, angle: 90)
        
        if countList.count > 1 {
            set1.drawCirclesEnabled = false
        } else {
            set1.drawCirclesEnabled = true
        }
        
        let data = LineChartData(dataSet: set1)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        data.setValueFormatter(formatter)
        
        return data
    }
}

// MARK: - Calculator

extension StatisticsViewModel {
    
    private func calculateUsage(_ dateIn: Date, _ dateOut: Date) {
        usageSeconds += compareTwoDates(date1: dateIn, date2: dateOut)
    }
    
    private func compareTwoDates(date1: Date, date2: Date) -> Int {
        
        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: date1)
        let nowComponents = calendar.dateComponents([.hour, .minute, .second], from: date2)
        
        if let difference = calendar.dateComponents([.second], from: timeComponents, to: nowComponents).second, difference > 0 {
            return Int(difference)
        } else {
            return 0
        }
    }
    
    // MARK: - DateType Calculation
    
    private func calculateToday(with date: Date, dateOut: Date) {
        let calendar = Calendar.current
        if date.isInToday {
            let timeComponents = calendar.dateComponents([.hour], from: date)
            if let hour = timeComponents.hour, hour != 0 {
                
                var formattedHour = String(hour)
                if formattedHour.count == 1 {
                    formattedHour = "0" + formattedHour
                }
                formattedHour += ":00"
                
                if let firstIndex = dateList.firstIndex(of: formattedHour) {
                    countList[firstIndex] += 1
                } else {
                    countList.append(1)
                    dateList.append(formattedHour)
                }
                
                self.calculateUsage(date, dateOut)
            }
        }
    }
    
    private func calculateWeek(with date: Date, dateOut: Date) {
        let calendar = Calendar.current
        if date.isInThisWeek {
            let timeComponents = calendar.dateComponents([.day], from: date)
            if let day = timeComponents.day, day != 0 {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                let formattedDay = dateFormatter.string(from: date)
                
                if let firstIndex = dateList.firstIndex(of: formattedDay) {
                    countList[firstIndex] += 1
                } else {
                    countList.append(1)
                    dateList.append(formattedDay)
                }
                
                self.calculateUsage(date, dateOut)
            }
        }
    }
    
    private func calculateMonth(with date: Date, dateOut: Date) {
        let calendar = Calendar.current
        if date.isInThisMonth {
            let timeComponents = calendar.dateComponents([.day], from: date)
            if let day = timeComponents.day, day != 0 {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                let formattedDay = dateFormatter.string(from: date)
                
                if let firstIndex = dateList.firstIndex(of: formattedDay) {
                    countList[firstIndex] += 1
                } else {
                    countList.append(1)
                    dateList.append(formattedDay)
                }
                
                self.calculateUsage(date, dateOut)
            }
        }
    }
}
