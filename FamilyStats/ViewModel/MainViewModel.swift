//
//  BaseViewModel.swift
// FamilyStats
//
//  Created by Can Koç on 18.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import CORELIB
import RxCocoa
import RxSwift
import CoreLocation
import PhoneNumberKit
import Kingfisher

class MainViewModel: BaseViewModel {
    
    // MARK: - Input
    
    let phoneNumber = BehaviorRelay<String>(value: String())
    let name = BehaviorRelay<String>(value: String())
    var userDisposer: Disposable?
    
    private let phoneNumberKit = PhoneNumberKit()
    
    // MARK: - Output
    
    let trackingList = BehaviorRelay<[UserTrackingModel]>(value: [])
    var trackingListDisposer: [String: Disposable] = [:]
    
    override func viewModelDidLoad() {
        super.viewModelDidLoad()
    }
    
    deinit {
        userDisposer?.dispose()
    }
}

// MARK: - Bind

extension MainViewModel {
    
    public func observeGroupStatus() -> Observable<GCGroupStatusModel?> {
        return Observable.create({ observer in
             
            guard let groupKey = AppSingleton.shared.userTrackingList.value?.groups.first?.key else {
                 return Disposables.create()
             }
             FirebaseManager.shared.authUser().flatMap({ _ in
                 FirebaseManager.shared.getData(path: FirebaseGetDataPath.pathGroupStatus(groupKey: groupKey))
             }).subscribe(onNext: { (status: GCGroupStatusModel?) in
                observer.on(.next(status))
             }).disposed(by: self.disposeBag)
            
            return Disposables.create()
        })
    }
    
    func addUserToTrackingList() {
        guard let reporters = AppSingleton.shared.userTrackingList.value?.reporters else {
            return
        }
        
        var newValue = self.trackingList.value
        reporters.forEach({ item in
            if newValue.first(where: { userItem in
                userItem.phoneNumber == item.phoneNumber
            }) == nil {
                newValue.append(UserTrackingModel(name: item.name, phoneNumber: item.phoneNumber,
                                                  isOnline: false, notf: item.notf, image: item.image, lastSeen: 0))
                CORELIB.shared.updateUsageCountForLandingPage(for: .appSpecific(ShowPackagePageEnum.trackerAdd.rawValue))
            }
        })
        self.trackingList.accept(newValue)

        self.calculateTrackingList()
    }
    
    func calculateTrackingList() {
        if !trackingList.value.isEmpty {
            trackingList.value.forEach({ trackingItem in
                if self.trackingListDisposer[trackingItem.phoneNumber] == nil {
                    self.trackingListDisposer[trackingItem.phoneNumber] = FirebaseManager.shared.authUser().flatMap({ _ in
                        FirebaseManager.shared.getData(path: FirebaseGetDataPath.pathNumberStatus(phoneNumber: trackingItem.phoneNumber))
                    }).subscribe(onNext: { [weak self] (response: GCStatusResponseModel?) in
                        guard let result = response else {
                            return
                        }
                        var currentValue = self?.trackingList.value
                        if let firstIndex = currentValue?.firstIndex(where: { currentItem in
                            return currentItem.phoneNumber == trackingItem.phoneNumber
                        }) {
                            currentValue?[firstIndex].duration = result.lastDuration
                            currentValue?[firstIndex].lastSeen = result.lastOnline
                            currentValue?[firstIndex].isOnline = result.isOnline
                        }
                        self?.trackingList.accept(currentValue ?? [])
                    })
                }
            })
        } else {
            self.trackingListDisposer = [:]
        }
    }
}

// MARK: - Action

extension MainViewModel {
    
    func actionPhoneNumberAdd() {
        let options = UNAuthorizationOptions(arrayLiteral: [.alert, .badge, .sound])

        DispatchQueue.main.async {
            UNUserNotificationCenter.current().requestAuthorization(options: options) { (granted, error) in
                if self.trackingList.value.count >= (Int(CORELIB.shared.readAppInfoValue(for: "max_number_track_count") ?? "2") ?? 2) {
                    AlertManager.shared.showNoAction(title: LocKey.View.Main.maxNumberAlert.localize, button: nil)
                } else {
                    if self.phoneNumber.value.isEmpty && self.name.value.isEmpty  {
                        guard let viewController = self.presentedViewController as? MainViewController else {
                            return
                        }
                        DispatchQueue.main.async {
                            viewController.txtPhoneNumber.becomeFirstResponder()
                        }
                    } else if self.phoneNumber.value.isEmpty {
                        AlertManager.shared.showNoAction(title: LocKey.View.Main.phoneNumberNullValidation.localize, button: nil)
                    } else if self.name.value.isEmpty {
                        AlertManager.shared.showNoAction(title: LocKey.View.Main.nameNullValidation.localize, button: nil)
                    } else {
                        do {
                            let parsedNumber = try self.phoneNumberKit.parse(self.phoneNumber.value)
                            let numberString = parsedNumber.numberString
                            let requestModel = CreateTrackerRequestModel(name: self.name.value, number: numberString, color: nil, order: nil,
                                                                     countryCode: DeviceHelper.getDeviceCountry(), notificationStatus: true)
                            self.requestAddNumber(requestModel)
                        }
                        catch {
                            AlertManager.shared.showNoAction(title: LocKey.View.Main.phoneNumberNotValidValidation.localize, button: nil)
                        }
                    }
                }
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func actionUpdateStatusNotification(phoneNumber: String, status: Bool) {
        
        if CORELIB.shared.readAppInfoValue(for: "tracker_notification_status") != "false" {
            guard let numberId = AppSingleton.shared.userTrackingList.value?.reporters.first(where: { $0.phoneNumber == phoneNumber })?.id,
                  let firstIndex = AppSingleton.shared.userTrackingList.value?.reporters.firstIndex(where: { $0.phoneNumber == phoneNumber }) else { return }
            ApiRequest.updateTracker(with: UpdateTrackerRequestModel(notificationStatus: status), id: numberId).subscribe(onNext: { [weak self] response in
                if response.isSuccess {
                    var newValue = AppSingleton.shared.userTrackingList.value
                    newValue?.reporters[firstIndex].notf = status
                    if status {
                        self?.logEvent(eventRouter: EventRouter.numberUnmuted)
                    } else {
                        self?.logEvent(eventRouter: EventRouter.numberMuted)
                    }
                    AppSingleton.shared.userTrackingList.accept(newValue)
                } else {
                    AlertManager.shared.showNoAction(title: LocKey.Dialog.General.warning.localize, button: nil)
                }
            }).disposed(by: disposeBag)
        }
    }
    
    func actionDeleteNumberToList(with phoneNumber: String) {
        var currentValue = self.trackingList.value
        if let firstIndex = currentValue.firstIndex(where: { trackingItem in
            return trackingItem.phoneNumber == phoneNumber
        }) {
            currentValue.remove(at: firstIndex)
            self.trackingList.accept(currentValue)
            self.trackingListDisposer[phoneNumber] = nil
            
            var currentValue = AppSingleton.shared.userTrackingList.value
            currentValue?.groups.removeAll()
            AppSingleton.shared.userTrackingList.accept(currentValue)
        }
    }
    
    func actionUpdateNotificationStatusToList(with phoneNumber: String) {
        var currentValue = self.trackingList.value
        if let firstIndex = currentValue.firstIndex(where: { trackingItem in
            return trackingItem.phoneNumber == phoneNumber
        }) {
            currentValue[firstIndex].notf = !currentValue[firstIndex].notf
            self.trackingList.accept(currentValue)
        }
    }
}

// MARK: - Request

extension MainViewModel: LoaderShowable, Eventable {
    
    func requestAddNumber(_ requestModel: CreateTrackerRequestModel) {
        DispatchQueue.main.async {
            self.showLoader()
        }
        
        ApiRequest.createTracker(with: requestModel).subscribe(onNext: { [weak self] response in
            if response.isSuccess {
                guard let viewController = self?.presentedViewController as? MainViewController else {
                    return
                }
                viewController.actionClearField()
                self?.logEvent(eventRouter: EventRouter.numberAdded)
                
                ApiRequest.hello().subscribe(onNext: { _ in
                    self?.addUserToTrackingList()
                    self?.hideLoader()
                }).disposed(by: (self?.disposeBag)!)
                
            } else {
                self?.hideLoader()
                if response.meta.errorCode?.rawValue == 403001 {
                    CORELIB.shared.showLandingPage(for: .appSpecific(ShowPackagePageEnum.numberAdd.rawValue), on: (self?.presentedViewController)!)
                } else {
                    AlertManager.shared.showNoAction(title: LocKey.Dialog.General.warning.localize, button: nil)
                }
            }
        }, onError: { [weak self] _ in
            self?.hideLoader()
        }).disposed(by: disposeBag)
    }
    
    func requestDeleteNumber(with phoneNumber: String) {
        showLoader()
        
        guard let numberId = AppSingleton.shared.userTrackingList.value?.reporters.first(where: { $0.phoneNumber == phoneNumber })?.id else { return }
        ApiRequest.deleteTracker(with: numberId).subscribe(onNext: { [weak self] response in
            self?.hideLoader()
            if response.isSuccess {
                self?.actionDeleteNumberToList(with: phoneNumber)
                self?.removeUserCachedData(with: phoneNumber)
                self?.logEvent(eventRouter: EventRouter.numberDeleted)
            } else {
                AlertManager.shared.showNoAction(title: LocKey.Dialog.General.warning.localize, button: nil)
            }
        }, onError: { [weak self] _ in
            self?.hideLoader()
        }).disposed(by: disposeBag)
    }
}

// MARK: - Cache

extension MainViewModel: Cacheable {
    
    private func removeUserCachedData(with phoneNumber: String) {
        guard var cachedData: [UserHistoryCacheModel] = getJSONFromCache(for: StorageConstant.userHistory),
            let firstIndex = cachedData.firstIndex(where: { item in
                item.phoneNumber == phoneNumber
            }) else {
            return
        }
        
        cachedData.remove(at: firstIndex)
        saveToCache(with: cachedData, for: StorageConstant.userHistory)
    }
}
