//
//  ContactManagerViewModel.swift
//  FamilyStats
//
//  Created by Can KOÇ on 20.01.2022.
//  Copyright © 2022 Can KOÇ. All rights reserved.
//

import Foundation
import CORELIB

class ContactManagerViewModel: BaseViewModel {

    override func viewModelDidLoad() {
        super.viewModelDidLoad()
    }
	
    func actionOpenMenuPage() {
        let otherVC = UIStoryboard(name: StoryboardConstant.other, bundle: nil).instantiateViewController(withIdentifier: "OtherNavigationViewController")
        otherVC.modalPresentationStyle = .fullScreen
        self.presentedViewController.present(otherVC, animated: true, completion: nil)
    }
}
