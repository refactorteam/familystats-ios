//
//  CodeVerificationViewModel.swift
//  FamilyStats
//
//  Created by Can Koç on 15.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB
import RxCocoa
import RxSwift
import FirebaseAuth

class CodeVerificationViewModel: BaseViewModel {
    
    var phoneNumber = BehaviorRelay(value: String())
    var verificationId = BehaviorRelay(value: String())
    var smsCode = BehaviorRelay(value: String())
    
    override func viewModelDidLoad() {
        super.viewModelDidLoad()
    }
}

// MARK: - Action

extension CodeVerificationViewModel: Cacheable, Eventable {
    
    func actionVerify() -> Observable<Bool> {
        return Observable.create({ observer in
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verificationId.value, verificationCode: self.smsCode.value)
            Auth.auth().signIn(with: credential) { (_, error) in
                if let error = error {
                    observer.on(.error(NumberVerifyError.firebaseError(errorMessage: error.localizedDescription)))
                } else {
                    self.saveUserToCache()
                    self.logEvent(eventRouter: EventRouter.numberVerified)
                    observer.on(.next(true))
                }
            }
            return Disposables.create()
        })
    }
    
    private func saveUserToCache() {
        /*
        if let _: ProfileModel = getJSONFromCache(for: StorageConstant.profile) {
            removeCache(for: StorageConstant.profile)
        }
        
        guard let firebaseAuthId = Auth.auth().currentUser?.uid else { return }
        let newProfile = ProfileModel(phoneInfo: ProfileModel.PhoneInfoModel(phoneNumber: phoneNumber.value,
                                                                             firebaseAuthId: firebaseAuthId), userInfo: nil)
        self.saveToCache(with: newProfile, for: StorageConstant.profile)
        AppSingleton.shared.profile.accept(newProfile)*/
    }
}

