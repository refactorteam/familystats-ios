//
//  HistoryViewModel.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxCocoa
import CORELIB

class HistoryViewModel: BaseViewModel {
    
    var phoneNumber = BehaviorRelay<String>(value: String())
    var historyList = BehaviorRelay<[UserHistoryModel]>(value: [])
    var totalDuration = BehaviorRelay<String>(value: String())
    
    override func viewModelDidLoad() {
        super.viewModelDidLoad()
    }
}

// MARK: - Get

extension HistoryViewModel: LoaderShowable {
    
    func getAllHistory() {
        showLoader()
        
        let lastKey: Int = getDataToCache() ?? 0
        FirebaseManager.shared.authUser().flatMap({ [weak self] _ in
            FirebaseManager.shared.getDataForUserHistory(phoneNumber: self?.phoneNumber.value ?? String(), lastKey: lastKey)
        }).subscribe(onNext: { [weak self] (result: [String: GCHistoryModel]?) in
            
            self?.saveDataToCache(with: result ?? [:])
            
            var currentList = self?.historyList.value ?? []
            let dateHelper = DateHelper()
            
            result?.forEach({ historyItem in
                
                if (currentList.filter({ item -> Bool in
                    return item.inTimeTimeStampForOrder == historyItem.value.inTime
                    }).isEmpty) {
                    
                    let inTime = historyItem.value.inTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
                    let inTimeDate = historyItem.value.inTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
                    let outTime = historyItem.value.outTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
                    let outTimeDate = historyItem.value.outTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
                    
                    var duration = "-"
                    if historyItem.value.outTime != 0 {
                        duration = dateHelper.compareTwoDates(timeStamp1: TimeInterval(historyItem.value.inTime), timeStamp2: TimeInterval(historyItem.value.outTime))
                    }
                    
                    let newItem = UserHistoryModel(inTimeTimeStampForOrder: historyItem.value.inTime,
                                                   outTimeTimeStampForOrder: historyItem.value.outTime,
                                                   inTime: inTime,
                                                   inTimeDate: inTimeDate,
                                                   outTime: outTime,
                                                   outTimeDate: outTimeDate,
                                                   duration: duration)
                    currentList.append(newItem)
                }
                
            })
            
            currentList.sort { (item1, item2) -> Bool in
                return item1.inTimeTimeStampForOrder > item2.inTimeTimeStampForOrder
            }
            self?.historyList.accept(currentList)
            self?.calculateTimeStamp()
            self?.hideLoader()
            
        }).disposed(by: disposeBag)
    }
    
    func calculateTimeStamp() {
        
        var usageSeconds = 0
        self.historyList.value.forEach({ historyItem in
            let duration = self.compareTwoDates(date1: Date(timeIntervalSinceReferenceDate: TimeInterval(historyItem.inTimeTimeStampForOrder)),
                                                  date2: Date(timeIntervalSinceReferenceDate: TimeInterval(historyItem.outTimeTimeStampForOrder)))
            usageSeconds += duration
        })
        var totalUsageText = "0 " + LocKey.View.Report.minutes.localize
        let minutes = usageSeconds / 60
        if minutes > 0 {
            totalUsageText = String(minutes) + " " + LocKey.View.Report.minutes.localize
        }
        self.totalDuration.accept(totalUsageText)
        
    }
    
    private func compareTwoDates(date1: Date, date2: Date) -> Int {
        
        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: date1)
        let nowComponents = calendar.dateComponents([.hour, .minute, .second], from: date2)
        
        if let difference = calendar.dateComponents([.second], from: timeComponents, to: nowComponents).second, difference > 0 {
            return Int(difference)
        } else {
            return 0
        }
    }
}

// MARK: - Cache

extension HistoryViewModel: Cacheable {
    
    private func saveDataToCache(with newData: [String: GCHistoryModel]) {
        if !newData.isEmpty {
            var cachedData: [UserHistoryCacheModel]? = getJSONFromCache(for: StorageConstant.userHistory)
            
            if cachedData == nil {
                var currentList: [String: UserHistoryCacheModel.HistoryModel] = [:]
                newData.forEach({ historyItem in
                    currentList[historyItem.key] = UserHistoryCacheModel.HistoryModel(inTime: historyItem.value.inTime, outTime: historyItem.value.outTime)
                })
                cachedData = [UserHistoryCacheModel(phoneNumber: self.phoneNumber.value, lastKey: newData.first?.key ?? String(), data: currentList)]
            } else {
                if let firstIndex: Int = cachedData?.firstIndex(where: { item in
                    item.phoneNumber == self.phoneNumber.value
                }) {
                    var currentList = cachedData?[firstIndex].data ?? [:]
                    newData.forEach({ historyItem in
                        currentList[historyItem.key] = UserHistoryCacheModel.HistoryModel(inTime: historyItem.value.inTime, outTime: historyItem.value.outTime)
                    })
                    cachedData?[firstIndex].data = currentList
                    cachedData?[firstIndex].lastKey = newData.first?.key ?? String()
                    cachedData?[firstIndex].phoneNumber = self.phoneNumber.value
                }
                else {
                    var currentList: [String: UserHistoryCacheModel.HistoryModel] = [:]
                    newData.forEach({ historyItem in
                        currentList[historyItem.key] = UserHistoryCacheModel.HistoryModel(inTime: historyItem.value.inTime, outTime: historyItem.value.outTime)
                    })
                    cachedData?.append(UserHistoryCacheModel(phoneNumber: self.phoneNumber.value, lastKey: newData.first?.key ?? String(), data: currentList))
                }
                
            }
            
            saveToCache(with: cachedData, for: StorageConstant.userHistory)
        }
        
    }
    
    private func getDataToCache() -> Int? {
        guard let cachedData: [UserHistoryCacheModel] = getJSONFromCache(for: StorageConstant.userHistory),
            let firstIndex = cachedData.firstIndex(where: { item in
                item.phoneNumber == self.phoneNumber.value
            }) else {
            return nil
        }
        
        createList(with: cachedData[firstIndex].data)
        return self.historyList.value.filter({ historyItem -> Bool in
            return historyItem.outTimeTimeStampForOrder > 0
        }).first?.inTimeTimeStampForOrder
    }
    
    private func createList(with data: [String: UserHistoryCacheModel.HistoryModel]) {
        var newList: [UserHistoryModel] = []
        let dateHelper = DateHelper()
        data.forEach({ historyItem in
           let inTime = historyItem.value.inTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
           let inTimeDate = historyItem.value.inTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.inTime)) ?? "-" : "-"
           let outTime = historyItem.value.outTime != 0 ? dateHelper.getReadableHour(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
           let outTimeDate = historyItem.value.outTime != 0 ? dateHelper.getReadableDate(timeStamp: TimeInterval(historyItem.value.outTime)) ?? "-" : "-"
           
           var duration = "-"
           if historyItem.value.outTime != 0 {
               duration = dateHelper.compareTwoDates(timeStamp1: TimeInterval(historyItem.value.inTime), timeStamp2: TimeInterval(historyItem.value.outTime))
           }
           
           let newItem = UserHistoryModel(inTimeTimeStampForOrder: historyItem.value.inTime,
                                          outTimeTimeStampForOrder: historyItem.value.outTime,
                                          inTime: inTime,
                                          inTimeDate: inTimeDate,
                                          outTime: outTime,
                                          outTimeDate: outTimeDate,
                                          duration: duration)
           newList.append(newItem)
        })
        newList.sort { (item1, item2) -> Bool in
            return item1.inTimeTimeStampForOrder > item2.inTimeTimeStampForOrder
        }
       self.historyList.accept(newList)
    }
}
