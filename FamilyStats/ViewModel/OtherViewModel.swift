//
//  OtherViewModel.swift
// FamilyStats
//
//  Created by Can Koç on 26.02.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxCocoa
import CORELIB

class OtherViewModel: BaseViewModel {
    
    var otherMenuList = BehaviorRelay<[OtherMenuModel]>(value: [])
    
    override func viewModelDidLoad() {
        bindOtherMenuList()
    }
    
    func bindOtherMenuList() {
        let list = CORELIB.shared.getStaticPageList()
        otherMenuList.accept(list)
    }
}

