//
//  CreateGroupViewModel.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CORELIB

class CreateGroupViewModel: BaseViewModel {
    
    var isEdit: Bool = false
    var groupName =  BehaviorRelay<String>(value: String())
    let trackingList = BehaviorRelay<[UserTrackingModel]>(value: [])
    let switchStatus = BehaviorRelay<Bool>(value: true)
}

extension CreateGroupViewModel: LoaderShowable, Eventable {
    
    func actionCreateGroup() -> Observable<Bool?> {
        return Observable.create({ observer in
            
            if self.groupName.value.isEmpty {
                AlertManager.shared.showNoAction(title: LocKey.View.CreateGroup.groupNameNullValidation.localize, button: nil)
                observer.on(.next(nil))
            } else {
                if self.isEdit == false {
                    self.actionCreateGroupToFirebase().subscribe(onNext: { result in
                        observer.on(.next(result))
                    }).disposed(by: self.disposeBag)
                } else {
                    self.actionUpdateGroupToFirebase().subscribe(onNext: { result in
                        observer.on(.next(result))
                    }).disposed(by: self.disposeBag)
                }
            }
            
            return Disposables.create()
        })
    }
    
    private func actionCreateGroupToFirebase() -> Observable<Bool> {
        return Observable.create({ observer in
            
            let requestModel = CreateGroupRequestModel(name: self.groupName.value, onAlert: self.switchStatus.value,
                                                       offAlert: self.switchStatus.value)
            ApiRequest.createGroup(with: requestModel).flatMap({ _ in
                ApiRequest.hello()
            }).subscribe(onNext: { [weak self] response in
                if response.isSuccess {
                    self?.logEvent(eventRouter: EventRouter.groupAdded)
                    observer.on(.next(true))
                } else {
                    observer.on(.next(false))
                }
            }, onError: { _ in
                
            }).disposed(by: self.disposeBag)
            return Disposables.create()
        })
    }
    
    private func actionUpdateGroupToFirebase() -> Observable<Bool> {
        return Observable.create({ observer in
            
            self.actionUpdateOnlineStatusNotification(status: self.switchStatus.value)
            self.actionUpdateOfflineStatusNotification(status: self.switchStatus.value)
            self.actionUpdateGroup(name: self.groupName.value)
            observer.on(.next(true))
            
            return Disposables.create()
        })
    }
    
    func actionUpdateOnlineStatusNotification(status: Bool) {
        FirebaseManager.shared.authUser().subscribe(onNext: { [weak self] userID in
            guard let groupId = AppSingleton.shared.userTrackingList.value?.groups.first?.id else { return }
            ApiRequest.updateGroup(with: CreateGroupRequestModel(onAlert: status), id: groupId).subscribe(onNext: { [weak self] response in
                if response.isSuccess {
                    if status {
                        self?.logEvent(eventRouter: EventRouter.groupUnmuted)
                    } else {
                        self?.logEvent(eventRouter: EventRouter.groupMuted)
                    }
                    var currentValue = AppSingleton.shared.userTrackingList.value
                    currentValue?.groups[0].onAlert = status
                    AppSingleton.shared.userTrackingList.accept(currentValue)
                }
            }).disposed(by: (self?.disposeBag)!)
        }).disposed(by: disposeBag)
    }
    
    func actionUpdateOfflineStatusNotification(status: Bool) {
        FirebaseManager.shared.authUser().subscribe(onNext: { [weak self] _ in
            guard let groupId = AppSingleton.shared.userTrackingList.value?.groups.first?.id else { return }
            ApiRequest.updateGroup(with: CreateGroupRequestModel(offAlert: status), id: groupId).subscribe(onNext: { response in
                if response.isSuccess {
                    var currentValue = AppSingleton.shared.userTrackingList.value
                    currentValue?.groups[0].offAlert = status
                    AppSingleton.shared.userTrackingList.accept(currentValue)
                }
            }).disposed(by: (self?.disposeBag)!)
        }).disposed(by: disposeBag)
    }
    
    func actionUpdateGroup(name: String) {
        FirebaseManager.shared.authUser().subscribe(onNext: { [weak self] _ in
            
            guard let groupId = AppSingleton.shared.userTrackingList.value?.groups.first?.id else { return }
            ApiRequest.updateGroup(with: CreateGroupRequestModel(name: name), id: groupId).subscribe(onNext: { response in
                if response.isSuccess {
                    var currentValue = AppSingleton.shared.userTrackingList.value
                    currentValue?.groups[0].name = (self?.groupName.value ?? String())
                    AppSingleton.shared.userTrackingList.accept(currentValue)
                    self?.logEvent(eventRouter: EventRouter.groupRenamed)
                }
            }).disposed(by: (self?.disposeBag)!)
        }).disposed(by: disposeBag)
    }
    
    func actionRemoveGroup() {
        self.actionRemoveGroupToFirebase()
    }
    
    private func actionRemoveGroupToFirebase() {
        self.showLoader()
        guard  let groupId = AppSingleton.shared.userTrackingList.value?.groups.first?.id else { return }
        
        ApiRequest.deleteGroup(with: groupId).subscribe(onNext: { [weak self] response in
            self?.hideLoader()
            if response.isSuccess {
                self?.logEvent(eventRouter: EventRouter.groupDeleted)
                var currentValue = AppSingleton.shared.userTrackingList.value
                currentValue?.groups.removeAll()
                AppSingleton.shared.userTrackingList.accept(currentValue)
            }
        }).disposed(by: disposeBag)
    }
    
}
