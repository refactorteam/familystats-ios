//
//  LocalizationCacheModel.swift
//  FamilyStats
//
//  Created by Can KOÇ on 18.09.2019.
//  Copyright © 2019 CanKOÇ. All rights reserved.
//

import Foundation
struct CustomTestModel: Codable {
	var firstName: String?
	var lastName: String?
	var telephone: String?
	var contactId: String?
	var mail: String?
	var status: String?
	var image: Data?
	var isSelectedContact: Bool?
}

