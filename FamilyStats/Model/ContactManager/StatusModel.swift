 import UIKit

 enum StatusModel: CaseIterable {
	 
	 case swipe
	 case deleted

	 init?(index: Int) {
		 guard let item = StatusModel.allCases.first(where: { $0.index == index }) else { return nil }
		 self = item
	 }
 }

 // MARK: - Helpers
 extension StatusModel {
	 
	 var title: String? {
		 switch self {
		 case .swipe:
			 return LocKey.View.Stats.swipedTitle.localize
		 case .deleted:
			 return LocKey.View.Stats.deletedTitle.localize
		 }
	 }
	  
	var description: String? {
		switch self {
		case .swipe:
			return LocKey.View.Stats.swipedDescription.localize
		case .deleted:
			return LocKey.View.Stats.deletedDescription.localize
		}
	}
	
	 var image: UIImage? {
		 switch self {
		 case .swipe:
			 return UIImage(named: "iconStatusSwipe")
		 case .deleted:
			return UIImage(named: "iconStatusDeleted")
		 }
	 }
	 
	var totalCount: Int {
		switch self {
		case .swipe:
			return self.getCount() // CacheManager.shared.getContactTypeCount(with: StorageConstant.swipe)
		case .deleted:
			return CacheManager.shared.getContactTypeCount(with: StorageConstant.deleted)
		}
	}
	
	func getCount() -> Int {
        let swipeCount = CacheManager.shared.getContactTypeCount(with: StorageConstant.swipe)
        let deletedCount = CacheManager.shared.getContactTypeCount(with: StorageConstant.deleted)
		
		let totalCount = swipeCount + deletedCount

		return totalCount
	}
	
	 var index: Int {
		 switch self {
		 case .swipe:
			 return 0
		 case .deleted:
			 return 1
		 }
	 }
 }
