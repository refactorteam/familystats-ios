//
//  GroupHistoryCacheModel.swift
//  FamilyStats
//
//  Created by Can Koç on 13.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct GroupHistoryCacheModel: Codable {
    var groupKey: String
    var lastKey: String
    var data: [String: HistoryModel]
    
    struct HistoryModel: Codable {
        var inTime: Int
        var outTime: Int
        
        enum CodingKeys: String, CodingKey {
            case inTime = "i"
            case outTime = "o"
        }
    }
}
