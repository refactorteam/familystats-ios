//
//  UserTrackingModel.swift
//  FamilyStats
//
//  Created by Can Koç on 23.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct UserTrackingModel: Codable {
    var name: String
    var phoneNumber: String
    var isOnline: Bool
    var notf: Bool
    var image: String?
    var lastSeen: Int
    var duration: Int?
}
