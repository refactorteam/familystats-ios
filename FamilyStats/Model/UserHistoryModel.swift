//
//  HistoryModel.swift
//  FamilyStats
//
//  Created by Can Koç on 24.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct UserHistoryModel {
    var inTimeTimeStampForOrder: Int
    var outTimeTimeStampForOrder: Int
    var inTime: String
    var inTimeDate: String
    var outTime: String
    var outTimeDate: String
    var duration: String
}
