//
//  UserGroupModel.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct UserGroupModel: Codable {
    var id: Int
    var name: String
    var key: String
    var offAlert: Bool
    var onAlert: Bool
    var members: [UserModel]
    
    enum CodingKeys: String, CodingKey {
        case id, name, key, members
        case offAlert = "off_alert"
        case onAlert = "on_alert"
    }
}

struct UserGroupStatusModel: Codable {
    var isOnline: Bool
    var lastOnline: Int
    var lastDuration: Int?
    
    enum CodingKeys: String, CodingKey {
        case isOnline = "is_online"
        case lastOnline =  "last_online"
        case lastDuration =  "last_duration"
    }
}
