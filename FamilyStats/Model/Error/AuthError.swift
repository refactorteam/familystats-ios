//
//  AuthError.swift
//  FamilyStats
//
//  Created by Can Koç on 20.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum AuthError: Error {
    case signInError
}
