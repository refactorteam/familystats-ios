//
//  VictimModel.swift
//  FamilyStats
//
//  Created by Can Koç on 21.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    var id: Int
    var name: String
    var phoneNumber: String
    var countryCode: String
    var image: String?
    var notf: Bool
    var color: String?
    var order: Int?
    
    enum CodingKeys: String, CodingKey {
        case name, image, id
        case countryCode = "country_code"
        case phoneNumber = "number"
        case notf = "notification_status"
        
    }
}
