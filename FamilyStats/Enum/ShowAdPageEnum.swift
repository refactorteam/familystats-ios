//
//  ShowAdPageEnum.swift
// FamilyStats
//
//  Created by Can Koç on 21.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum ShowAdPageEnum: String, Codable {
    case other, other2, contactCard, leftSwipe, rightSwipe
}
