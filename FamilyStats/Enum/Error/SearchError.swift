//
//  SearchError.swift
// FamilyStats
//
//  Created by Can Koç on 7.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum SearchError: Error {
    case numberValidation
    case numberParseValidation
}
