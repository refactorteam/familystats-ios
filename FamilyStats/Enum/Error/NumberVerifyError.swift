//
//  NumberVerifyError.swift
// FamilyStats
//
//  Created by Can Koç on 1.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum NumberVerifyError: Error {
    case firebaseError(errorMessage: String)
}
