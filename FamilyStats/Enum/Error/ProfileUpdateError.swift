//
//  ProfileUpdateError.swift
// FamilyStats
//
//  Created by Can Koç on 4.03.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum ProfileUpdateError: Error {
    case nicknameValidation
    case emailValidation
    case sameValue
}
