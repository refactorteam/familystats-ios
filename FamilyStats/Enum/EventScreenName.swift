//
//  EventScreenName.swift
// FamilyStats
//
//  Created by Can Koç on 7.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum EventScreenName: String, Codable {
    
    case register, onboarding, main, other, privacy
    case numberHistory = "number_history"
    case numberStatistics = "number_statistics"
    case groupHistory = "group_history"
    case numberRegistration = "number_registration"
    case codeVerification = "code_verification"
    case createGroup = "create_group"
    case contactManager = "contact_manager"
}
