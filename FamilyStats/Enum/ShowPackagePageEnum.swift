//
//  ShowPackagePageEnum.swift
// FamilyStats
//
//  Created by Can Koç on 21.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum ShowPackagePageEnum: String, Codable {
    case mainCrown = "main_crown"
    case bannerRemove = "banner_remove"
    case numberAdd = "number_add_error"
    case report = "report"
    case groupReport = "group_report"
    case trackerAdd = "tracker_add"
    case numberDetail = "number_detail"
}
