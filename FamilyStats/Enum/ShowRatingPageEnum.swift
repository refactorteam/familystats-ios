//
//  ShowRatingPageEnum.swift
// FamilyStats
//
//  Created by Can Koç on 21.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

enum ShowRatingPageEnum: String, Codable {
    case firstCapture, firstExport
    
    enum CodingKeys: String, CodingKey {
        case firstCapture = "first_capture"
        case firstExport = "first_export"
    }
}
