//
//  AppDelegate.swift
// FamilyStats
//
//  Created by Can Koç on 02.01.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import UIKit
import RxSwift
import CORELIB
import CoreLocation
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain 
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        CORELIB.shared.startAppDelegate(with: launchOptions)
        return true
    }
    
    // MARK: - Remote Notifications
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        CORELIB.shared.syncNotification(token: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // AppEvents.activateApp()
        
        // Reset Bagde while entering App
        UIApplication.shared.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    // MARK: - Deep Link
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        CORELIB.shared.saveOpened(url: url)
        return true
        // return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    // MARK: - Messaging Delegate
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        CORELIB.shared.readNotification(with: userInfo)
    }
}

extension AppDelegate {

    func applicationWillTerminate(_ application: UIApplication) {
        CORELIB.shared.sendAppCloseEvent()
    }
}
