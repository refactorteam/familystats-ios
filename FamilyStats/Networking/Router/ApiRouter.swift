//
//  ApiRouter.swift
// FamilyStats
//
//  Created by Can Koç on 13.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB
import Alamofire

enum ApiRouter: ApiRouterProtocol {
    
    case hello
    case createTracker(requestModel: CreateTrackerRequestModel)
    case removeTracker(id: Int)
    case updateTracker(id: Int, requestModel: UpdateTrackerRequestModel)
    
    case createGroup(requestModel: CreateGroupRequestModel)
    case removeGroup(id: Int)
    case updateGroup(id: Int, requestModel: CreateGroupRequestModel)
    
    case image(requestModel: ImageRequestModel)
}

// MARK: - Path

extension ApiRouter {
    
    var path: String {
        switch self {
        case .hello:
            return "hello"
        case .createTracker:
            return "number"
        case .removeTracker(let id):
            return "number/\(id)"
        case .updateTracker(let id, _):
            return "number/\(id)"
            
        case .createGroup:
            return "group"
        case .removeGroup(let id):
            return "group/\(id)"
        case .updateGroup(let id, _):
            return "group/\(id)"
            
        case .image:
            return "image"
        }
    }
}

// MARK: - Method

extension ApiRouter {
    
    var method: HTTPMethod {
        switch self {
        case .hello:
            return .get
        case .createTracker, .createGroup, .image:
            return .post
        case .removeTracker, .removeGroup:
            return .delete
        case .updateTracker, .updateGroup:
            return .patch
        }
    }
}

// MARK: - Parameters

extension ApiRouter {
    
    var parameters: Parameters {
        switch self {
        case .hello, .removeTracker, .removeGroup:
            return NullRequestModel().dictionary!
        case .createTracker(let requestModel):
            return requestModel.dictionary!
        case .updateTracker(_, let requestModel):
            return requestModel.dictionary!
        case .createGroup(let requestModel):
            return requestModel.dictionary!
        case .updateGroup(_, let requestModel):
            return requestModel.dictionary!
        case .image(let requestModel):
            return requestModel.dictionary!
        }
    }
}

// MARK: - Loader

extension ApiRouter {
    
    var loadingMessage: String {
        return String()
    }
}

// MARK: - Error Action

extension ApiRouter {
    
    var errorAction: [ApiErrorAction] {
        return [.none]
    }
}

// MARK: - Loader, Logger

extension ApiRouter {
    
    var isLogEnable: Bool {
        return true
    }
    
    var isLoaderEnable: Bool {
        return false
    }
}
