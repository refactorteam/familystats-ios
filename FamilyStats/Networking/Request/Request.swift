//
//  Request.swift
// FamilyStats
//
//  Created by Can Koç on 13.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import RxSwift
import CORELIB

class ApiRequest {
    
    static func hello() -> Observable<BaseResponseModel<HelloResponseModel>> {
        return ApiClient().request(router: ApiRouter.hello)
    }
    
    static func createTracker(with requestModel: CreateTrackerRequestModel) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.createTracker(requestModel: requestModel))
    }
    
    static func deleteTracker(with id: Int) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.removeTracker(id: id))
    }
    
    static func updateTracker(with requestModel: UpdateTrackerRequestModel, id: Int) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.updateTracker(id: id, requestModel: requestModel))
    }
    
    static func createGroup(with requestModel: CreateGroupRequestModel) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.createGroup(requestModel: requestModel))
    }
    
    static func deleteGroup(with id: Int) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.removeGroup(id: id))
    }
    
    static func updateGroup(with requestModel: CreateGroupRequestModel, id: Int) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.updateGroup(id: id, requestModel: requestModel))
    }
    
    static func image(with requestModel: ImageRequestModel) -> Observable<BaseResponseModel<CreateTrackerResponseModel>> {
        return ApiClient().request(router: ApiRouter.image(requestModel: requestModel))
    }
}
