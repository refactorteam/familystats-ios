//
//  CreateTrackerRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct CreateTrackerRequestModel: Codable {
    var name: String
    var number: String
    var color: String?
    var order: Int?
    var countryCode: String
    var notificationStatus: Bool
    
    enum CodingKeys: String, CodingKey {
        case name, number, color, order
        case countryCode = "country_code"
        case notificationStatus = "notification_status"
    }
}

