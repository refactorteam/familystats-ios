//
//  CreateGroupRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 14.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct CreateGroupRequestModel: Codable {
    var name: String?
    var onAlert: Bool?
    var offAlert: Bool?
    
    enum CodingKeys: String, CodingKey {
        case name
        case onAlert = "on_alert"
        case offAlert = "off_alert"
    }
}
