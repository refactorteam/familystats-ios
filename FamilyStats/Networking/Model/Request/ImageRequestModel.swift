//
//  ImageRequestModel.swift
//  FamilyStats
//
//  Created by Can KOÇ on 17.07.2021.
//  Copyright © 2021 Can KOÇ. All rights reserved.
//

import Foundation

struct ImageRequestModel: Codable {
    var countryCode: String
    var number: String
    
    enum CodingKeys: String, CodingKey {
        case countryCode = "country_code"
        case number
    }
}
