//
//  UpdateTrackerRequestModel.swift
//  FamilyStats
//
//  Created by Can Koç on 14.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation

struct UpdateTrackerRequestModel: Codable {
    var notificationStatus: Bool
    
    enum CodingKeys: String, CodingKey {
        case notificationStatus = "notification_status"
    }
}
