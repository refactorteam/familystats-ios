//
//  HelloResponseModel.swift
//  FamilyStats
//
//  Created by Can Koç on 6.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB

struct HelloResponseModel: ResponseProtocol {
    var reporters: [UserModel]
    var groups: [UserGroupModel]
    
    func processResponse() {
        AppSingleton.shared.userTrackingList.accept(self)
    }
}
