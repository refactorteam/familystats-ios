//
//  TestEventLogModel.swift
// FamilyStats
//
//  Created by Can Koç on 13.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB

struct DefaultEventLogModel: EventLogModel, Codable {
    var userType: String = DeviceHelper.getUserType()
    var country: String = DeviceHelper.getDeviceCountry()
    var deviceID: String = DeviceHelper.getDeviceID()
    
    enum CodingKeys: String, CodingKey {
        case country
        case deviceID = "device_id"
        case userType = "user_type"
    }
}
