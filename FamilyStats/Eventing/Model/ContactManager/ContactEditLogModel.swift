import Foundation
import CORELIB

struct ContactEditLogModel: EventLogModel, Codable {
    var country: String = DeviceHelper.getDeviceCountry()
    var deviceID: String = DeviceHelper.getDeviceID()
    var userType: String = DeviceHelper.getUserType()
    
    enum CodingKeys: String, CodingKey {
        case country
        case deviceID = "device_id"
        case userType = "user_type"
    }
}
