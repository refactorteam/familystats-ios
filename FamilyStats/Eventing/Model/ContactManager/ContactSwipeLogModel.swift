import Foundation
import CORELIB
 
struct ContactSwipeLogModel: EventLogModel, Codable {
    var country: String = DeviceHelper.getDeviceCountry()
    var deviceID: String = DeviceHelper.getDeviceID()
    var userType: String = DeviceHelper.getUserType()
	var swipeAction: String?
	var cardType: String?

    init(swipeAction: String, cardType: String) {
		self.swipeAction = swipeAction
		self.cardType = cardType
    }
	
    enum CodingKeys: String, CodingKey {
        case country
        case deviceID = "device_id"
        case userType = "user_type"
		case swipeAction = "swipe_action"
		case cardType = "card_type"
    }
}
