import Foundation
import CORELIB

struct MergeLogModel: EventLogModel, Codable {
    var country: String = DeviceHelper.getDeviceCountry()
    var deviceID: String = DeviceHelper.getDeviceID()
    var userType: String = DeviceHelper.getUserType()
	var engagementScreen: String?
	var actionStatus: String?

    init(engagementScreen: String, actionStatus: String) {
        self.engagementScreen = engagementScreen
        self.actionStatus = actionStatus
    }
	
    enum CodingKeys: String, CodingKey {
        case country
        case deviceID = "device_id"
        case userType = "user_type"
		case engagementScreen = "engagement_screen"
		case actionStatus = "action_status"
    }
}
