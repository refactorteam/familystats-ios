import Foundation
import CORELIB
 
struct ButtonSwipeLogModel: EventLogModel, Codable {
    var country: String = DeviceHelper.getDeviceCountry()
    var deviceID: String = DeviceHelper.getDeviceID()
    var userType: String = DeviceHelper.getUserType()
	var buttonSwipeAction: String?
	var cardType: String?
	
    init(buttonSwipeAction: String, cardType: String) {
		self.buttonSwipeAction = buttonSwipeAction
		self.cardType = cardType
    }
	
    enum CodingKeys: String, CodingKey {
        case country
        case deviceID = "device_id"
        case userType = "user_type"
		case buttonSwipeAction = "button_swipe_action"
		case cardType = "card_type"
    }
}
