import Foundation
import CORELIB
 
struct RecycleBinLogModel: EventLogModel, Codable {
    var country: String = DeviceHelper.getDeviceCountry()
    var deviceID: String = DeviceHelper.getDeviceID()
    var userType: String = DeviceHelper.getUserType()
	var recycleBinAction: String?
	
    init(recycleBinAction: String) {
		self.recycleBinAction = recycleBinAction
    }
	
    enum CodingKeys: String, CodingKey {
        case country
        case deviceID = "device_id"
        case userType = "user_type"
		case recycleBinAction = "recycle_bin_action"
    }
}
