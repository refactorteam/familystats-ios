//
//  EventRouter.swift
// FamilyStats
//
//  Created by Can Koç on 13.07.2021.
//  Copyright © 2021 Can Koç. All rights reserved.
//

import Foundation
import CORELIB

enum EventRouter: EventRouterProtocol {
    
    case numberVerified
    case numberAdded
    case numberDeleted
    case numberMuted
    case numberUnmuted
    case groupAdded
    case groupDeleted
    case groupMuted
    case groupUnmuted
    case groupRenamed
    
    // contact manager
    case contactSwipe(logModel: ContactSwipeLogModel)
    case buttonSwipe(logModel: ButtonSwipeLogModel)
    case merge(logModel: MergeLogModel)
    case stats(logModel: StatsLogModel)
    case contactEdit(logModel: ContactEditLogModel)
    case recycleBin(logMode: RecycleBinLogModel)
    case contactCompleted
    
    var eventName: String {
        switch self {
        case .numberVerified:
            return "numberVerified"
        case .numberAdded:
            return "numberAdded"
        case .numberDeleted:
            return "numberDeleted"
        case .numberMuted:
            return "numberMuted"
        case .numberUnmuted:
            return "numberUnmuted"
            
        case .groupAdded:
            return "numberAdded"
        case .groupDeleted:
            return "numberDeleted"
        case .groupMuted:
            return "numberMuted"
        case .groupUnmuted:
            return "numberUnmuted"
        case .groupRenamed:
            return "groupRenamed"
            
            
        case .contactSwipe:
            return "contact_swipe"
        case .buttonSwipe:
            return "button_swipe"
        case .merge:
            return "merge"
        case .stats:
            return "stats"
        case .contactEdit:
            return "contact_edit"
        case .recycleBin:
            return "recycle_bin"
            
        case .contactCompleted:
            return "contact_completed"
            
        }
    }
    
    var parameters: [String : Any]? {
        
        switch self {
        case .contactSwipe(let logModel):
            return logModel.dictionary!
        case .buttonSwipe(let logModel):
            return logModel.dictionary!
        case .merge(let logModel):
            return logModel.dictionary!
        case .stats(let logModel):
            return logModel.dictionary!
        case .contactEdit(let logModel):
            return logModel.dictionary!
        case .recycleBin(let logModel):
            return logModel.dictionary!
        default:
            return DefaultEventLogModel().dictionary!
        }
    }
    
}
